const jobs = require('./app/controllers/cron.controller');
var moment = require('moment');
const schedule = require('node-schedule');

async function resourceReleaseInvoiceGeneration(mondayCheck, todayDate) {
    var n = 0;
    await jobs.cronReleaseResourceInvoice().then(async data => {
        return data;
    }).then(async data => {

        await data.forEach(async element => {
            console.log(element.Release_date + '<' + todayDate);
            n++;
            console.log('CRON ' + n);

            //if (element.Release_date) {
            if (element.Release_date <= todayDate) {
                var c = {
                    body: {
                        Resource_id: element.Resource_id,
                        Requirement_id: element.Requirement_id,
                        User_id: element.Released_by,
                        mondayCheck: mondayCheck,
                    }
                }
                await jobs.resourceRelease(c).then(datas => {
                    console.log('********************');
                });
            } else {
                console.log(element.Release_date + '>' + todayDate);
                console.log('Release is on next time!');
            }
        });
    })
}

async function invoiceGeneration(mondayCheck) {

    jobs.cronUsersInvoice().then(async data => {
        return data;
    }).then(users => {

        users.forEach(element => {
            var c = {
                body: {
                    User_id: element,
                    mondayCheck: mondayCheck
                }
            }
            jobs.getInvoices(c).then(data => {
                console.log('********************');
                console.log(data);
            });
        });
    })
}

function getMondays(date) {
    var d = date || new Date(),
        month = d.getMonth(),
        mondays = [];
    d.setDate(1);
    // Get the first Monday in the month
    while (d.getDay() !== 1) {
        d.setDate(d.getDate() + 1);
    }
    // Get all the other Mondays in the month
    while (d.getMonth() === month) {
        mondays.push(new Date(d.getTime()).toLocaleDateString("en-US"));
        d.setDate(d.getDate() + 7);
    }
    return mondays;
}

async function initInvoice() {
    var thisMonth = moment().month();
    let mondaysList = getMondays(moment().month(thisMonth).toDate());
    var n = 0;
    mondaysList.forEach(async element => {
        n++;
        var check = moment(element, 'MM/DD/YYYY');
        var mondayCheck = check.format('YYYY-MM-DD');
        var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '-');

        if (todayDate >= mondayCheck && (n % 2) == 0) {
            console.log('valida date' + mondayCheck);
            await invoiceGeneration(mondayCheck);
            await resourceReleaseInvoiceGeneration(mondayCheck, todayDate);
        } else {
            //await resourceReleaseInvoiceGeneration(mondayCheck, todayDate);
        }
    });
}

function earMarkingInit() {
    jobs.removeEarmarking().then(async data => {
        console.log('********************');
        console.log("CRON job is running for  Earmarking...");
        console.log('********************');
        console.log(data);

    })
}
const getCircularReplacer = () => {
    const seen = new WeakSet();
    return (key, value) => {
        if (typeof value === "object" && value !== null) {
            if (seen.has(value)) {
                return;
            }
            seen.add(value);
        }
        return value;
    };
};

const weeklyInvoice = schedule.scheduleJob('00 0 * * 1', async function() {
    //const job = schedule.scheduleJob('1 * * * * *', async function() {
    console.log('********************');
    console.log("CRON job is running for Weekly invoice... And Resource Release...");
    console.log('********************');
    initInvoice();
});

const InitearMarking = schedule.scheduleJob('00 00 00 * * 0-6', async function() {
    // const job = schedule.scheduleJob('1 * * * * *', async function(){
    console.log('********************');
    console.log("CRON job is running for earMarking...");
    console.log('********************');
    earMarkingInit();
});


console.log('JOB Lists =' + JSON.stringify(schedule.scheduledJobs, getCircularReplacer()));
module.exports = { initInvoice };