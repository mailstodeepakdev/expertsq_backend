module.exports = app => {
    const HManager = require("../controllers/hiring.controller.js");

    var router = require("express").Router();

    router.put("/updateProfile", HManager.updateProfile);
    router.post("/getMyCompany", HManager.getMyCompany);
    router.post("/getResources_all", HManager.getResources_all);
    router.post("/searchByKeywords", HManager.searchByKeywords);
    router.post("/createproject", HManager.createProject);
    router.post("/projectList", HManager.listofProjects);
    router.post("/searchProjectById", HManager.searchProjectById);
    router.post("/myResources", HManager.myResources);
    router.post("/editProject", HManager.editProject);
    router.post("/getRequirementDataById", HManager.getRequirementDataById);
    router.post("/getRequirementDomainsById", HManager.getRequirementDomainsById);
    router.post("/getRequirementTechnologiesById", HManager.getRequirementTechnologiesById);
    router.post("/getRequirementQualificationsById", HManager.getRequirementQualificationsById);
    router.post("/getRequirementRolesById", HManager.getRequirementRolesById);
    router.post("/createNewAssignment", HManager.createNewAssignment);
    router.post("/getResourceTechnologiesById", HManager.getResourceTechnologiesById);
    router.post("/getResourceDomainsById", HManager.getResourceDomainsById);
    router.post("/getResourceRolesById", HManager.getResourceRolesById);
    router.post("/getResourceQualificationsById", HManager.getResourceQualificationsById);

    app.use('/hiring', router);
};