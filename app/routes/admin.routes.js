module.exports = app => {
    const admin = require("../controllers/admin.controller");
    const cron = require("../controllers/cron.controller");
 
   var router = require("express").Router();
 
   router.post("/invoiceCronCall", admin.invoiceCronCall); 
   router.post("/invoiceLists", admin.invoiceLists);   
   router.post("/initialInvoiceGeneration", cron.initialInvoiceGeneration);   
   router.post("/clearData", admin.clearData);    
  
   app.use('/admin', router);
};  