module.exports = app => {
    const spoc = require("../controllers/spoc.controller.js");
   
    var router = require("express").Router();
   
    router.post("/userCheck", spoc.checkManagerType); 
    router.post("/getMyCompany", spoc.getMyCompany);     
    router.put("/updateProfile", spoc.updateProfile);   
    router.post("/createListingManager", spoc.createListingManager);  
    router.post("/getListingManagers", spoc.getListingManagers);   
    router.post("/createHiringManager", spoc.createHiringManager); 
    router.post("/getHiringManagers", spoc.getHiringManagers);   
    router.put("/HMDeletion", spoc.HMDeletion);    
    router.post("/getAddressById", spoc.getAddressById);   
    router.post("/getManagersList", spoc.getManagersList);   
    router.put("/UserDeletion", spoc.UserDeletion);    
    router.post("/createManager", spoc.createManager);    
  
    //app.use('/spoc/signup', router);
    app.use('/spoc', router);
};