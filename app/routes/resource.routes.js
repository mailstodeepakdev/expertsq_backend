module.exports = app => {
    const resource = require("../controllers/resource.controller");

    var router = require("express").Router();

    router.post("/getRequirementData", resource.getRequirementData);
    router.post("/updateTimesheet", resource.updateTimesheet);
    router.post("/getTimesheetResource", resource.getTimesheetResource);
    router.post("/timesheetOk", resource.timesheetOk);
    router.post("/getRequirementsListsByResourceId", resource.getRequirementsListsByResourceId);
    router.post("/getRequirementsListsByResourceIdTimesheet", resource.getRequirementsListsByResourceIdTimesheet);
    router.post("/getRequirementDataTimesheet", resource.getRequirementDataTimesheet);
    router.post("/getRequirementIdByResourceId", resource.getRequirementIdByResourceId);

    //app.use('/spoc/signup', router);
    app.use('/resource', router);
};