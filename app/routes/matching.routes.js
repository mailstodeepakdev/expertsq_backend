module.exports = app => {
    const matcher = require("../controllers/matching.controller.js");

    var router = require("express").Router();


    router.post("/projectMatching", matcher.projectMatching);
    router.post("/resourceMatching", matcher.resourceMatching);

    router.post("/getMatchingValues", matcher.getMatchingValues);

    app.use('/match', router);
};