const db = require("../models");
const companyTb = db.companyTb;
const HManagerTb = db.HManagerTb;
const delegateTb = db.delegateTb;
const resourceTb = db.resourceTb;
const requirementTb = db.requirement;
const NotificationsTbs = db.NotificationsTbs;
const projectTb = db.project;
const usersTb = db.user;
const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt');
var moment = require('moment');
const resourceTechnologyTbs = db.resourceTechnologyTbs;
const resourceDomainTbs = db.resourceDomainTbs;
const resourceRoleTbs = db.resourceRoleTbs;
const resourceEducationTbs = db.resourceEducationTbs;
const SelectedDomainsTb = db.SelectedDomains;
const SelectedQualificationsTb = db.SelectedQualifications;
const SelectedTechTb = db.SelectedTech;
const selectedRolesTb = db.SelectedRoles;
const educationTb = db.educationTb;
const rolesTb = db.roles;
const technologyTb = db.technology;
const domainTb = db.domainTb;


exports.updateProfile = async(req, res) => {
    if (!req.body.HManager_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const profileData = {
        HManager_name: req.body.HManager_name,
        HManager_designation: req.body.HManager_designation,
        HManager_phone: req.body.HManager_phone
    };
    const userData = {
        User_email: req.body.User_email,
    }
    console.log(req.body);
    async function updateHManager() {
        return await HManagerTb.update(profileData, {
                where: { HManager_id: req.body.HManager_id }
            }).then(num => {
                if (num == 1) {
                    res.send(num);
                    //return true;
                } else {
                    return false;
                }
            })
            .catch(err => {
                var errorMsg = {
                        error: 0
                    }
                    //res.status(500).send(errorMsg);
                return false;
            });


    }
    async function userMailUpdate() {
        return await usersTb.update(userData, {
                where: { User_id: req.body.User_id }
            }).then(num => {
                if (num == 1) {
                    // res.send(num);
                    return true;
                } else {
                    return false;
                }
            })
            .catch(err => {
                var errorMsg = {
                        error: 0
                    }
                    //res.status(500).send(errorMsg);
                return false;
            });
    }
    const isUpdatedHManager = await updateHManager();
    const isHManagerMailUpdated = await userMailUpdate();

    if (isUpdatedHManager == true && isHManagerMailUpdated == true) {
        res.send(isHManagerMailUpdated);
        //return true;
    } else {
        return false;
    }

};


exports.getMyCompany = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    async function getCompanyData() {
        return await usersTb.findAll({
            where: {
                User_id: req.body.User_id
            },
            include: {
                model: HManagerTb,
                include: {
                    model: companyTb,
                    required: true
                }
            },

        });
    }
    const myCompanyData = await getCompanyData();
    res.status(200).send(myCompanyData);

};

exports.getResources_all = async(req, res) => {
    if (req.body.User_id < 0) {
        res.status(400).send({
            message: "Content can not be empty!!"
        });
        return;
    }
    async function getCompanyData() {
        return await resourceTb.findAll({
            where: {
                Resource_active: '1'
            },
            include: {
                model: companyTb,
                required: true
            },

        });
    }
    const myCompanyData = await getCompanyData();
    res.status(200).send(myCompanyData);
    return;
};

exports.searchByKeywords = (req, res) => {
    if (req.body.HManager_id < 0 || req.body.Delegate_id < 0) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    const title = req.body.title;

    resourceTb.findAll({
            where: {
                [Op.or]: [{
                        Resource_name: {
                            [Op.like]: `%${title}%`
                        }
                    },
                    {
                        Resource_Designation: {
                            [Op.like]: `%${title}%`
                        }
                    }
                ],
                [Op.and]: [{
                    Resource_active: 1
                }]
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};
exports.createProject = async(req, res) => {
    console.log('--------------------------- --');
    console.log(req.body);
    console.log('--------------------------- --');
    if (!req.body.Project_name) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var user_id = req.body.User_id;
    var Manager_id = req.body.allocatedUser;
    if (Manager_id <= 0) {
        Manager_id = user_id;

    }

    const projectData = {
        Project_name: req.body.Project_name,
        Project_location: req.body.Project_location,
        Project_state: req.body.Project_state,
        User_id: user_id,
        Start_date: req.body.Start_date,
        End_date: req.body.End_date,
        Description: req.body.Description,
        Created_by: req.body.User_id,
        Company_id: req.body.Company_id,
        Status: req.body.Status,
        Manager_id: Manager_id
    };
    async function pushtoNotificationsTb(user_id, Company_id, allocatedUser) {
        var pushData = {
            User_id: user_id,
            Company_id: Company_id,
            CC_mail_user_id: allocatedUser,
            Category: 'ProjectCreation'
        }
        NotificationsTbs.create(pushData);
    }
    async function projectCreation() {
        if (req.body.notifications) {
            await pushtoNotificationsTb(req.body.User_id, req.body.Company_id, req.body.allocatedUser);
        }
        return await projectTb
            .findOne({ where: { Project_id: req.body.Project_id } })
            .then(async function(obj) {
                // update
                if (obj) {
                    return await obj.update(projectData);

                } else {
                    // insert
                    return await projectTb.create(projectData);

                }
            })
    }
    await projectCreation().then(data => {
        console.log(data);
        res.send(data);

    });

};

exports.getRequirementDataById = async(req, res) => {
    const requirement_id = req.body.Requirement_id;

    await requirementTb.findAll({
            where: { Requirement_id: requirement_id },
            include: [{
                model: SelectedDomainsTb,
                required: false,
                include: {
                    model: domainTb,
                    required: true,
                }
            }, {
                model: SelectedTechTb,
                required: false,
                include: {
                    model: technologyTb,
                    required: true,
                }
            }, {
                model: selectedRolesTb,
                required: false,
                include: {
                    model: rolesTb,
                    required: true,
                }
            }, {
                model: SelectedQualificationsTb,
                required: false,
                include: {
                    model: educationTb,
                    required: true,
                }
            }]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};

exports.getRequirementDomainsById = async(req, res) => {
    const requirement_id = req.body.Requirement_id;

    await SelectedDomainsTb.findAll({
            where: { Requirement_id: requirement_id },
            include: {
                model: domainTb,
                required: true
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};
exports.getRequirementTechnologiesById = async(req, res) => {
    const requirement_id = req.body.Requirement_id;

    await SelectedTechTb.findAll({
            where: { Requirement_id: requirement_id },
            include: {
                model: technologyTb,
                required: true
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};
exports.getRequirementQualificationsById = async(req, res) => {
    const requirement_id = req.body.Requirement_id;

    await SelectedQualificationsTb.findAll({
            where: { Requirement_id: requirement_id },
            include: {
                model: educationTb,
                required: true
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};
exports.getRequirementRolesById = async(req, res) => {
    const requirement_id = req.body.Requirement_id;

    await selectedRolesTb.findAll({
            where: { Requirement_id: requirement_id },
            include: {
                model: rolesTb,
                required: true
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};

exports.editProject = async(req, res) => {
    console.log(req.body);
    if (!req.body.Project_name) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var user_id = req.body.User_id;
    var Manager_id = req.body.allocatedUser;
    if (Manager_id <= 0) {
        Manager_id = user_id;

    }

    const projectData = {
        Project_name: req.body.Project_name,
        Project_location: req.body.Project_location,
        Project_state: req.body.Project_state,
        User_id: user_id,
        Start_date: req.body.Start_date,
        End_date: req.body.End_date,
        Description: req.body.Description,
        Created_by: req.body.User_id,
        Company_id: req.body.Company_id,
        Manager_id: Manager_id
    };
    async function pushtoNotificationsTb(user_id, Company_id, allocatedUser) {
        var pushData = {
            User_id: user_id,
            Company_id: Company_id,
            CC_mail_user_id: allocatedUser,
            Category: 'ProjectCreation'
        }
        NotificationsTbs.create(pushData);
    }
    async function projectCreation() {
        projectTb.update(projectData, { where: { Project_id: req.body.Project_id } })
            .then(async num => {
                if (num == 1) {
                    if (req.body.notifications) {
                        await pushtoNotificationsTb(req.body.User_id, req.body.Company_id, req.body.allocatedUser);
                    }
                    res.send({
                        status: true
                    });
                }

            })
            .catch(err => {
                console.log(err);
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the Tutorial."
                });
            });
    }
    const isCreated = projectCreation();

};

exports.listofProjects = (req, res) => {
    const Company_id = req.body.Company_id;
    const User_id = req.body.User_id;
    let projectdata = [];
    let completion = 0;
    const today = moment();


    projectTb.findAll({
            where: {
                Company_id: Company_id
            },
            include: {
                model: requirementTb,
                required: false,
                where: {
                    Project_id: {
                        [Op.col]: 'ProjectsTbs.Project_id'
                    }
                }
            }
        })
        .then(data => {
            for (var val in data) {

                var given = moment(data[val].End_date, "YYYY-MM-DD");
                var current = moment().startOf('day');
                completion = moment.duration(given.diff(current)).asDays();
                if (completion < 1) {
                    completion = 0;
                }

                var c = {
                    "Description": data[val].Description,
                    "End_date": data[val].End_date,
                    "Need_remote": data[val].Need_remote,
                    "Project_id": data[val].Project_id,
                    "Project_location": data[val].Project_location,
                    "Project_state": data[val].Project_state,
                    "Project_name": data[val].Project_name,
                    "Start_date": data[val].Start_date,
                    "Status": data[val].Status,
                    "Remaining": completion,
                    "RequirementsTbs": data[val].RequirementsTbs,
                    "RequirementsCount": data[val].RequirementsTbs.length
                }
                projectdata.push(c);

            }
            res.send(projectdata);
        })
        .catch(err => {
            console.log(err);
        });
};

exports.searchProjectById = (req, res) => {
    const Project_id = req.body.Project_id;

    projectTb.findByPk(Project_id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });
};


exports.myResources = (req, res) => {
    const Company_id = req.body.Company_id;
    let projectdata = [];
    let completion = 0;
    const today = moment();


    projectTb.findAll({
            where: { Company_id: Company_id },
            include: {
                model: companyTb,
                required: true
            }
        })
        .then(data => {
            for (var val in data) {

                var given = moment(data[val].End_date, "YYYY-MM-DD");
                var current = moment().startOf('day');
                completion = moment.duration(given.diff(current)).asDays();
                if (completion < 1) {
                    completion = 0;
                }

                var c = {
                    "Description": data[val].Description,
                    "End_date": data[val].End_date,
                    "Need_remote": data[val].Need_remote,
                    "Project_id": data[val].Project_id,
                    "Project_location": data[val].Project_location,
                    "Project_name": data[val].Project_name,
                    "Start_date": data[val].Start_date,
                    "Status": data[val].Status,
                    "Remaining": completion,
                }
                projectdata.push(c);

            }
            res.send(projectdata);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};


exports.createNewAssignment = async(req, res) => {
    console.log(req.body);
    const requirementData = {
        "Company_id": req.body.Company_id,
        "Project_id": req.body.Project_id,
        "User_id": req.body.User_id,
        "Requirement_name": req.body.Requirement_name,
        "Week_duration": req.body.Week_duration,
        "Week_must_time": req.body.Week_must_time,
        "Hours_per_week": req.body.Hours_per_week,
        "Hours_per_month": req.body.Hours_per_month,
        "Hours_per_day": req.body.Hours_per_day,
        "No_of_resources": req.body.No_of_resources,
        "Requirements_description": req.body.Requirements_description,
        "Requirement_start": req.body.Start_date,
        "Requirement_end": req.body.End_date,
        "Need_remote": req.body.Need_remote,
        "Requirement_section": req.body.Requirement_section
    };

    const addRequirement = new Promise(async(resolve, reject) => {
        requirementTb.findOne({ where: { Requirement_id: req.body.Requirement_id, Requirement_name: req.body.Requirement_name } }).then(async function(obj) {
            if (obj) {
                console.log('Found Requirement');
                return await obj.update(requirementData).then(function(data) {
                    return resolve(data);
                });
            } else {
                return await requirementTb.create(requirementData)
                    .then(data => {
                        if (data) {
                            return resolve(data);
                        } else {
                            var reason = new Error('Your friend is not Ready');
                            return reject(reason);
                        }
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }
        });
    });
    const isAddedRequirement = async() => {
        const result = await addRequirement;
        console.log('Requirement added successfully');
        return result;
    }
    isAddedRequirement()
        .then(result => {

            res.send(result);
        })
        .catch(err => {
            console.error(err);
            res.send(err);
        })
};


exports.getResourceTechnologiesById = async(req, res) => {
    const Resource_id = req.body.Resource_id;

    await resourceTechnologyTbs.findAll({
            where: { Resource_id: Resource_id },
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};
exports.getResourceDomainsById = async(req, res) => {
    const Resource_id = req.body.Resource_id;

    await resourceDomainTbs.findAll({
            where: { Resource_id: Resource_id },
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};
exports.getResourceRolesById = async(req, res) => {
    const Resource_id = req.body.Resource_id;

    await resourceRoleTbs.findAll({
            where: { Resource_id: Resource_id },
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};
exports.getResourceQualificationsById = async(req, res) => {
    const Resource_id = req.body.Resource_id;

    await resourceEducationTbs.findAll({
            where: { Resource_id: Resource_id },
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};