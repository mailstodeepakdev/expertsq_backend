const db = require("../models");
const companyTb = db.companyTb;
const usersTb = db.user;
const ChatTbs = db.ChatTbs;
const BranchesTb = db.BranchesTb;
const InvoiceTbs = db.InvoiceTbs;
var Sequelize = require("sequelize");
const requirementTb = db.requirement;
const resourceTb = db.resourceTb;
const OfferLetterTbs = db.OfferLetterTbs;
const assignTb = db.assignTb;
const ReleaseResourcesTbs = db.ReleaseResourcesTbs;
var pdfConvert = require("html-pdf");
var fs = require("fs");
const TimesheetTbs = db.TimesheetTbs;
var nodemailer = require('nodemailer');
var handlebars = require('handlebars');
const moment = require('moment');
const Op = db.Sequelize.Op;
var fs = require('fs');
const path = require('path')
const AWS = require('aws-sdk');
const awsConfig = require('../../config/AWS.config');
const BUCKET_NAME = awsConfig.Bucket_Name;
const interviewTb = db.interviewTb;

const s3 = new AWS.S3({
    accessKeyId: awsConfig.Access_Key_ID,
    secretAccessKey: awsConfig.Secret_Access_Key,
});

const SES_DEV = new AWS.SES({
    accessKeyId: awsConfig.Access_Key_ID,
    secretAccessKey: awsConfig.Secret_Access_Key,
    apiVersion: '2010-12-01',
    region: 'ap-south-1'
});


var options = {
    format: "Letter",
    orientation: "portrait",
    border: "10mm",
    header: {
        height: "45mm",
        contents: '<div style="text-align: center;">INVOICE | EXPERTSQ</div>'
    },
    footer: {
        height: "28mm",
        contents: '<div style="text-align: center;">www.expertsq.com</div>'

    }
};
async function getNotificationMailLists(User_email) {
    return await usersTb.findOne({ where: { User_email: User_email } }).then(data => {
        let mails = data.Notification_mails;
        return JSON.parse(mails);
    })

}
async function awsSES(reciverMail, subject, mailBody, templatePath) {

    let ccMails = await getNotificationMailLists(reciverMail);
    ccMails - JSON.stringify(ccMails);

    var params = {
        Destination: { /* required */
            CcAddresses: [
                reciverMail,
                /* more items */
            ],
            ToAddresses: [
                reciverMail,
                /* more items */
            ]
        },
        Message: { /* required */
            Body: { /* required */
                Html: {
                    Charset: "UTF-8",
                    Data: "HTML_FORMAT_BODY"
                },
                Text: {
                    Charset: "UTF-8",
                    Data: mailBody
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: subject
            }
        },
        Source: 'admin@expertsq.com',
        /* required */
        ReplyToAddresses: [
            'admin@expertsq.com',
            /* more items */
        ],
    };

    var sendPromise = SES_DEV.sendEmail(params).promise();

    // Handle promise's fulfilled/rejected states
    sendPromise.then(
        function(data) {
            console.log(data.MessageId);
        }).catch(
        function(err) {
            console.error(err, err.stack);
        });

}

async function sendMail(reciverMail, subject, mailBody, templatePath) {
    const myEmail = "admin@expertsq.com";
    const emailPass = "Moonlight@2021";
    let ccMails = await getNotificationMailLists(reciverMail);

    var readHTMLFile = async function(path, callback) {
        fs.readFile(path, { encoding: 'utf-8' }, async function(err, html) {
            if (err) {
                throw err;
                callback(err);
            } else {
                callback(null, html);
            }
        });
    };
    readHTMLFile(path.resolve(__dirname, templatePath), async function(err, html) {
        var template = await handlebars.compile(html);
        var replacements = {
            mailBody: mailBody,
        };
        var htmlToSend = template(replacements);
        return new Promise((resolve, reject) => {
            let transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: myEmail,
                    pass: emailPass
                }
            });
            var mailOptions = {
                from: myEmail,
                to: reciverMail,
                cc: ccMails,
                subject: subject,
                html: htmlToSend
            };
            let resp = false;

            transporter.sendMail(mailOptions, function(error, info) {
                if (error) {
                    console.log("error is " + error);
                    resolve(false); // or use rejcet(false) but then you will have to handle errors
                } else {
                    console.log('Email sent: ' + info.response);
                    resolve(true);
                }
            });
        })


    });

}

// async function sendMail(reciverMail, subject, mailBody, templatePath) {
//     const myEmail = "admin@expertsq.com";
//     const emailPass = "Moonlight@2021";
//     let ccMails = await getNotificationMailLists(reciverMail);

//     var readHTMLFile = async function(path, callback) {
//         fs.readFile(path, { encoding: 'utf-8' }, async function(err, html) {
//             if (err) {
//                 throw err;
//                 callback(err);
//             } else {
//                 callback(null, html);
//             }
//         });
//     };
//     readHTMLFile(path.resolve(__dirname, templatePath), async function(err, html) {
//         var template = await handlebars.compile(html);
//         var replacements = {
//             mailBody: mailBody,
//         };
//         var htmlToSend = template(replacements);
//         var transporter = await nodemailer.createTransport({
//             service: 'gmail',
//             auth: {
//                 user: myEmail,
//                 pass: emailPass
//             }
//         });

//         var mailOptions = {
//             from: myEmail,
//             to: reciverMail,
//             cc: ccMails,
//             subject: subject,
//             html: htmlToSend
//         };
//         await transporter.sendMail(mailOptions, async function(error, info) {
//             if (error) {
//                 console.log(error);
//             } else {
//                 console.log('Email sent: ' + info.response + " || " + reciverMail);
//             }
//         });
//     });

// }

exports.cronReleaseResourceInvoice = async(req, res) => {
    return await ReleaseResourcesTbs.findAll({
            where: { Cron_status: 'PENDING' },
            include: {
                model: usersTb,
                required: true,
            }
        }).then(async data => {
            return data;
        })
        .catch(err => {
            console.log(err);
        });

};


exports.cronUsersInvoice = async(req, res) => {
    let uniqUsers = [];
    return await assignTb.findAll({
            where: { Approved_status: 'Approved' },
            include: {
                model: usersTb,
                required: true,
            }
        }).then(async data => {
            data.forEach(async element => {
                uniqUsers.push(element.UsersTb.User_id);
            });
            let findDuplicates = uniqUsers.filter((v, i) => uniqUsers.indexOf(v) == i)
            uniqUsers = findDuplicates;
            console.log('Unique for invoice:' + uniqUsers);
            return uniqUsers;
        })
        .catch(err => {
            console.log(err);
        });

};

exports.getInvoices = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    endDate = req.body.mondayCheck;
    startDate = moment(endDate, 'YYYY-MM-DD').subtract(14, 'days').format('YYYY-MM-DD');
    console.log('Start date:' + startDate);
    console.log('EndDate date:' + endDate);
    async function get_Requirements(monday) {
        return await assignTb.findAll({
            where: { User_id: req.body.User_id, Approved_status: 'Approved' },
            include: [{
                model: resourceTb,
                required: true,
                include: [{
                    model: TimesheetTbs,
                    required: true,
                    where: {
                        Working_date: {
                            [Op.between]: [startDate, endDate]
                        },
                        Status: 'Approved'
                    }
                }]
            }, {
                model: requirementTb,
                required: true,
                include: {
                    model: usersTb,
                    required: true,
                    include: [{
                            model: companyTb,
                        },
                        {
                            model: BranchesTb,
                            required: true,
                        }
                    ]
                }
            }]
        });
    }

    async function calculate_Amount(details) {
        if (Object.keys(details).length > 0) {
            var userswithPrice = [];
            details.forEach(element => {
                if (element.ResourceTb !== null) {
                    var resourceID = element.ResourceTb.Resource_id;
                    var Resource_name = element.ResourceTb.Resource_name;
                    var resourceRate = element.ResourceTb.Resource_rate;
                    var resourceCurrency = element.ResourceTb.Resource_currency;

                    if (element.ResourceTb.TimesheetTbs !== null) {
                        var totalHours = 0;
                        let requirementName = element.RequirementsTb.Requirement_name;
                        let Requirement_id = element.RequirementsTb.Requirement_id;
                        let hiringManager = element.RequirementsTb.UsersTb.User_firstname;
                        let hiringManager_mail = element.RequirementsTb.UsersTb.User_email;
                        let hiringgManager_id = element.RequirementsTb.UsersTb.User_id;
                        let hiringManager_company = element.RequirementsTb.UsersTb.CompanyTb.C_full_name;
                        let hiringManager_company_address = element.RequirementsTb.UsersTb.Invoice_address;

                        element.ResourceTb.TimesheetTbs.forEach(async element => {
                            totalHours += Number(element.Working_hours);
                        });
                        var getRandomId = (min = 0, max = 500000) => {
                            min = Math.ceil(min);
                            max = Math.floor(max);
                            const num = Math.floor(Math.random() * (max - min + 1)) + min;
                            return num.toString().padStart(6, "0")
                        };
                        var c = {
                            'Resource_id': resourceID,
                            'Resource_name': Resource_name,
                            'TotalPrice': (resourceRate * totalHours),
                            'Requirement_name': requirementName,
                            'Requirement_id': Requirement_id,
                            'hiringManager': hiringManager,
                            'hiringManager_mail': hiringManager_mail,
                            'hiringgManager_id': hiringgManager_id,
                            'hiringManager_company': hiringManager_company,
                            'hiringManager_company_address': hiringManager_company_address,
                            'totalHours': totalHours,
                            'resourceRate': resourceRate,
                            'resourceCurrency': resourceCurrency,
                            'invoiceid': getRandomId()
                        }
                        userswithPrice.push(c);
                    }


                }
            });
            return userswithPrice;

        }

    }

    async function getLatestMonday() {
        let currentDateObj = new Date();
        currentDateObj.setDate(currentDateObj.getDate() - (currentDateObj.getDay() + 1) % 7);
        currentDateObj = currentDateObj.toLocaleDateString("zh-Hans-CN");
        return currentDateObj;
    }
    async function prepareInvoice(val) {
        val.forEach(async element => {
            var filePaths = path.resolve(__dirname, "../../uploads/templates/invoiceData.handlebars");
            var utc = new Date();

            let comission = Number(element.TotalPrice) / 10;
            let grantTotal = element.TotalPrice + comission;
            var replacements = {
                Resource_name: element.Resource_name,
                TotalPrice: element.TotalPrice,
                Requirement_name: element.Requirement_name,
                hiringManager: element.hiringManager,
                hiringManager_mail: element.hiringManager_mail,
                hiringManager_company: element.hiringManager_company,
                hiringManager_company_address: element.hiringManager_company_address,
                totalHours: element.totalHours,
                resourceRate: element.resourceRate,
                comission: comission + element.resourceCurrency,
                grantTotal: grantTotal + element.resourceCurrency,
                date: utc.toLocaleDateString("zh-Hans-CN"),
                invoiceid: element.invoiceid
            }
            console.log(replacements);

            var filename = "Invoice_" + element.Resource_name + "_" + utc + ".pdf";
            var filepath = "../Invoice_" + element.Resource_name + "_" + utc + ".pdf";
            await htmlloading(filePaths, filepath, filename, element, replacements);


        });
    }
    async function htmlloading(htmlFile, filepath, filename, element, replacements) {
        var readHTMLFile = function(filePaths, callback) {
            fs.readFile(filePaths, { encoding: 'utf-8' }, function(err, html) {
                if (err) {
                    throw err;
                    callback(err);
                } else {
                    callback(null, html);
                }
            });
        };
        readHTMLFile(path.resolve(htmlFile), async function(err, html) {
            var template = handlebars.compile(html);
            var htmlToSend = template(replacements);
            var options = { format: 'A4' };
            pdfConvert.create(htmlToSend, options).toFile(filepath, async function(err, res) {
                if (err) return console.log(err);
                console.log(res);
                await saveInvoice(filepath, filename, element);
            });
        });
    }

    async function saveInvoice(filepath, filename, element) {
        const fileContent = fs.createReadStream(filepath);
        const params = {
            Bucket: BUCKET_NAME + '/Invoices',
            Key: filename,
            Body: fileContent
        };

        s3.upload(params, async function(err, data) {
            if (err) {
                throw err;
            }
            console.log(`Invoice uploaded successfully. ${data.Location}`);
            console.log(data);
            var invoiceBodyMail = '<div> Dear ' + element.hiringManager + ' , <br>   The invoice for your resource ' + element.Resource_name + ' for the assignment ' + element.Requirement_name + ' has been generated.<a href="' + data.Location + '">Click to download</a> </div>';
            var subject = "Invoice | ExpertsQ";
            var templatemail = "../../uploads/templates/invoiceMail.handlebars";
            await sendMail(element.hiringManager_mail, subject, invoiceBodyMail, templatemail);

            var invoiceDBData = {
                Requirement_id: element.Requirement_id,
                Resource_id: element.Resource_id,
                User_id: element.hiringgManager_id,
                Invoice_file: data.Location,
                Invoice_code: element.invoiceid,
                From_date: startDate,
                To_date: endDate
            }
            var checkWhere = {
                Requirement_id: element.Requirement_id,
                Resource_id: element.Resource_id,
                User_id: element.hiringgManager_id,
                From_date: startDate,
                To_date: endDate

            }
            console.log(invoiceDBData);
            await InvoiceTbs.findOne({ where: checkWhere }).then(async function(obj) {
                if (obj) {
                    console.log('Invoice updation...');
                    await InvoiceTbs.update(invoiceDBData, {
                        where: checkWhere
                    });
                } else {
                    console.log('Invoice creation...');
                    await InvoiceTbs.create(invoiceDBData);
                }


            })

        });
    }
    let monday = await getLatestMonday().then(data => {
        console.log('Fetched mondays ..............');
    });

    let requirements = await get_Requirements(monday).then(data => {
        console.log('Fetched requirements ..............');
    });

    let amountData = await calculate_Amount(requirements).then(data => {
        console.log('Fetched amountData ..............');
    });

    let invoiceData = await prepareInvoice(amountData).then(data => {
        console.log('Fetched invoiceData ..............');
    });


    return amountData;
};

exports.initialInvoiceGeneration = async(req, res) => {

    console.log('-------initialInvoiceGeneration-----');
    async function getData() {
        return await assignTb.findOne({
            where: {
                Approved_status: 'Approved',
                Requirement_id: req.body.Requirement_id,
                Resource_id: req.body.Resource_id
            },
            include: [{
                    model: requirementTb,
                    required: true,
                    include: [{
                        model: usersTb,
                        required: true,
                        include: {
                            model: companyTb,
                            required: true,
                        }
                    }, ]
                },
                {
                    model: resourceTb,
                    required: true,
                    include: [{
                            model: usersTb,
                            required: true,
                        },
                        {
                            model: OfferLetterTbs,
                            required: true,
                        }
                    ]
                },
            ]
        });
    }
    async function calculateAmount(element) {
        const dailyHours = 80;
        var totalAmounts = 0;
        var commissionPercentange = 10;
        var gstPercentage = 18;
        var finalAmount = 0;
        var subAmount = 0;
        var gstAmount = 0;
        var commissionPrice = 0;

        var getRandomId = (min = 0, max = 500000) => {
            min = Math.ceil(min);
            max = Math.floor(max);
            const num = Math.floor(Math.random() * (max - min + 1)) + min;
            return num.toString().padStart(6, "0")
        };
        totalAmounts = (element.ResourceTb.Resource_rate * dailyHours);
        commissionPrice = totalAmounts / commissionPercentange;
        subAmount = totalAmounts + commissionPrice;
        gstAmount = Math.round(subAmount / gstPercentage);
        finalAmount = Math.round(subAmount + gstAmount);


        var utc = new Date();
        var formatter = new Intl.DateTimeFormat("fr-CA");
        let newDate = formatter.format(utc);

        var To_date = utc.setDate(utc.getDate() + 3);
        To_date = formatter.format(To_date);


        var invoiceid = getRandomId();
        var replacements = {
            Resource_name: element.ResourceTb.Resource_name,
            TotalPrice: totalAmounts,
            Requirement_name: element.RequirementsTb.Requirement_name,
            hiringManager: element.RequirementsTb.UsersTb.User_firstname,
            hiringManager_mail: element.RequirementsTb.UsersTb.User_email,
            hiringManager_company: element.RequirementsTb.UsersTb.CompanyTb.C_full_name,
            hiringManager_company_address: element.ResourceTb.OfferLetterTbs[0].Invoice_address,
            totalHours: dailyHours,
            resourceRate: element.ResourceTb.Resource_rate,
            comission: commissionPrice + element.ResourceTb.Resource_currency,
            subTotal: subAmount + element.ResourceTb.Resource_currency,
            date: newDate,
            invoiceid: invoiceid,
            gstAmount: gstAmount + element.ResourceTb.Resource_currency,
            grantTotal: finalAmount + element.ResourceTb.Resource_currency,
            To_date: To_date
        }
        console.log('-------REPLACEMENTS-----');
        console.log(replacements);
        console.log('-------REPLACEMENTS-----');


        var filePaths = path.resolve(__dirname, "../../uploads/templates/initialInvoiceData.handlebars");
        var filename = "EQ_Invoice_" + element.ResourceTb.Resource_name + "_" + newDate + ".pdf";
        // var filepath = "../EQ_Invoice_" + element.ResourceTb.Resource_name + "_" + newDate + ".pdf";
        var filepath = path.join(__dirname, "../../uploads/invoices/EQ_Invoice_" + element.ResourceTb.Resource_name + "_" + newDate + ".pdf");
        await htmlloading(filePaths, filepath, filename, element, replacements);


    }

    async function htmlloading(htmlFile, filepath, filename, element, replacements) {
        var readHTMLFile = function(filePaths, callback) {

            fs.readFile(filePaths, { encoding: 'utf-8' }, function(err, html) {
                if (err) {
                    throw err;
                    callback(err);
                } else {
                    callback(null, html);
                }
            });
        };
        readHTMLFile(path.resolve(htmlFile), async function(err, html) {
            var template = handlebars.compile(html);
            var htmlToSend = template(replacements);
            var options = { format: 'A4' };
            pdfConvert.create(htmlToSend, options).toFile(filepath, async function(err, res) {
                if (err) {
                    console.log('Error....................!');
                    return console.log(err);
                }
                await saveInvoice(filepath, filename, element, replacements);
            });
        });
    }

    async function saveInvoice(filepath, filename, element, replacements) {
        console.log('Saving invoice');
        const fileContent = fs.createReadStream(filepath);
        const params = {
            Bucket: BUCKET_NAME + '/Invoices/EQ',
            Key: filename,
            Body: fileContent
        };

        s3.upload(params, async function(err, data) {
            if (err) {
                throw err;
            }
            console.log(`Invoice uploaded successfully. ${data.Location}`);
            var invoiceBodyMail = '<div> Dear ' + element.RequirementsTb.UsersTb.User_firstname + ' , <br>   The invoice for the resource ' + element.ResourceTb.Resource_name + ' for the assignment ' + element.RequirementsTb.Requirement_name + ' has been generated.<a href="' + data.Location + '">Click to download</a> </div>';
            var subject = "Initial Invoice | ExpertsQ";
            var templatemail = "../../uploads/templates/invoiceMail.handlebars";
            await sendMail(element.RequirementsTb.UsersTb.User_email, subject, invoiceBodyMail, templatemail);


            var invoiceDBData = {
                Invoice_file: data.Location,
                Requirement_id: element.RequirementsTb.Requirement_id,
                Resource_id: element.ResourceTb.Resource_id,
                User_id: element.RequirementsTb.UsersTb.User_id,
                Invoice_type: 'INITIAL',
                From_date: replacements.date,
                Invoice_code: replacements.invoiceid,
                To_date: replacements.To_date
            }
            console.log('--------');
            console.log(invoiceDBData);
            console.log('--------');

            await InvoiceTbs.create(invoiceDBData);

        });
    }
    let preapprovedData = await getData();
    let amountData = await calculateAmount(preapprovedData);
    //res.send(preapprovedData);
    return true;
};

exports.removeEarmarking = async(req, res) => {

    async function findResources() {
        return await resourceTb.findAll({
                where: {
                    Resource_status: 'INTERVIEWING'
                },
                include: {
                    model: interviewTb,
                    required: true,
                    where: {
                        Interview_date: {
                            [Op.gt]: moment().add(2, 'days').toDate()
                        }
                    }
                }
            })
            .then(data => {
                return data;
            })
            .catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving tutorials."
                });
            });
    }
    async function removeStatus(resourceLists) {

        resourceLists.forEach(async element => {
            console.log(element.Resource_id);
            var c = {
                'Resource_status': 'AVAILABLE'
            }
            await resourceTb.update(c, {
                where: { Resource_id: element.Resource_id }
            });

        });

    }


    let resourceLists = await findResources();
    let resourceUpdate = await removeStatus(resourceLists);
    return resourceLists;
};


exports.resourceRelease = async(req, res) => {
    console.log(req.body);
    console.log('--------- Resource release checking ---------');

    async function timesheetDataForClosure() {
        let timesheets = [];
        return await TimesheetTbs.findAll({
            where: { Resource_id: req.body.Resource_id, Requirement_id: req.body.Requirement_id, Status: 'Approved' },
            order: [
                ['Working_date', 'ASC']
            ]

        }).then(async data => {
            data.forEach(element => {
                var c = {
                    'Working_date': element.Working_date,
                    'Working_hours': element.Working_hours,
                    'Working_day': element.Working_day,
                }
                timesheets.push(c);
            });
            return timesheets;

        });

    }
    async function dateFormation(timesheets) {

        let n = 0;
        let formatedSheet = [];
        let now = moment();
        let today = moment().format('YYYY-MM-DD');
        let weekDay = now.format('dddd');
        let firstDay = timesheets[0] ? timesheets[0].Working_date : today;
        let count = timesheets.length;
        var fromDate = firstDay;
        var endDate = firstDay;
        var total = 0;
        var c = [];
        console.log('Timesheet entries ' + count);
        for (let i = 0; i < count; i++) {
            total += Number(timesheets[i].Working_hours);

            if (timesheets[i].Working_day == 0 || i == 0) {
                fromDate = timesheets[i].Working_date;
            } else if (timesheets[i].Working_day == 6 || i == (count - 1)) {
                endDate = timesheets[i].Working_date;
                c = {
                    'startDate': timesheets[i - 6].Working_date,
                    'endDate': endDate,
                    'total': total
                }
                formatedSheet.push(c);
                total = 0;

            }
        }
        return formatedSheet;


    }
    async function prepareInvoiceOutward(val) {
        let n = 1;
        let resource = await getResourceData();
        let requirement = await requirementData();
        let offerLetterData = await offerletterDetails();
        let invoiceAddress = offerLetterData.Invoice_address;
        var timesheetRow = "";
        let totalHours = 0;
        let hiringManager = requirement.UsersTb.User_firstname;
        let Resource_name = resource.Resource_name;
        let Requirement_name = requirement.Requirement_name;
        let hiringManager_mail = requirement.UsersTb.User_email;

        var getRandomId = (min = 0, max = 500000) => {
            min = Math.ceil(min);
            max = Math.floor(max);
            const num = Math.floor(Math.random() * (max - min + 1)) + min;
            return num.toString().padStart(6, "0")
        };

        if (val.length > 0) {

            val.forEach(async element => {
                timesheetRow += "<tr>";
                timesheetRow += "<td>" + n + "</td>";
                timesheetRow += "<td>" + element.total + "</td>";
                timesheetRow += "<td> Time-sheet : " + element.startDate + " / " + element.endDate + "</td>";
                timesheetRow += "<td>" + resource.Resource_rate + resource.Resource_currency + "</td>";
                timesheetRow += "<td>" + (resource.Resource_rate * element.total) + resource.Resource_currency + "</td>";
                timesheetRow += "</tr>";
                totalHours += Number(element.total);
                n++;
            });
            var filePaths = path.resolve(__dirname, "../../uploads/templates/fullClosureInvoiceData.handlebars");

            var utc = new Date();
            var formatter = new Intl.DateTimeFormat("fr-CA");
            let newDate = formatter.format(utc);

            let totalPrice = Number(resource.Resource_rate * totalHours);
            let comission = Number(totalPrice) / 10;
            let gstAmount = 0;
            let initDeposit = (resource.Resource_rate * 80);
            let serviceCharge = Number(resource.Resource_rate * totalHours);
            let grantTotal = (initDeposit - serviceCharge);
            let subtotal = grantTotal + comission;
            let Total = subtotal + gstAmount;
            let type = 'OUTWARD ';


            var replacements = {
                timesheetRow: timesheetRow,
                invoiceAddress: invoiceAddress,
                gstAmount: gstAmount,
                subTotal: subtotal + resource.Resource_currency,
                totalHours: totalHours,
                resourceRate: resource.Resource_rate,
                comission: comission + resource.Resource_currency,
                Total: Total + resource.Resource_currency,
                date: newDate,
                Resource_name: Resource_name,
                Requirement_name: Requirement_name,
                hiringManager_mail: hiringManager_mail,
                hiringManager: hiringManager,
                invoiceid: getRandomId(),
                initDeposit: initDeposit + resource.Resource_currency,
                grantTotal: grantTotal + resource.Resource_currency,
                serviceCharge: serviceCharge + resource.Resource_currency,
                type: type,
                Invoice_type: 'RELEASE'
            }
            console.log(replacements);

            var filename = "ClosureInvoice_" + type + resource.Resource_name + "_" + newDate + ".pdf";
            var filepath = "../../ClosureInvoice_" + type + resource.Resource_name + "_" + newDate + ".pdf";
            await htmlloading(filePaths, filepath, filename, replacements);

        }
    }
    async function htmlloading(htmlFile, filepath, filename, replacements) {
        console.log('HTML reading..........');
        var readHTMLFile = async function(filePaths, callback) {
            fs.readFile(filePaths, { encoding: 'utf-8' }, function(err, html) {
                if (err) {
                    throw err;
                    callback(err);
                } else {
                    callback(null, html);
                }
            });
        };
        await readHTMLFile(path.resolve(htmlFile), async function(err, html) {
            console.log('PDF generation..........');
            var template = await handlebars.compile(html);
            var htmlToSend = template(replacements);
            var options = { format: 'A4' };
            pdfConvert.create(htmlToSend, options).toFile(filepath, async function(err, res) {
                if (err) return console.log(err);
                console.log(res);
                await saveInvoice(filepath, filename, replacements);
            });
        });
    }

    async function saveInvoice(filepath, filename, replacements) {
        console.log('Invoice processing..........');
        const fileContent = fs.createReadStream(filepath);
        const params = {
            Bucket: BUCKET_NAME + '/Invoices',
            Key: filename,
            Body: fileContent
        };

        s3.upload(params, async function(err, data) {
            if (err) {
                throw err;
            }
            console.log(`Closure Invoice uploaded successfully. ${data.Location}`);
            console.log(data);
            var invoiceBodyMail = '<div> Dear ' + replacements.hiringManager + ' , <br>   The invoice for your resource ' + replacements.Resource_name + ' for the assignment ' + replacements.Requirement_name + ' has been generated.<a href="' + data.Location + '">Click to download</a> </div>';
            var subject = (replacements.Invoice_type == 'RELEASE' ? 'Closure Invoice | ExpertsQ' : replacements.Invoice_type == 'PENALTY' ? 'Penalty Invoice | ExpertsQ' : 'Invoice | ExpertsQ');
            var templatemail = "../../uploads/templates/invoiceMail.handlebars";
            await sendMail(replacements.hiringManager_mail, subject, invoiceBodyMail, templatemail);


            var utc = new Date();
            var formatter = new Intl.DateTimeFormat("fr-CA");
            let fromDate = formatter.format(utc);

            var To_date = utc.setDate(utc.getDate() + 3);
            To_date = formatter.format(To_date);


            var invoiceDBData = {
                Requirement_id: req.body.Requirement_id,
                Resource_id: req.body.Resource_id,
                User_id: req.body.User_id,
                Invoice_file: data.Location,
                Invoice_code: replacements.invoiceid,
                From_date: fromDate,
                To_date: To_date,
                Invoice_type: replacements.Invoice_type
            }
            var checkWhere = {
                Requirement_id: req.body.Requirement_id,
                Resource_id: req.body.Resource_id,
                User_id: req.body.User_id,
                From_date: fromDate,
                To_date: To_date

            }
            console.log(invoiceDBData);
            await InvoiceTbs.findOne({ where: checkWhere }).then(async function(obj) {
                if (obj) {
                    console.log('Invoice updation...');
                    await InvoiceTbs.update(invoiceDBData, {
                        where: checkWhere
                    });
                } else {
                    console.log('Invoice creation...');
                    await InvoiceTbs.create(invoiceDBData);
                }


            })

        });
    }
    async function offerletterDetails() {
        return await OfferLetterTbs.findOne({ where: { Resource_id: req.body.Resource_id, Requirement_id: req.body.Requirement_id } })
    }
    async function getResourceData() {
        return await resourceTb.findOne({
            where: { Resource_id: req.body.Resource_id },
            include: {
                model: usersTb,
                required: true,
                include: {
                    model: companyTb,
                    required: true,
                }
            }
        });
    }
    async function getAddress() {

        return await assignTb.findOne({
            where: {
                Requirement_id: req.body.Requirement_id,
                Resource_id: req.body.Resource_id
            }
        }).then(data => {
            return data.Invoice_address;
        });



    }
    async function prepareInvoiceInward(val) {
        console.log('Calling inward Invoice');
        let n = 1;
        let resource = await getResourceData();
        let requirement = await requirementData();
        let offerLetterData = await offerletterDetails();
        let invoiceAddress = await getAddress();
        var timesheetRow = "";
        let totalHours = 0;
        let listingManager = resource.UsersTb.User_firstname;
        let Resource_name = resource.Resource_name;
        let Requirement_name = requirement.Requirement_name;
        let listingManager_mail = resource.UsersTb.User_email;

        var getRandomId = (min = 0, max = 500000) => {
            min = Math.ceil(min);
            max = Math.floor(max);
            const num = Math.floor(Math.random() * (max - min + 1)) + min;
            return num.toString().padStart(6, "0")
        };


        if (val.length > 0) {
            val.forEach(async element => {
                timesheetRow += "<tr>";
                timesheetRow += "<td>" + n + "</td>";
                timesheetRow += "<td>" + element.total + "</td>";
                timesheetRow += "<td> Time-sheet : " + element.startDate + " / " + element.endDate + "</td>";
                timesheetRow += "<td>" + resource.Resource_rate + resource.Resource_currency + "</td>";
                timesheetRow += "<td>" + (resource.Resource_rate * element.total) + resource.Resource_currency + "</td>";
                timesheetRow += "</tr>";
                totalHours += Number(element.total);
                n++;
            });
            var filePaths = path.resolve(__dirname, "../../uploads/templates/fullClosureInvoiceDataInward.handlebars");

            var utc = new Date();
            var formatter = new Intl.DateTimeFormat("fr-CA");
            let newDate = formatter.format(utc);

            let totalPrice = Number(resource.Resource_rate * totalHours);
            let comission = Number(totalPrice) / 10;
            let gstAmount = 0;
            let serviceCharge = Number(resource.Resource_rate * totalHours);
            let subtotal = serviceCharge - comission;
            let Total = subtotal + gstAmount;
            let type = 'INWARD ';


            var replacements = {
                timesheetRow: timesheetRow,
                invoiceAddress: invoiceAddress,
                gstAmount: gstAmount,
                subTotal: subtotal + resource.Resource_currency,
                totalHours: totalHours,
                resourceRate: resource.Resource_rate,
                comission: comission + resource.Resource_currency,
                Total: Total + resource.Resource_currency,
                date: newDate,
                Resource_name: Resource_name,
                Requirement_name: Requirement_name,
                hiringManager_mail: listingManager_mail,
                hiringManager: listingManager,
                invoiceid: getRandomId(),
                serviceCharge: serviceCharge + resource.Resource_currency,
                type: type,
                Invoice_type: 'RELEASE'
            }
            console.log('---------');
            console.log(replacements);
            console.log('---------');

            var filename = "ClosureInvoice_" + type + resource.Resource_name + "_" + newDate + ".pdf";
            var filepath = "../ClosureInvoice_" + type + resource.Resource_name + "_" + newDate + ".pdf";
            await htmlloading(filePaths, filepath, filename, replacements);

        }
    }
    async function checkPendingTimesheets() {
        return await TimesheetTbs.count({
            where: {
                Resource_id: req.body.Resource_id,
                Requirement_id: req.body.Requirement_id,
                Status: {
                    [Op.ne]: 'Approved'
                }
            }
        })
    }
    async function requirementData() {
        return await requirementTb.findOne({
            where: { Requirement_id: req.body.Requirement_id },
            include: {
                model: usersTb,
                required: true,
                include: {
                    model: companyTb,
                    required: true
                }
            }
        });

    }
    async function sendWarningMail() {
        let requirementDetails = await requirementData();

        let hiringManager_mail = requirementDetails.UsersTb.User_email;
        let hiringManager_name = requirementDetails.UsersTb.User_firstname;
        var invoiceBodyMail = "Dear " + hiringManager_name + ",<br>";
        invoiceBodyMail += "You have pending timesheets for approvel. Please login to the app and approve the remaing timesheets.";
        invoiceBodyMail += "Once you are approved all the timesheets, you will recive the Closure invoice.";
        var subject = "Warning! Timesheet approvel pending | ExpertsQ";
        var templatemail = "../../uploads/templates/warningTimesheetApprovel.handlebars";
        await sendMail(hiringManager_mail, subject, invoiceBodyMail, templatemail);


    }
    async function penaltyOutward(releaseData) {
        let n = 1;
        let initHours = 80;
        let gstAmount = 0;

        let resource = await getResourceData();
        let requirement = await requirementData();
        let offerLetterData = await offerletterDetails();
        let invoiceAddress = offerLetterData.Invoice_address;
        let hiringManager = requirement.UsersTb.User_firstname;
        let Resource_name = resource.Resource_name;
        let Requirement_name = requirement.Requirement_name;
        let hiringManager_mail = requirement.UsersTb.User_email;
        let unitPrice = resource.Resource_rate;
        let amount = (initHours * unitPrice);
        let comission = Number(amount) / 10;
        let serviceCharge = (amount + comission);
        let subtotal = serviceCharge;
        let Total = subtotal + gstAmount;
        let penaltyReason = releaseData.Release_reason;
        let type = 'OUTWARD ';

        var getRandomId = (min = 0, max = 500000) => {
            min = Math.ceil(min);
            max = Math.floor(max);
            const num = Math.floor(Math.random() * (max - min + 1)) + min;
            return num.toString().padStart(6, "0")
        };
        var filePaths = path.resolve(__dirname, "../../uploads/templates/penealtyInvoiceData.handlebars");
        var utc = new Date();
        var formatter = new Intl.DateTimeFormat("fr-CA");
        let newDate = formatter.format(utc);


        var replacements = {
            invoiceAddress: invoiceAddress,
            penaltyReason: penaltyReason,
            unitPrice: unitPrice,
            initHours: initHours,
            gstAmount: gstAmount,
            amount: amount,
            subTotal: subtotal + resource.Resource_currency,
            comission: comission + resource.Resource_currency,
            Total: Total + resource.Resource_currency,
            date: newDate,
            Resource_name: Resource_name,
            Requirement_name: Requirement_name,
            hiringManager_mail: hiringManager_mail,
            hiringManager: hiringManager,
            invoiceid: getRandomId(),
            serviceCharge: serviceCharge + resource.Resource_currency,
            type: type,
            Invoice_type: 'PENALTY'
        }
        console.log(replacements);

        var filename = "PenaltyInvoice_" + type + resource.Resource_name + "_" + newDate + ".pdf";
        var filepath = "../../PenaltyInvoice_" + type + resource.Resource_name + "_" + newDate + ".pdf";
        await htmlloading(filePaths, filepath, filename, replacements);

    }

    async function penaltyInward(releaseData) {
        console.log('Calling   penaltyInward');

        let n = 1;
        let initHours = 80;
        let gstAmount = 0;

        let resource = await getResourceData();
        let requirement = await requirementData();
        let invoiceAddress = await getAddress();
        let Resource_name = resource.Resource_name;
        let Requirement_name = requirement.Requirement_name;
        let unitPrice = resource.Resource_rate;
        let amount = (initHours * unitPrice);
        let comission = Number(amount) / 10;
        let serviceCharge = (amount + comission);
        let subtotal = serviceCharge;
        let Total = subtotal + gstAmount;
        let penaltyReason = releaseData.Release_reason;
        let type = 'INWARD ';
        let listingManager = resource.UsersTb.User_firstname;
        let listingManager_mail = resource.UsersTb.User_email;

        var getRandomId = (min = 0, max = 500000) => {
            min = Math.ceil(min);
            max = Math.floor(max);
            const num = Math.floor(Math.random() * (max - min + 1)) + min;
            return num.toString().padStart(6, "0")
        };
        var filePaths = path.resolve(__dirname, "../../uploads/templates/penealtyInvoiceDataInward.handlebars");

        var utc = new Date();
        var formatter = new Intl.DateTimeFormat("fr-CA");
        let newDate = formatter.format(utc);



        var replacements = {
            invoiceAddress: invoiceAddress,
            gstAmount: gstAmount,
            amount: amount,
            unitPrice: unitPrice,
            subTotal: subtotal + resource.Resource_currency,
            initHours: initHours,
            resourceRate: resource.Resource_rate,
            penaltyReason: penaltyReason,
            comission: comission + resource.Resource_currency,
            Total: Total + resource.Resource_currency,
            date: newDate,
            Resource_name: Resource_name,
            Requirement_name: Requirement_name,
            hiringManager_mail: listingManager_mail,
            hiringManager: listingManager,
            invoiceid: getRandomId(),
            serviceCharge: serviceCharge + resource.Resource_currency,
            type: type,
            Invoice_type: 'PENALTY'
        }
        console.log('---------');
        console.log(replacements);
        console.log('---------');

        var filename = "PenaltyClosureInvoice_" + type + resource.Resource_name + "_" + newDate + ".pdf";
        var filepath = "../PenaltyClosureInvoice_" + type + resource.Resource_name + "_" + newDate + ".pdf";
        await htmlloading(filePaths, filepath, filename, replacements);
    }
    async function releaseType() {
        await ReleaseResourcesTbs.findOne({
            where: {
                Resource_id: req.body.Resource_id,
                Requirement_id: req.body.Requirement_id
            }
        }).then(async data => {
            if (data.Release_type == 'FULL') {
                await prepareInvoiceOutward(data);
                await prepareInvoiceInward(data);
                await cronStatusUpdate();
            } else if (data.Release_type == 'SHORT') {
                await penaltyOutward(data);
                await penaltyInward(data);
                await cronStatusUpdate();

            } else {
                console.log('Invalid resource release type!');
            }

        });
    }

    async function cronStatusUpdate() {
        var cronStatus = {
            'Cron_status': 'COMPLETED'

        }
        await ReleaseResourcesTbs.update(cronStatus, {
            where: {
                Requirement_id: req.body.Requirement_id,
                Resource_id: req.body.Resource_id
            }
        });
    }
    let pendingTimeSheetCount = await checkPendingTimesheets();

    if (pendingTimeSheetCount == 0) {
        console.log('  Timesheet looks good...........');
        let timesheets = await timesheetDataForClosure().then(async data => {
            console.log('timesheets fun' + data);
            return data;
        });
        await dateFormation(timesheets).then(async data => {
            console.log('dateFormation fun' + data);
            await releaseType();
        });

    } else {
        console.log('Pending Timesheet Approvels...........');
        await sendWarningMail();
    }


};