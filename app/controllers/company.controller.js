const db = require("../models");
const companyTb = db.companyTb;
const TermsAndConditionsTbs = db.TermsAndConditionsTbs;
const locationsTb = db.locations;
const usersTb = db.user;
const userrolesTb = db.userroles;
const BranchesTb = db.BranchesTb;
const bankdetailsTb = db.bankdetailsTb;
const TempTbs = db.TempTbs;
const GovermentTbs = db.GovermentTbs;
const Op = db.Sequelize.Op;
const sequelize = db.sequelize;
const resourceTb = db.resourceTb;
const bcrypt = require("bcrypt");
var nodemailer = require("nodemailer");
var handlebars = require("handlebars");

const IncomingForm = require("formidable").IncomingForm;
var fs = require("fs");
const path = require("path");
const AWS = require("aws-sdk");
const awsConfig = require("../../config/AWS.config");
const BUCKET_NAME = awsConfig.Bucket_Name;

const s3 = new AWS.S3({
    accessKeyId: awsConfig.Access_Key_ID,
    secretAccessKey: awsConfig.Secret_Access_Key,
});

async function getNotificationMailLists(User_email) {
    return await usersTb.findOne({ where: { User_email: User_email } }).then(data => {
        if (data) {
            let mails = data.Notification_mails;
            return JSON.parse(mails);
        }
    })

}

async function sendMail(reciverMail, subject, mailBody, templatePath) {
    const myEmail = "admin@expertsq.com";
    const emailPass = "Moonlight@2021";
    let ccMails = await getNotificationMailLists(reciverMail);

    var readHTMLFile = function(path, callback) {
        fs.readFile(path, { encoding: "utf-8" }, function(err, html) {
            if (err) {
                throw err;
                callback(err);
            } else {
                callback(null, html);
            }
        });
    };
    readHTMLFile(path.resolve(__dirname, templatePath), function(err, html) {
        var template = handlebars.compile(html);
        var replacements = {
            mailBody: mailBody,
        };
        var htmlToSend = template(replacements);
        var transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
                user: myEmail,
                pass: emailPass,
            },
        });

        var mailOptions = {
            from: myEmail,
            to: reciverMail,
            cc: ccMails,
            subject: subject,
            html: htmlToSend,
        };
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log("Email sent: " + info.response + " || " + reciverMail);
            }
        });
    });
}

exports.findAll = (req, res) => {
    companyTb
        .findAll()
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials.",
            });
        });
};
exports.findOne = (req, res) => {
    const Company_id = req.params.Company_id;

    companyTb
        .findByPk(Company_id)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + Company_id,
            });
        });
};
exports.update = (req, res) => {
    const Company_id = req.params.Company_id;

    companyTb
        .update(req.body, {
            where: { Company_id: Company_id },
        })
        .then((num) => {
            if (num == 1) {
                res.send({
                    message: "Updated successfully.",
                });
            } else {
                res.send({
                    message: `Cannot update Tutorial with id=${Company_id}. Maybe Tutorial was not found or req.body is empty!`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + Company_id,
            });
        });
};
exports.delete = async(req, res) => {
    const Company_id = req.params.Company_id;

    companyTb
        .destroy({
            where: { Company_id: Company_id },
        })
        .then((num) => {
            if (num == 1) {
                res.send({
                    message: "Deleted successfully!",
                });
            } else {
                res.send({
                    message: `Cannot delete   with id=${Company_id}. Maybe it was not found!`,
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: "Could not delete   with id=" + Company_id,
            });
        });
};

exports.getRoles = (req, res) => {
    userrolesTb
        .findAll()
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials.",
            });
        });
};
exports.addBranch = (req, res) => {

    console.log(req.body);
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }

    const branch = {
        Company_city: req.body.Company_city,
        Company_city_address: req.body.Company_city_address,
        Company_gmap: req.body.Company_gmap,
        Company_GSTIN: req.body.Company_GSTIN,
        Company_state: req.body.Company_state,
        Company_id: req.body.Company_id,
        Locations_id: req.body.Location_id,
        User_id: req.body.User_id,
        Status: 'ACTIVE',
    };

    // Save Tutorial in the database
    BranchesTb.create(branch)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            var c = {
                status: "Failed",
            };
            res.send(c);
        });
};

exports.getBranches = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }

    BranchesTb.findAll({ where: { Company_id: req.body.Company_id } })
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            var c = {
                Status: "Failed",
            };
            res.send(c);
        });
};

exports.addBank = (req, res) => {
    // Validate request
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }

    const bank = {
        Bank_name: req.body.Bank_name,
        Bank_branch: req.body.Bank_branch,
        Bank_accountNumber: req.body.Bank_accountNumber,
        Bank_address: req.body.Bank_address,
        Bank_IFSC: req.body.Bank_IFSC,
        Company_id: req.body.Company_id,
        User_id: req.body.User_id,
    };

    // Save Tutorial in the database
    bankdetailsTb
        .create(bank)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            var c = {
                status: "Failed",
            };
            res.send(c);
        });
};
exports.getBanks = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }

    bankdetailsTb
        .findAll({ where: { Company_id: req.body.Company_id } })
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            var c = {
                Status: "Failed",
            };
            res.send(c);
        });
};

exports.updatePreferences = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }
    const preferences = {
        Enable_masking: req.body.Enable_masking,
        Freelancers: req.body.Freelancers,
        Tiers_maching: JSON.stringify(req.body.Tiers_maching),
    };
    console.log(preferences);

    companyTb
        .update(preferences, {
            where: { Company_id: req.body.Company_id },
        })
        .then((num) => {
            if (num == 1) {
                res.send({
                    Status: "Success",
                });
            } else {
                res.send({
                    Status: "Falied",
                });
            }
        })
        .catch((err) => {
            res.status(500).send({
                message: err,
            });
        });
};

exports.getPreferences = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }

    companyTb
        .findOne({ where: { Company_id: req.body.Company_id } })
        .then((data) => {
            console.log(data);
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            var c = {
                Status: "Failed",
            };
            res.send(c);
        });
};

exports.addGovIds = async(req, res) => {
    // Validate request
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }

    const gov = {
        Company_TAN: req.body.Company_TAN,
        Company_CIN: req.body.Company_CIN,
        Company_PAN: req.body.Company_PAN,
        Company_id: req.body.Company_id,
        User_id: req.body.User_id,
    };
    async function insertGov() {
        return GovermentTbs.findOne({
            where: { Company_id: req.body.Company_id },
        }).then(function(obj) {
            // update
            if (obj) return obj.update(gov);
            // insert
            return GovermentTbs.create(gov);
        });
    }
    let govData = await insertGov();
    res.send(govData);
};

exports.getGovernmentData = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }

    GovermentTbs.findOne({ where: { Company_id: req.body.Company_id } })
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            var c = {
                Status: "Failed",
            };
            res.send(c);
        });
};

exports.addLocation = (req, res) => {
    console.log(req.body);
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }

    const locations = {
        Locations_name: req.body.Locations_name,
        Location_country: req.body.Locations_country,
        Location_pincode: req.body.Locations_pincode,
        Location_state: req.body.Locations_state,
        Company_id: req.body.Company_id,
        User_id: req.body.User_id,
        Location_category: req.body.Location_category ?
            req.body.Location_category : "LOCAL",
    };

    locationsTb
        .create(locations)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            var c = {
                status: "Failed",
            };
            res.send(c);
        });
};

exports.updateLocations = (req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }

    const locations = {
        Locations_name: req.body.Locations_name,
        Location_country: req.body.Locations_country,
        Location_pincode: req.body.Locations_pincode,
        Location_state: req.body.Locations_state,
        User_id: req.body.User_id,
        Location_category: req.body.Location_category ?
            req.body.Location_category : "LOCAL",
    };

    locationsTb
        .update(locations, {
            where: { User_id: req.body.User_id, Location_category: "PRIMARY" },
        })
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            var c = {
                status: "Failed",
            };
            res.send(c);
        });
};

exports.getLocation = (req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }

    locationsTb
        .findAll({
            where: { User_id: req.body.User_id, Location_category: "PRIMARY", Location_status: "ACTIVE" },
        })
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            var c = {
                Status: "Failed",
            };
            res.send(c);
        });
};

exports.companyRegistration = async(req, res) => {
    console.log(req.body);
    async function completeRegistration() {
        var c = {
            Complete_registration: 1,
            AgreeTerms: req.body.Agree_two
        };
        await companyTb
            .update(c, { where: { Company_id: req.body.Company_id } })
            .then((num) => {
                res.send({ status: num });
            });
    }
    await completeRegistration();
};

exports.getCompanyBank = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }
    bankdetailsTb
        .findOne({ where: { Company_id: req.body.Company_id, Bank_category: "primary" } })
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            var c = {
                Status: "Failed",
            };
            res.send(c);
        });
};

exports.updateCompanyBank = (req, res) => {
    // Validate request
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }

    const bank = {
        Bank_name: req.body.Bank_name,
        Bank_branch: req.body.Bank_branch,
        Bank_accountNumber: req.body.Bank_accountNumber,
        Bank_address: req.body.Bank_address,
        Bank_IFSC: req.body.Bank_IFSC,
        User_id: req.body.User_id,
        Bank_category: req.body.Bank_category,
    };

    bankdetailsTb
        .update(bank, {
            where: {
                Bank_category: "primary",
                Bank_id: req.body.bank_id,
            },
        })
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            var c = {
                status: "Failed",
            };
            res.send(c);
        });
};

exports.tempStorage = async(req, res) => {
    if (req.body.profileForm.hasOwnProperty("User_email")) {
        console.log(req.body);

        const tempData = {
            User_salutation: req.body.profileForm.User_salutation,
            User_firstName: req.body.profileForm.User_firstName,
            User_secondName: req.body.profileForm.User_secondName,
            User_phone: req.body.profileForm.User_phone,
            User_email: req.body.profileForm.User_email,
            User_password: req.body.profileForm.User_password,
            Delegate_firstName: req.body.controllerName == "Delegate_firstName" ?
                req.body.controllerValue : "",
        };

        async function inserttempData() {
            return TempTbs.findOne({
                where: { User_email: req.body.profileForm.User_email },
            }).then(async function(obj) {
                if (obj) {
                    if (req.body.profileForm.User_password === obj.User_password) {
                        return await updateTemp();
                    } else {
                        return false;
                    }
                } else {
                    return TempTbs.create(tempData);
                }
            });
        }
        async function updateTemp() {
            var cname = req.body.controllerName;
            var cvalue = req.body.controllerValue;
            var jsonData = {};
            jsonData[cname] = cvalue;
            return await TempTbs.update(jsonData, {
                where: { User_email: req.body.profileForm.User_email },
            });
        }

        async function getData() {
            return await TempTbs.findOne({
                where: { User_email: req.body.profileForm.User_email },
            });
        }

        let temp = await inserttempData();
        let Data = await getData();
        res.send(Data);
    }
};

exports.sendtoDelegate = async(req, res) => {
    console.log(req.body);
    const transactions = await sequelize.transaction();
    var Company_id = req.body.Company_id
    console.log(req.body);
    var contractOwnerName = "";
    var contractOwnerEmail = "";
    var delegateEmail = "";
    var delegateName = "";

    var delegateErrors = "";




    async function contractownerData() {
        console.log("-----------------------");
        console.log(req.body);
        console.log("-----------------------");
        contractOwnerName =
            req.body.profileForm.User_salutation + req.body.profileForm.User_firstname;
        contractOwnerEmail = req.body.profileForm.User_email;

        console.log("-----------------------");
        console.log(contractOwnerName);
        console.log("-----------------------");

    }

    async function createDelegate(delegateData, passwordHash) {
        delegateName =
            delegateData.Delegate_salutation + delegateData.Delegate_firstName;
        delegateEmail = delegateData.Delegate_email;
        var data = {
            User_roles_id: 3,
            User_salutation: delegateData.Delegate_salutation,
            User_email: delegateData.Delegate_email,
            User_firstname: delegateData.Delegate_firstName,
            User_secondname: delegateData.Delegate_secondName,
            User_phonenumber: delegateData.Delegate_phone,
            User_designation: delegateData.Delegate_designation,
            User_password: passwordHash,
            User_location: delegateData.Delegate_location,
            Company_id: Company_id
        };
        return await usersTb
            .create(data, { transaction: transactions })
            .then(async(data) => {
                console.log("DELEGATE CREATION SUCCESSFUL....");
                return data;
            })
            .catch((err) => {
                console.log(err);
                delegateErrors = "Invalid / Duplicate Delegate Details!";
            });
    }

    async function passwordGenerator(newPassword) {
        return bcrypt.hashSync(newPassword, 10);
    }

    var newPassword = "Eq@" + Math.floor(Math.random() * (1000000 - 10000 + 10)) + 1000;
    console.log(newPassword);
    let passwordTwo = await passwordGenerator(newPassword);
    try {
        await contractownerData();
        let delegate = await createDelegate(req.body.delegateForm, passwordTwo);


        if (delegate) {
            await transactions.commit();
            var subject = "Account Creation Completed | Expertsq";
            var dBody = "Dear " + delegateName + ",<br>";
            dBody += contractOwnerName +
                " has initiated a registration of your Company on ExpertsQ platform and has added you as a delegate to complete the registration formalities on his/her behalf. Please log in to the application using the following credentials";
            dBody +=
                "<br> Username : " + delegateEmail + "<br> Password : " + newPassword;
            dBody +=
                "<br> <i>Please remember to change the password once you login</i>";
            var templatemail =
                "../../uploads/templates/accountCreationMail.handlebars";
            await sendMail(delegateEmail, subject, dBody, templatemail);
            res.send(delegate);
        } else {
            await transactions.rollback();
            res.send({ status: false });
        }
    } catch (error) {
        console.log(error);
        await transactions.rollback();
        res.send({ status: false });
    }
};

exports.addBankAccountPrimary = (req, res) => {
    // Validate request
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }

    const bank = {
        Bank_name: req.body.Bank_name,
        Bank_branch: req.body.Bank_branch,
        Bank_accountNumber: req.body.Bank_accountNumber,
        Bank_address: req.body.Bank_address,
        Bank_IFSC: req.body.Bank_IFSC,
        User_id: req.body.User_id,
        Bank_category: req.body.Bank_category,
    };

    // Save Tutorial in the database
    bankdetailsTb
        .create(bank)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            var c = {
                status: "Failed",
            };
            res.send(c);
        });
};


exports.forgotPassword = async(req, res) => {
    if (!req.body.User_email) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }
    async function findUsers() {
        return await usersTb.findOne({
            where: { User_email: req.body.User_email },
        });
    }
    async function findResources() {
        return await resourceTb.findOne({
            where: { Resource_Email: req.body.User_email },
        });
    }
    async function passwordResetUser(User_firstName, User_email) {
        var newPassword = "Eq@" + Math.floor(Math.random() * (1000000 - 10000 + 10)) + 1000;
        let hashPassword = await passwordHashing(newPassword);
        console.log("User : password reset" + newPassword);

        var subject = "Password reset | Expertsq";
        var dBody = "Dear " + User_firstName + ",<br>";
        dBody += "Please use the below credentials to login.";
        dBody += "<br> Username : " + User_email + "<br> Password : " + newPassword;
        var templatemail = "../../uploads/templates/passwordResetMail.handlebars";
        await updateUserPassword(hashPassword, User_email);
        await sendMail(User_email, subject, dBody, templatemail);
    }
    async function passwordHashing(newPassword) {
        return bcrypt.hashSync(newPassword, 10);
    }
    async function updateUserPassword(hashPassword, User_email) {
        var passwordData = {
            User_password: hashPassword,
        };
        return await usersTb.update(passwordData, {
            where: { User_email: User_email },
        });
    }

    async function updateResourcePassword(hashPassword, Resource_Email) {
        var passwordData = {
            Resource_Password: hashPassword,
        };
        return await resourceTb.update(passwordData, {
            where: { Resource_Email: Resource_Email },
        });
    }

    async function passwordResetResource(User_firstName, Resource_Email) {
        var newPassword = "Eq@" + Math.floor(Math.random() * (1000000 - 10000 + 10)) + 1000;
        let hashPassword = await passwordHashing(newPassword);
        console.log("Resource : password reset" + newPassword);

        var subject = "Password reset | Expertsq";
        var dBody = "Dear " + User_firstName + ",<br>";
        dBody += "Please use the below credentials to login.";
        dBody +=
            "<br> Username : " + Resource_Email + "<br> Password : " + newPassword;
        var templatemail = "../../uploads/templates/passwordResetMail.handlebars";
        await updateResourcePassword(hashPassword, Resource_Email);
        await sendMail(Resource_Email, subject, dBody, templatemail);
    }

    let isUser = await findUsers();
    let istheResource = await findResources();
    if (isUser) {
        await passwordResetUser(isUser["User_firstname"], isUser["User_email"]);
    }
    if (istheResource) {
        await passwordResetResource(
            istheResource["Resource_name"],
            istheResource["Resource_Email"]
        );
    }
};

exports.contractOwnerRegistration = async(req, res) => {
    var User_id = Math.floor(Math.random() * (1000000 - 100 + 10)) + 100;

    console.log("-----------------------");
    console.log("User_id" + User_id);
    console.log(req.body.profileForm);
    console.log("-----------------------");

    async function passwordHashingOne(password) {
        return bcrypt.hashSync(password, 10);
    }
    let passwordHash = await passwordHashingOne(
        req.body.profileForm.User_password
    );
    contractOwnerName =
        req.body.profileForm.User_salutation + req.body.profileForm.User_firstName;
    contractOwnerEmail = req.body.profileForm.User_email;



    async function userCreation(Company_id) {
        var data = {
            User_id: User_id,
            User_roles_id: 2,
            User_salutation: req.body.profileForm.User_salutation,
            User_email: req.body.profileForm.User_email,
            User_firstname: req.body.profileForm.User_firstName,
            User_secondname: req.body.profileForm.User_secondName,
            User_designation: req.body.profileForm.User_designation,
            User_location: req.body.profileForm.User_location,
            User_phonenumber: req.body.profileForm.User_phone,
            User_DINnumber: req.body.profileForm.User_DINnumber,
            User_password: passwordHash,
            Company_id: Company_id
        };
        console.log('-------');
        console.log(data);
        return await usersTb
            .create(data)
            .then(async(data) => {
                data['Company_id'] = Company_id;
                console.log("PROFILE CREATION SUCCESSFUL....");
                await governIdCreationTemp(Company_id, User_id);

                var subject = "Account Creation Completed | Expertsq";
                var templatemail =
                    "../../uploads/templates/accountCreationMail.handlebars";
                var cBody = "Dear " + contractOwnerName + ",<br>";
                cBody +=
                    "Thank you for completing the Registration process. Please login to the Application and explore our oppertunities";
                await sendMail(contractOwnerEmail, subject, cBody, templatemail);
                res.send(data);
            })
            .catch((err) => {
                console.log(err);
                res.send({ status: false });
            });
    }

    async function findUser() {
        usersTb.findOne({ where: { User_email: req.body.profileForm.User_email } })
            .then(async data => {
                console.log("EXISTING USER PASSWORD CHECKING....");
                if (data) {
                    if (bcrypt.compareSync(req.body.profileForm.User_password, data.User_password)) {
                        console.log("EXISTING USER FOUND....");
                        res.send({ status: 'FOUND' });
                    } else {
                        console.log("Password wrong" + data.User_password);
                        console.log(passwordHash);
                        let cId = await companyCreationTemp();
                        await userCreation(cId);
                    }
                } else {
                    let cId = await companyCreationTemp();
                    await userCreation(cId);
                }

            })
            .catch(err => {
                console.log(err);
            });
    }
    async function companyCreationTemp() {

        return await companyTb.create().then(data => {
            console.log('COMPNMAY CREATEDID ' + data.Company_id);
            return data.Company_id
        });
    }
    async function governIdCreationTemp(cId, User_id) {
        var c = {
            Company_id: cId,
            User_id: User_id
        }

        return await GovermentTbs.create(c).then(data => {
            console.log('GovermentTbs CREATEDID ' + data.Company_id);
            return data.Company_id
        });
    }

    await findUser();

};

exports.delegateRegistration = async(req, res) => {
    console.log('DELEAGTE CREATION CALL');
    var User_id = Math.floor(Math.random() * (1000000 - 100 + 10)) + 100;
    var newPassword =
        "Eq@" + Math.floor(Math.random() * (1000000 - 10000 + 10)) + 1000;

    console.log("-----------------------");
    console.log("User_id" + User_id);
    console.log(req.body);
    console.log("-----------------------");

    async function passwordGenerator(newPassword) {
        return bcrypt.hashSync(newPassword, 10);
    }

    let passwordHash = await passwordGenerator(newPassword);
    let userName = req.body.delegateForm.Delegate_salutation + req.body.delegateForm.Delegate_firstName;
    let userEmail = req.body.delegateForm.Delegate_email;
    var data = {
        User_id: User_id,
        User_roles_id: 3,
        User_salutation: req.body.delegateForm.Delegate_salutation,
        User_email: req.body.delegateForm.Delegate_email,
        User_firstname: req.body.delegateForm.Delegate_firstName,
        User_secondname: req.body.delegateForm.Delegate_secondName,
        User_designation: req.body.delegateForm.Delegate_designation,
        User_location: req.body.delegateForm.Delegate_location,
        User_phonenumber: req.body.delegateForm.Delegate_phone,
        User_password: passwordHash,
        Company_id: req.body.Company_id
    };
    console.log(data);
    return await usersTb
        .create(data)
        .then(async(data) => {
            console.log("PROFILE CREATION SUCCESSFUL....");

            var subject = "Delegate creation| Expertsq";
            var templatemail = "../../uploads/templates/accountCreationMail.handlebars";
            var cBody = "Dear " + userName + ",<br>";
            cBody += req.body.profileForm.User_salutation + req.body.profileForm.User_firstName + " has created you as a Delegate user in Expertsq application.";
            cBody += "Please login to the Application with the following credentials: ";
            cBody += "<br>Username : " + req.body.delegateForm.Delegate_email + "<br>";
            cBody += "Password : " + newPassword;
            await sendMail(userEmail, subject, cBody, templatemail);
            res.send(data);
        })
        .catch((err) => {
            console.log(err);
            res.send({ status: false });
        });
};

exports.companyCreation = async(req, res) => {
    console.log(req.body);
    var Company_id = req.body.Company_id;
    var C_short_name = Math.floor(Math.random() * (1000000 - 1000 + 10)) + 100;
    var Company_email = Math.floor(Math.random() * (1000000 - 1000 + 10)) + 100;

    async function updateUserCompany(Company_id, C_id) {
        var Companydata = {
            Company_id: Company_id,
        };
        await usersTb.update(Companydata, { where: { User_id: C_id } });
    }

    async function governmentIds(companyData, Company_id, User_id) {
        var data = {
            Company_id: Company_id,
            User_id: User_id,
            Company_TAN: companyData.Company_TAN,
            Company_CIN: companyData.Company_CIN,
            Company_PAN: companyData.Company_PAN,
        };
        return await GovermentTbs.create(data)
            .then((data) => {
                console.log("GOVERNMENT IDS SUCCESSFUL....");
                return true;
            })
            .catch((err) => {
                console.log(err);
                governmentErrors = "Invalid / Duplicate TAN , CIN or PAN Numbers ";
            });
    }
    async function createCompany(companyData) {
        console.log("-----------------------");
        console.log(companyData);
        console.log("-----------------------");
        var data = {
            Company_id: Company_id,
            C_full_name: companyData.Company_fullname ? companyData.Company_fullname : Company_email,
            C_short_name: companyData.Company_shortname ? companyData.Company_shortname : C_short_name,
            No_employees: companyData.Company_noemployees ? companyData.Company_noemployees : 0,
            Company_email: companyData.Company_email ? companyData.Company_email : Company_id,
            Website: companyData.Company_website ? companyData.Company_website : Company_id,
            About: companyData.Company_about ? companyData.Company_about : "",
            Numberof_Locations: companyData.Numberof_Locations ? companyData.Numberof_Locations : 0,
            Enable_masking: companyData.ResourceMasked,
            Freelancers: companyData.EnalbleFreelancers,
            Tiers_maching: JSON.stringify(companyData.Company_tier),
        };
        return await companyTb
            .update(data, { where: { Company_id: Company_id } })
            .then(async(data) => {
                console.log("COMPANY CREATION SUCCESSFUL....");
                console.log(data);

                if (req.body.User_id != 0) {
                    var cData = await updateUserCompany(Company_id, req.body.User_id);
                    console.log("COMPANY UPDATION FOR USER SUCCESSFUL....");
                }
                if (req.body.Delegate_id != 0) {
                    var dData = await updateUserCompany(Company_id, req.body.Delegate_id);
                    console.log("COMPANY UPDATION FOR DELEGATE SUCCESSFUL....");
                }
                await governmentIds(companyData, Company_id, req.body.User_id).then(
                    (num) => {
                        console.log("GOVERNMENTID UPDATION SUCCESSFUL....");
                    }
                );
                if (data) {
                    var cData = await companyTb.findOne({ where: { Company_id: Company_id } });
                    res.send(cData);
                } else {
                    res.send({ status: false });

                }
            })
            .catch((err) => {
                console.log(err);
                res.send({ status: false });
            });
    }

    let company = await createCompany(req.body.companyForm);
};

exports.locationCreation = async(req, res) => {
    console.log(req.body);
    async function createLocation(locationData) {
        console.log("-----------------------");
        console.log(locationData);
        console.log("-----------------------");
        var data = {
            Locations_name: locationData.Locations_name ? locationData.Locations_name : "",
            Location_country: locationData.Locations_country,
            Location_pincode: locationData.Locations_pincode ? locationData.Locations_pincode : 0,
            Location_state: 'INDIA',
            User_id: req.body.User_id,
            Location_category: "PRIMARY",
            Company_id: req.body.Company_id
        };
        return await locationsTb
            .findOne({ where: { Locations_id: req.body.locationForm.Locations_id } })
            .then(function(obj) {
                // update
                if (obj)
                    return obj.update(data);
                // insert
                return locationsTb.create(data);
            })
    }
    if (req.body.locationForm.Locations_name !== '') {
        let Location = await createLocation(req.body.locationForm);
        res.send({ status: true });
    } else {
        res.send({ status: false });
    }
};

exports.bankCreation = async(req, res) => {
    async function createBank(bankData) {
        console.log("-----------------------");
        console.log(bankData);
        console.log("-----------------------");
        var data = {
            Bank_name: bankData.Bank_name,
            Bank_branch: bankData.Bank_Branch,
            Bank_address: bankData.Bank_address,
            Bank_accountNumber: bankData.Bank_accountNumber,
            Bank_IFSC: bankData.Bank_IFSC,
            User_id: req.body.User_id,
            Company_id: req.body.Company_id,
        };
        return await bankdetailsTb
            .create(data)
            .then((data) => {
                console.log("BANK CREATION SUCCESSFUL....");
                res.send(data);
            })
            .catch((err) => {
                console.log(err);
                res.send({ status: false });
            });
    }
    let Bank = await createBank(req.body.bankForm);
};


exports.getAllLocations = async(req, res) => {
    console.log('Location.............');
    console.log(req.body);
    console.log('Location.............');
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }
    await locationsTb.findAll({ where: { Company_id: req.body.Company_id, Location_status: 'ACTIVE' } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};


exports.getTermsAndConditions = (req, res) => {

    TermsAndConditionsTbs.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};
exports.aggreeTerms = (req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }
    var c = {
        "AgreeTerms": 1
    }

    companyTb.update(c, { where: { Company_id: req.body.Company_id } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

exports.branchBankCreation = async(req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }
    var branchBank = {
        "Bank_name": req.body.bankForm.Bank_name,
        "Bank_branch": req.body.bankForm.Bank_Branch,
        "Bank_address": req.body.bankForm.Bank_address,
        "Bank_accountNumber": req.body.bankForm.Bank_accountNumber,
        "Bank_IFSC": req.body.bankForm.Bank_IFSC,
        "Bank_category": 'local',
        "User_id": req.body.User_id,
        "Branches_id": req.body.bankForm.Bank_branch_id,
        "Company_id": req.body.Company_id
    }
    var branchWherte = {
        "Bank_name": req.body.bankForm.Bank_name,
        "Bank_branch": req.body.bankForm.Bank_Branch,
        "Bank_accountNumber": req.body.bankForm.Bank_accountNumber,
        "Bank_IFSC": req.body.bankForm.Bank_IFSC,
        "Bank_category": 'local',
        "Branches_id": req.body.bankForm.Bank_branch_id,
        "Company_id": req.body.Company_id

    }

    await bankdetailsTb
        .findOne({ where: branchWherte })
        .then(async function(obj) {
            if (obj) {
                obj.update(branchBank);
                res.send({ status: 'updated' });

            } else {
                await bankdetailsTb.create(branchBank);
                res.send({ status: 'created' });
            }
        })


};

exports.branchBankLists = (req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }
    bankdetailsTb.findAll({
            where: { Company_id: req.body.Company_id, Bank_category: 'local', Status: 'ACTIVE' },
            include: {
                model: BranchesTb,
                required: true,
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

exports.locationDelete = (req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }
    var delLocation = {
        Location_status: 'INACTIVE'
    }
    locationsTb.update(delLocation, { where: { Locations_id: req.body.Locations_id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    status: true
                });
            } else {
                res.send({
                    status: false
                });

            }
        })
        .catch(err => {
            console.log(err);

        });
};

exports.deleteBranch = (req, res) => {
    console.log(req.body);
    if (!req.body.Branches_id) {
        res.status(400).send({
            message: "Content can not be empty!",
        });
        return;
    }
    var stat = {
        Status: 'INACTIVE'
    }
    BranchesTb.update(stat, { where: { Branches_id: req.body.Branches_id } })
        .then(num => {
            if (num == 1) {
                res.send({
                    status: true
                });
            } else {
                res.send({
                    status: false
                });

            }
        })
        .catch(err => {
            console.log(err);

        });
};
exports.removeLocationDb = (req, res) => {
    console.log(req.body);


    locationsTb.destroy({ where: { User_id: req.body.User_id, Locations_id: req.body.Locations_id } })
        .then(num => {
            res.send({ status: true });

        })
        .catch(err => {
            console.log(err);

        });
};

exports.deleteBankAccount = async(req, res) => {
    console.log(req.body);
    console.log('----');
    let cBank = {
        'Status': 'INACTIVE',
    }
    await bankdetailsTb.update(cBank, { where: { Bank_id: req.body.Bank_id } })
        .then(data => {
            console.log(data);
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });
};
exports.getLocationById = async(req, res) => {
    console.log(req.body);
    console.log('----');

    await locationsTb.findOne({ where: { Locations_id: req.body.Locations_id } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });
};