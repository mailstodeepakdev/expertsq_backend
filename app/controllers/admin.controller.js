const db = require("../models");
const companyTb = db.companyTb;
const cron = require('../../cron');
const InvoiceTbs = db.InvoiceTbs;
const requirementTb = db.requirement;
const resourceTb = db.resourceTb;
const usersTb = db.user;
const sequelize = db.sequelize;

exports.invoiceCronCall = async(req, res) => {
    let f = await cron.initInvoice();
    let g = { values: f };
    res.send(g);
};

exports.invoiceLists = async(req, res) => {

    await InvoiceTbs.findAll({
            include: [{
                model: requirementTb,
                required: true,
                include: [{
                    model: usersTb,
                    required: true,
                }, ]
            }, {
                model: resourceTb,
                required: true,
                include: {
                    model: companyTb,
                    required: true,
                }
            }]
        }).then(async data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });

};

exports.clearData = async(req, res) => {
    console.log(req.body);
    if (!req.body.Admin_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    const transactions = await sequelize.transaction();

    await db.sequelize.transaction(async function() {
        var options = { raw: true, transaction: transactions }

        await sequelize
            .query('SET FOREIGN_KEY_CHECKS = 0', null, options)
            .then(function() {
                return sequelize.query('truncate UsersTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate CompanyTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate BankDetailsTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate ResourceTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate FeedbackTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate RequirementsTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate ProjectsTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate OfferLetterTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate AssignTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate BookmarkTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate BranchAllocationTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate BranchesTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate ComplaintsTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate InterviewTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate SelectedDomainsTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate SelectedQualificationsTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate SelectedTechnologiesTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate TimesheetTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate SelectedRolesTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate Resource_technologiesTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate ExtendTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate Resource_domainTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate Resource_rolesTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate ResourceSkillTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate SelectedQualificationsTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate GovermentTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate InvoiceTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate LocationAllocationTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate LocationsTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate Resource_educationTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate NotificationsTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate PaymentTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate CandidatesTbs', null, options)
            })
            .then(function() {
                return sequelize.query('truncate ReleaseResourcesTbs', null, options)
            })
            .then(function() {
                return sequelize.query('SET FOREIGN_KEY_CHECKS = 1', null, options)
            })
            .then(function() {
                return transactions.commit()
            })
    }).then(function() {
        console.log('Data truncated successfully');
        res.send({ status: true });
    }).catch(function(err) {
        console.log(err);
        res.send({ status: false });

    });

};