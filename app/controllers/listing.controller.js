const db = require("../models");
const companyTb = db.companyTb;
const LManagerTb = db.LManagerTb;
const HManagerTb = db.HManagerTb;
const techcategoryTb = db.techcategory;
const technologyTb = db.technology;
const resourceTb = db.resourceTb;
const usersTb = db.user;
const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt');
const domainTb = db.domainTb;
const projectTb = db.project;
const rolesTb = db.roles;
const TimesheetTbs = db.TimesheetTbs;
const educationTb = db.educationTb;
const resourceRoleTbs = db.resourceRoleTbs;
const resourceDomainTbs = db.resourceDomainTbs;
const resourceTechnologyTbs = db.resourceTechnologyTbs;
const resourceEducationTbs = db.resourceEducationTbs;
const stream_educationTbs = db.stream_educationTbs;
const CandidatesTbs = db.CandidatesTbs;

const SelectedTech = db.SelectedTech;
const SelectedDomains = db.SelectedDomains;
const SelectedQualifications = db.SelectedQualifications;
const SelectedRoles = db.SelectedRoles;
const SelectedQualificationsTb = db.SelectedQualifications;

const MasterTbs = db.MasterTbs;
const SelectedTechTb = db.SelectedTech;
const SelectedDomainsTb = db.SelectedDomains;

const mtech_Tbs = db.mtech_Tbs;
const assignTb = db.assignTb;
const requirementTb = db.requirement;
var fs = require('fs');
var nodemailer = require('nodemailer');
const path = require('path');
var handlebars = require('handlebars');
const IncomingForm = require('formidable').IncomingForm;
const AWS = require('aws-sdk');
const awsConfig = require('../../config/AWS.config');
const OfferLetterTbs = db.OfferLetterTbs;
const BUCKET_NAME = awsConfig.Bucket_Name;
const jobs = require('../controllers/cron.controller');
const { exit } = require("process");
const s3 = new AWS.S3({
    accessKeyId: awsConfig.Access_Key_ID,
    secretAccessKey: awsConfig.Secret_Access_Key,
});
async function getConstants(Category) {
    return await MasterTbs.findOne({ where: { 'Category': Category } });
}

async function getNotificationMailLists(User_email) {
    return await usersTb.findOne({ where: { User_email: User_email } }).then(data => {
        if (data) {
            let mails = data.Notification_mails;
            return JSON.parse(mails);
        }
    })

}
async function sendMails(reciverMail, subject, mailBody, templatePath) {
    const myEmail = "admin@expertsq.com";
    const emailPass = "Moonlight@2021";
    let ccMails = await getNotificationMailLists(reciverMail);

    var readHTMLFile = function(path, callback) {
        fs.readFile(path, { encoding: 'utf-8' }, function(err, html) {
            if (err) {
                throw err;
                callback(err);
            } else {
                callback(null, html);
            }
        });
    };
    readHTMLFile(path.resolve(__dirname, templatePath), function(err, html) {
        var template = handlebars.compile(html);
        var replacements = {
            mailBody: mailBody,
        };
        var htmlToSend = template(replacements);
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: myEmail,
                pass: emailPass
            }
        });

        var mailOptions = {
            from: myEmail,
            to: reciverMail,
            cc: ccMails,
            subject: subject,
            html: htmlToSend
        };
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response + " || " + reciverMail);
            }
        });
    });

}

exports.checkManagerType = async(req, res) => {
    if (!req.body.email) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    async function isListing(email) {
        return await LManagerTb.findOne({
                where: { LManager_email: email },
                include: {
                    model: companyTb,
                    required: true
                },
            }).then(data => {
                if (data.LManager_email === email && bcrypt.compareSync(req.body.password, data.LManager_password)) {
                    var id = data.LManager_id;
                    id = id.toString();
                    var C_id = data.Company_id;
                    C_id = C_id.toString();
                    var auth = {
                        "status": "LM_MANAGER",
                        "LManager_id": id,
                        "Company_id": C_id
                    }
                    var result = new Array();
                    result.push(auth);
                    result.push(data);
                    return result;
                } else {
                    return false;
                }
            })
            .catch(err => {
                return false;
            });
    }
    async function isHiring(email) {
        return await HManagerTb.findOne({
                where: { HManager_email: email },
                include: {
                    model: companyTb,
                    required: true
                }
            }).then(data => {
                if (data.HManager_email === email && bcrypt.compareSync(req.body.password, data.HManager_password)) {
                    var id = data.HManager_id;
                    id = id.toString();
                    var C_id = data.Company_id;
                    C_id = C_id.toString();
                    var auth = {
                        "status": "HM_MANAGER",
                        "HManager_id": id,
                        "Company_id": C_id
                    }
                    var result = new Array();
                    result.push(auth);
                    result.push(data);
                    return result;
                } else {
                    return false;
                }
            })
            .catch(err => {
                return false;
            });
    }

    const LManagerFlag = await isListing(req.body.email);
    const HManagerFlag = await isHiring(req.body.email);

    if (!LManagerFlag && !HManagerFlag) {
        var result = {
            status: 'FALSE'
        }
        res.status(401).send(LManagerFlag);
    } else if (LManagerFlag[0]) {
        res.status(200).send(LManagerFlag);
        return true;
    } else if (HManagerFlag[0]) {
        res.status(200).send(HManagerFlag);
        return true;
    } else {
        res.status(401).send("Contact Admin - Not Working\n");
        return false;

    }
};

exports.updateProfile = async(req, res) => {
    if (!req.body.LManager_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const profileData = {
        LManager_name: req.body.LManager_name,
        LManager_designation: req.body.LManager_designation,
        LManager_phone: req.body.LManager_phone
    };
    const userData = {
        User_email: req.body.User_email,
    }
    async function userMailUpdate() {
        return await usersTb.update(userData, {
                where: { User_id: req.body.User_id }
            }).then(num => {
                if (num == 1) {
                    // res.send(num);
                    return true;
                } else {
                    return false;
                }
            })
            .catch(err => {
                var errorMsg = {
                        error: 0
                    }
                    //res.status(500).send(errorMsg);
                return false;
            });
    }
    async function updateLManager() {
        return await LManagerTb.update(profileData, {
                where: { LManager_id: req.body.LManager_id }
            }).then(num => {
                if (num == 1) {
                    res.send(num);
                    //return true;
                } else {
                    return false;
                }
            })
            .catch(err => {
                var errorMsg = {
                        error: 0
                    }
                    //res.status(500).send(errorMsg);
                return false;
            });


    }
    const isupdateLManager = await updateLManager();
    const isHManagerMailUpdated = await userMailUpdate();

    if (isupdateLManager == true && isHManagerMailUpdated == true) {
        res.send(isHManagerMailUpdated);
        //return true;
    } else {
        return false;
    }

};


exports.getMyCompany = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    async function getCompanyData() {
        return await usersTb.findAll({
            where: {
                User_id: req.body.User_id
            },
            include: {
                model: LManagerTb,
                include: {
                    model: companyTb,
                    required: true
                }
            },

        });
    }
    const myCompanyData = await getCompanyData();
    res.status(200).send(myCompanyData);
    return;
};



exports.createResource = async(req, res) => {

    var form = new IncomingForm();
    form.multiples = true;
    console.log('--------------');
    console.log(form);
    console.log('--------------');

    var newpath = "";
    var newvideoPath = "";
    var Resource_id_db = "";
    var profilePhoto = "";
    var resource_email = "";
    var resource_name = "";

    form.on('video', (field, files) => {
        console.log("in:video");

        var oldvideoPath = files.path;
        newvideoPath = './uploads/' + Math.floor(Math.random() * 200000) + files.name;
        fs.rename(oldvideoPath, newvideoPath, function(err) {
            if (err) throw err;
            console.log("Video uploaded");
        });

    });


    form.on('file', async(field, files) => {

        const uploadFile = async(fileName) => {
            console.log('Uploading Resume ................');
            const fileContent = fs.createReadStream(fileName.path);
            newpath = Math.floor(Math.random() * 100000) + fileName.name;

            const params = {
                Bucket: BUCKET_NAME + '/Resume',
                Key: newpath,
                Body: fileContent
            };

            s3.upload(params, function(err, data) {
                if (err) {
                    throw err;
                }
                console.log(`Resume uploaded. ${data.Location}`);
                newpath = data.Location;

            });

            newpath = "https://photoseq.s3.amazonaws.com/" + BUCKET_NAME + '/Resume/' + newpath;

        };

        const uploadImage = async(fileName) => {
            console.log('Uploading Profile photo ................');
            const fileContent = fs.createReadStream(fileName.path);
            let newpathImage = Math.floor(Math.random() * 100000) + fileName.name;

            const params = {
                Bucket: BUCKET_NAME + '/Profile_Photos',
                Key: newpathImage,
                Body: fileContent
            };

            s3.upload(params, function(err, data) {
                if (err) {
                    throw err;
                }
                console.log(`profilePhoto uploaded. ${data.Location}`);
                newpathImage = data.Location;
                profilePhoto = data.Location;
            });

            profilePhoto = "https://photoseq.s3.amazonaws.com/" + BUCKET_NAME + '/Profile_Photos/' + newpathImage;
        };

        if (files.type == 'application/pdf') {
            await uploadFile(files);
        }
        if (files.type == 'image/jpeg') {
            await uploadImage(files);
        }


    });
    form.parse(req, (err, fields, files) => {

        if (!fields.Company_id) {
            res.status(400).send({
                message: "Content can not be empty!"
            });
            return;
        }

        var newPassword = "Eq@" + Math.floor(Math.random() * (1000000 - 10000 + 10)) + 1000;
        var passwordHash = bcrypt.hashSync(newPassword, 10);
        console.log('RP :' + newPassword);

        async function insertResource() {


            const resourceData = {
                Resource_salutation: fields.Resource_salutation,
                Resource_name: fields.Resource_name,
                Resource_Experience: fields.Resource_Experience,
                Resource_Email: fields.Resource_Email,
                Resource_phone: fields.Resource_Phone,
                Resource_Password: passwordHash,
                Resource_Designation: fields.Resource_Designation,
                Resource_summery: fields.Resource_summery,
                Resource_stack: fields.Resource_stack,
                Resource_status: fields.Resource_status,
                Is_remote: fields.Is_remote,
                Resource_rate: fields.Resource_rate,
                Availability_status: fields.Availability_status,
                Available_from: fields.Available_from,
                Resource_currency: fields.Resource_currency,
                Available_to: fields.Available_to,
                Company_id: fields.Company_id,
                Resource_resume: newpath,
                Intro_video: newvideoPath,
                Created_by: fields.Created_by,
                Resource_location: fields.Resource_location,
                Min_available_from: fields.Min_available_from,
                Min_available_to: fields.Min_available_to,
                Manager_id: (fields.allocatedUser == 0 ? fields.Created_by : fields.allocatedUser),
                Resource_photo: profilePhoto,
            };
            resource_email = fields.Resource_Email;
            resource_name = fields.Resource_name;
            console.log(resourceData);
            return await resourceTb.create(resourceData).then(data => {
                Resource_id_db = data.Resource_id;
                return data;
            }).catch(err => {
                console.log(err);
            });
        }
        insertResource()
            .then(async(data) => {
                var result = "";
                //mail
                var subject = "Expertsq | Account Creation";
                var mailBody = "Dear " + resource_name + ",<br> Greetings from Expertq.<br>Please login to the Application and complete your profile.";
                mailBody += "<br> Username :" + resource_email + "<br>Password :" + newPassword;
                var templatePath = "../../uploads/templates/resourceCreationMail.handlebars";

                await sendMails(resource_email, subject, mailBody, templatePath);
                console.log("Education added");
                result = {
                    status: "Success"
                }
                res.send(result);

            }).catch((err) => {
                console.log(err);

                result = {
                    status: "Failed"
                }
            });
    });
};



exports.getTechnologyParents = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    technologyTb.findAll({
            where: {
                Technology_category_id: 0
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};


exports.deleteResource = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var data = {
        "Resource_active": 0,
    }

    resourceTb.update(data, {
            where: { Resource_id: req.body.Resource_id }
        }).then(num => {
            if (num == 1) {
                res.send({
                    status: true
                });
            } else {
                res.send({
                    message: 'Cannot Delete'
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating  with id"
            });
        });
};


exports.introVideo = (req, res) => {

    var form = new IncomingForm();
    console.log("called from video upload");
    var Resource_id = 0;

    form.parse(req, (err, fields, files) => {
        console.log(fields.Resource_id);
        Resource_id = fields.Resource_id;
    })

    form.on('file', (field, files) => {
        console.log("in");

        const uploadFile = (fileName) => {
            const fileContent = fs.createReadStream(fileName.path);
            newpath = Math.floor(Math.random() * 100000) + fileName.name;

            const params = {
                Bucket: BUCKET_NAME + '/Videos',
                Key: newpath,
                Body: fileContent
            };

            s3.upload(params, function(err, data) {
                if (err) {
                    throw err;
                }
                console.log(`Intro_video uploaded. ${data.Location}`);
                newpath = data.Location;
                var vFile = {
                    "Intro_video": newpath
                }
                resourceTb.update(vFile, {
                        where: { Resource_id: Resource_id }
                    }).then(num => {
                        if (num == 1) {
                            res.send({
                                status: true
                            });
                        }
                    })
                    .catch(err => {
                        console.log(err);
                    });

            });

        };
        uploadFile(files);

    })
    form.parse(req)
};


exports.getDomainLists = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    domainTb.findAll({
            where: { Status: 'APPROVED' },
            order: [
                ['Domain', 'ASC']
            ]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            var c = {
                "Status": "Failed"
            }
            res.send(c);
        });
};

exports.getJobRoleLists = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    rolesTb.findAll({
            where: { Status: 'APPROVED' },
            order: [
                ['Role_name', 'ASC']
            ]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            var c = {
                "Status": "Failed"
            }
            res.send(c);
        });
};

exports.getTechnologyByParent = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    technologyTb.findAll({
            where: {
                Technology_category_id: req.body.Technology_category_id
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            var c = {
                "Status": "Failed"
            }
            res.send(c);
        });
};
exports.addNewTechnology = (req, res) => {
    console.log(req.body);
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var c = {
        'Company_id': req.body.Company_id,
        'User_id': req.body.User_id,
        'Technology_name': req.body.Technology.Technology_name,
        'Technology_category_id': req.body.Technology.Technology_parent,
        'Status': 'PENDING'
    }

    technologyTb.create(c)
        .then(data => {
            var c = {
                "status": true
            }
            res.send(c);
        })
        .catch(err => {
            console.log(err);
            var c = {
                "status": false
            }
            res.send(c);
        });
};
exports.addNewDomain = (req, res) => {
    console.log(req.body);
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var c = {
        'Company_id': req.body.Company_id,
        'User_id': req.body.User_id,
        'Domain': req.body.Domain.Domain_name,
        'Status': 'PENDING'
    }
    domainTb.create(c)
        .then(data => {
            var c = {
                "status": true
            }
            res.send(c);
        })
        .catch(err => {
            console.log(err);
            var c = {
                "status": false
            }
            res.send(c);
        });
};
exports.addNewEducation = (req, res) => {
    console.log(req.body);
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var c = {
        'Company_id': req.body.Company_id,
        'User_id': req.body.User_id,
        'Qualification': req.body.Education.Education,
        'Status': 'PENDING'
    }
    educationTb.create(c)
        .then(data => {
            var c = {
                "status": true
            }
            res.send(c);
        })
        .catch(err => {
            console.log(err);
            var c = {
                "status": false
            }
            res.send(c);
        });
};

exports.addNewRole = (req, res) => {
    console.log(req.body);
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var c = {
        'Company_id': req.body.Company_id,
        'User_id': req.body.User_id,
        'Role_name': req.body.Job_title.Job_title,
        'Status': 'PENDING'
    }
    rolesTb.create(c)
        .then(data => {
            var c = {
                "status": true
            }
            res.send(c);
        })
        .catch(err => {
            console.log(err);
            var c = {
                "status": false
            }
            res.send(c);
        });
};
exports.getTechnologyLists = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    techcategoryTb.findAll({
            include: {
                model: technologyTb,
                required: true,
                where: { Status: 'APPROVED' }
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            var c = {
                "Status": "Failed"
            }
            res.send(c);
        });
};
exports.getEducationLists = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    educationTb.findAll({
            where: { Status: 'APPROVED' },
            order: [
                ['Qualification', 'ASC']
            ]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            var c = {
                "Status": "Failed"
            }
            res.send(c);
        });
};
exports.getEduStreams = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    console.log(req.body.Stream_category + "popop");
    stream_educationTbs.findAll({
            where: {
                Stream_category: req.body.Stream_category
            }
        }, {
            order: [
                ['Stream_name', 'ASC']
            ]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            var c = {
                "Status": "Failed"
            }
            res.send(c);
        });
};
exports.getEduMtech = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    mtech_Tbs.findAll({
            order: [
                ['Mtech_name', 'ASC']
            ]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            var c = {
                "Status": "Failed"
            }
            res.send(c);
        });
};

exports.toogleActive = (req, res) => {
    let data = {
        Resource_active: req.body.Resource_active
    }
    resourceTb.update(data, {
            where: { Resource_id: req.body.Resource_id }
        })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Success"
                });
            } else {
                res.send({
                    message: 'Failed'
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};
exports.updateFrom = (req, res) => {
    let data = {
        Available_from: req.body.Available_from
    }
    console.log(data);
    resourceTb.update(data, {
            where: { Resource_id: req.body.Resource_id }
        })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Success"
                });
            } else {
                res.send({
                    message: 'Failed'
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};
exports.updateTo = (req, res) => {
    let data = {
        Available_to: req.body.Available_to
    }
    resourceTb.update(data, {
            where: { Resource_id: req.body.Resource_id }
        })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Success"
                });
            } else {
                res.send({
                    message: 'Failed'
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};

exports.getResourceData = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    resourceTb.findOne({
            where: {
                Resource_id: req.body.Resource_id,
                Company_id: req.body.Company_id
            },

            include: [{
                model: resourceTechnologyTbs,
                required: false,
            }, {
                model: resourceEducationTbs,
                required: false,
            }, {
                model: resourceDomainTbs,
                required: false,
            }, {
                model: resourceRoleTbs,
                required: false,
            }],
            required: false,

        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            var c = {
                "Status": "Failed"
            }
            res.send(c);
        });
};

exports.profilePhotoChange = async(req, res) => {

    var form = new IncomingForm();
    var newpath = "";
    form.parse(req, (err, fields, files) => {
        console.log(fields.Resource_id);
        const uploadFile = (fileName) => {
            const fileContent = fs.createReadStream(fileName.file.path);
            newpath = Math.floor(Math.random() * 100000) + fileName.file.name;

            const params = {
                Bucket: BUCKET_NAME + '/Profile_Photos',
                Key: newpath,
                Body: fileContent
            };

            s3.upload(params, function(err, data) {
                if (err) {
                    throw err;
                }
                console.log(`Resource_photouploaded. ${data.Location}`);
                newpath = data.Location;
                var proData = {
                    "Resource_photo": newpath
                }
                resourceTb.update(proData, {
                        where: { Resource_id: fields.Resource_id }
                    })
                    .then(num => {
                        console.log("Profile   updated");
                        if (num == 1) {
                            res.send({
                                Status: true
                            });
                        } else {
                            res.send({
                                Status: false
                            });
                        }
                    })
                    .catch(err => {
                        console.log(err);
                    });

            });

        };
        uploadFile(files);






    });
};


exports.editResource = async(req, res) => {

    var form = new IncomingForm();
    var newpath = "";
    const RESUME_BUCKET = BUCKET_NAME + '/Resume';
    console.log(RESUME_BUCKET)

    form.on('file', (field, files) => {
        console.log("in");
        console.log("File size : " + files.size);
        if (files.size > 0) {
            const uploadFile = async(fileName) => {
                const fileContent = fs.createReadStream(fileName.path);
                newpath = Math.floor(Math.random() * 100000) + fileName.name;

                const params = {
                    Bucket: RESUME_BUCKET,
                    Key: newpath,
                    Body: fileContent
                };

                s3.upload(params, function(err, data) {
                    if (err) {
                        throw err;
                    }
                    console.log(`Resume uploaded. ${data.Location}`);
                    newpath = data.Location;
                });
                newpath = "https://photoseq.s3.amazonaws.com/Resume/" + newpath;
            };

            uploadFile(files);
        }
    });



    form.parse(req, (err, fields, files) => {

        if (!fields.Company_id) {
            res.status(400).send({
                message: "Content can not be empty!"
            });
            return;
        }


        async function insertResource() {


            const resourceData = {
                Resource_name: fields.Resource_name,
                Resource_Experience: fields.Resource_Experience,
                Resource_Email: fields.Resource_Email,
                Resource_phone: fields.Resource_Phone,
                Resource_Designation: fields.Resource_Designation,
                Resource_summery: fields.Resource_summery,
                Resource_stack: fields.Resource_stack,
                Resource_status: fields.Resource_status,
                Is_remote: fields.Is_remote,
                Resource_rate: fields.Resource_rate,
                Availability_status: fields.Availability_status,
                Available_from: fields.Available_from,
                Available_to: fields.Available_to,
                Company_id: fields.Company_id,
                Resource_salutation: fields.Resource_salutation,
                Min_available_from: fields.Min_available_from,
                Min_available_to: fields.Min_available_to,
                Manager_id: fields.allocatedUser,
            };
            if (newpath != "") {
                resourceData['Resource_resume'] = newpath
            }
            console.log(resourceData);
            return await resourceTb.update(resourceData, {
                where: { Resource_id: fields.Resource_id }
            });
        }



        async function prepareTechnology(r_id) {
            await resourceTechnologyTbs.destroy({
                where: {
                    Resource_id: fields.Resource_id
                }
            });
            var techologyArr = fields.Technology_List;
            var Resource_Techs = [];
            console.log(JSON.parse(techologyArr));
            for (var tech of JSON.parse(techologyArr)) {
                var techData = {
                    RTechnology_name: tech.Technology,
                    RTechnology_version: tech.Technology_version,
                    RTechnology_level: tech.Technology_level,
                    RTechnology_duration: tech.Technology_experience,
                    Resource_id: r_id,
                }
                Resource_Techs.push(techData);
            }
            return await Resource_Techs;
        }


        async function prepareDomain(r_id) {
            await resourceDomainTbs.destroy({
                where: {
                    Resource_id: fields.Resource_id
                }
            });
            var domainArr = fields.Domain_List;
            var Resource_Domains = [];
            console.log(JSON.parse(domainArr));

            for (var domains of JSON.parse(domainArr)) {
                var domainData = {
                    RDomain: domains.Domain,
                    RDomain_duration: domains.Domain_duration,
                    Resource_id: r_id,
                }
                Resource_Domains.push(domainData);
            }
            return await Resource_Domains;
        }

        async function prepareRole(r_id) {
            await resourceRoleTbs.destroy({
                where: {
                    Resource_id: fields.Resource_id
                }
            });
            var roleArr = fields.Role_List;
            var Resource_Roles = [];
            console.log(JSON.parse(roleArr));

            for (var role of JSON.parse(roleArr)) {
                var roleData = {
                    RRole_name: role.Job_title,
                    RRole_duration: role.Job_duration,
                    Company_id: fields.Company_id,
                    Resource_id: r_id,
                }
                Resource_Roles.push(roleData);

            }
            return await Resource_Roles;
        }

        async function prepareEducation(r_id) {
            await resourceEducationTbs.destroy({
                where: {
                    Resource_id: fields.Resource_id
                }
            });
            var eduArr = fields.Education_List;
            var Education_lists = [];
            console.log(JSON.parse(eduArr));

            for (var edu of JSON.parse(eduArr)) {
                var eduData = {
                    REducation: edu.Education,
                    REducation_passyear: edu.Pass_year,
                    Resource_id: r_id,
                }
                Education_lists.push(eduData);

            }
            return await Education_lists;
        }


        insertResource().then((data) => {
                console.log("Resource added");
                return prepareTechnology(fields.Resource_id);

            })
            .then((data) => {

                resourceTechnologyTbs.bulkCreate(data);
                console.log("Technology added");
                return data;
            })
            .then((data) => {

                return prepareDomain(fields.Resource_id);

            })
            .then((data) => {

                resourceDomainTbs.bulkCreate(data);
                console.log("Domain added");
                return data;

            })
            .then((data) => {

                return prepareRole(fields.Resource_id);

            })
            .then((data) => {

                resourceRoleTbs.bulkCreate(data);
                console.log("Role added");
                return data;

            })
            .then((data) => {

                return prepareEducation(fields.Resource_id);

            })
            .then((data) => {
                resourceEducationTbs.bulkCreate(data);
                console.log("Education added");
                return data;

            }).catch((err) => {
                console.log(err);
            });


        var result = {
            status: "Success"
        }
        res.send(result);
    });
};

exports.resourceListing = async(req, res) => {

    let profilecount = 0;
    let completed = 0;
    await resourceTb.findAll({
            where: {
                Company_id: req.body.Company_id,
            }
        })
        .then(data => {
            let ResourceData = [];

            data.forEach(element => {
                profilecount = Object.keys(element['dataValues']).length;
                completed = 0;
                for (var colName in element['dataValues']) {
                    if (element[colName]) {
                        completed++;
                    } else {
                        console.log("colName" + colName);
                    }
                }
                completed = (completed / profilecount) * 100;

                var c = {
                    "Resource_id": element.Resource_id,
                    "Company_id": element.Company_id,
                    "Resource_name": element.Resource_name,
                    "Resource_Experience": element.Resource_Experience,
                    "Resource_Email": element.Resource_Email,
                    "Resource_phone": element.Resource_phone,
                    "Resource_Designation": element.Resource_Designation,
                    "Resource_summery": element.Resource_summery,
                    "Resource_masked": element.Resource_masked,
                    "Resource_active": element.Resource_active,
                    "Resource_stack": element.Resource_stack,
                    "Resource_status": element.Resource_status,
                    "Is_remote": element.Is_remote,
                    "Resource_rate": element.Resource_rate,
                    "Available_from": element.Available_from,
                    "Available_to": element.Available_to,
                    "Availability_status": element.Availability_status,
                    "Resource_resume": element.Resource_resume,
                    "Resource_photo": element.Resource_photo,
                    "Intro_video": element.Intro_video,
                    "createdAt": element.createdAt,
                    "updatedAt": element.updatedAt,
                    "completed": completed
                }
                ResourceData.push(c);
            });
            res.send(ResourceData);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};


exports.resourceRequests = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    assignTb.findAll({
            where: {
                Approved_status: 'Preapproved',
            },
            include: [{
                    model: resourceTb,
                    required: true,
                    where: {
                        Created_by: req.body.Created_by
                    }
                },
                {
                    model: requirementTb,
                    required: true,
                    include: {
                        model: companyTb,
                        required: true
                    }
                }
            ]
        })
        .then(data => {
            console.log(data);
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

exports.approveResources = async(req, res) => {

    console.log(req.body);
    var approveData = {
        "Approved_status": 'Approved',
        "Approved_by": req.body.Approved_by,
        "Invoice_address": req.body.Invoice_address
    }
    await assignTb.update(approveData, {
        where: {
            Resource_id: req.body.Resource_id,
            Requirement_id: req.body.Requirement_id
        }
    }).then(num => {
        if (num) {
            updateResourceStatus();
        } else {
            res.send({
                Status: false
            });
        }
    }).catch(err => {
        console.log(err);
        res.status(500).send({
            message: "Error updating Tutorial with id=" + id
        });
    });
    async function updateResourceStatus() {
        var resourceStat = {
            "Resource_status": 'SELECTED'
        }
        resourceTb.update(resourceStat, {
            where: {
                Resource_id: req.body.Resource_id
            }
        }).then(num => {
            if (num) {
                jobs.initialInvoiceGeneration(req).then(async data => {
                    console.log('********************');
                    console.log(" job is running for Init invoice...");
                    console.log('********************');
                    return data;
                })
                res.send({
                    Status: true
                });
            } else {
                res.send({
                    Status: false
                });
            }
        }).catch(err => {
            console.log(err);
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });


    }
};

exports.listofApprovedResources = (req, res) => {

    assignTb.findAll({
            where: { Approved_status: 'Approved', Approved_by: req.body.Approved_by },
            include: [{
                    model: resourceTb,
                    required: true
                },
                {
                    model: requirementTb,
                    required: true,
                    include: {
                        model: companyTb,
                        required: true
                    }
                }
            ]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

exports.get_introVideo = (req, res) => {
    resourceTb.findByPk(req.body.Resource_id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });
};

exports.resourceListsHiring = async(req, res) => {
    console.log(req.body);
    await resouirceHiringManager();
    async function resouirceHiringManager() {
        resourceTb.findAll({
                include: [{
                    model: TimesheetTbs,
                    required: true,
                    include: {
                        model: requirementTb,
                        required: false,
                        where: {
                            User_id: req.body.User_id
                        }
                    }
                }, ]
            })
            .then(data => {
                res.send(data);
            })
            .catch(err => {
                console.log(err);
            });
    }
};

exports.resourceLists = async(req, res) => {
    console.log(req.body);

    let lManagerResult = "";
    lManagerResult = await resouirceListingManager();
    console.log(lManagerResult);

    if (lManagerResult) {
        lManagerResult = await resouirceHiringManager();

    }
    res.send(lManagerResult);


    async function resouirceListingManager() {
        return await resourceTb.findAll({
                where: {
                    Created_by: req.body.User_id
                },
                include: {
                    model: TimesheetTbs,
                    required: true,
                    where: {
                        Resource_approvel: 'Approved'
                    }
                }
            })
            .then(data => {
                return data;
            })
            .catch(err => {
                res.status(500).send({
                    message: "Error retrieving Tutorial with id=" + id
                });
            });
    }
    async function resouirceHiringManager() {
        return await resourceTb.findAll({
                include: [{
                    model: TimesheetTbs,
                    required: true,
                    include: {
                        model: requirementTb,
                        required: false,
                        where: {
                            User_id: req.body.User_id
                        }
                    }
                }, ]
            })
            .then(data => {
                return data;
            })
            .catch(err => {
                console.log(err);
            });
    }

};
exports.getTimesheets = (req, res) => {
    TimesheetTbs.findAll({
            where: {
                Resource_id: req.body.Resource_id
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });
};
exports.changeResourceAvailStatus = async(req, res) => {
    if (!req.body.Resource_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    var changestatus = {};

    if (req.body.Status == 'SELECTED') {
        changestatus = {
            Resource_status: 'IN-NOTICE PERIOD'
        }

    } else {
        changestatus = {
            Resource_status: 'SELECTED'
        }

    }

    await resourceTb.update(changestatus, {
            where: { Resource_id: req.body.Resource_id }
        })
        .then(num => {
            if (num == 1) {
                res.send({
                    status: true
                });
            } else {
                res.send({
                    status: false
                });
            }
        })
        .catch(err => {
            console.log(err);
            res.send({
                status: false
            });
        });

};

exports.getAllUsersForAllocation = (req, res) => {
    usersTb.findAll({
            where: {
                Company_id: req.body.Company_id,
                User_id: {
                    [Op.ne]: req.body.User_id
                }
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });
};

exports.resourceMatching = async(req, res) => {

    let matchingRequirements = [];
    let techMatchingData = [];

    async function getRequirement_data(resource) {
        return await requirementTb.findAll().then(data => {
            return data;
        })
    }
    async function getResourceData() {
        return await resourceTb.findOne({
            where: {
                Resource_id: req.body.Resource_id
            },
        }).then(data => {
            return data;
        })
    }

    async function resourceTechLists(r) {
        await resourceTechnologyTbs.findAll({ where: { Resource_id: req.body.Resource_id } }).then(async data => {
            data.forEach(element => {
                resourceTechnologyLists.push(element.RTechnology_name);

            });
        });
    }
    async function resourceDoaminLists(resourceDomainLists) {
        await resourceDomainTbs.findAll({ where: { Resource_id: req.body.Resource_id } }).then(async data => {
            data.forEach(element => {
                resourceDomainLists.push(element.RDomain);

            });
        });
    }

    async function resourceRolesLists(resourceRoleLists) {
        await resourceRoleTbs.findAll({ where: { Resource_id: req.body.Resource_id } }).then(async data => {
            data.forEach(element => {
                resourceRoleLists.push(element.RRole_name);

            });
        });
    }

    async function resourceEducationsLists(resourceEducationLists) {
        await resourceEducationTbs.findAll({ where: { Resource_id: req.body.Resource_id } }).then(async data => {
            data.forEach(element => {
                resourceEducationLists.push(element.REducation);

            });
        });
    }




    let MatchingTechnology = await getConstants('MatchingTechnology').then(data => {
        return data.Value
    });
    let MatchingDomain = await getConstants('MatchingDomain').then(data => {
        return data.Value
    });
    let MatchingRoles = await getConstants('MatchingRoles').then(data => {
        return data.Value
    });
    let MatchingEducation = await getConstants('MatchingEducation').then(data => {
        return data.Value
    });
    let IntroVideoAvailability = await getConstants('IntroVideoAvailability').then(data => {
        return data.Value
    });
    let MatchingAvailability = await getConstants('MatchingAvailability').then(data => {
        return data.Value
    });


    async function prepareRequirement_ids(Data) {
        let ids = [];
        Data.forEach(async element => {
            ids.push(element.Requirement_id);

        });
        return ids;

    }
    async function orderDataBy(data) {

        let arrays = [];
        for (let i = 0; i < data.length; i++) {
            for (let j = 0; j < data.length; j++) {
                if (data[i][j]) {
                    arrays.push(data[i][j]);
                }
            }
            if (i == data.length - 1) {
                console.log('Ordered');
                return arrays

            }
        }
    }
    let resourceTechnologyLists = [];
    let resourceDomainLists = [];
    let resourceRoleLists = [];
    let resourceEducationLists = [];

    let resourceData = await getResourceData();
    await resourceTechLists(resourceTechnologyLists);
    await resourceDoaminLists(resourceDomainLists);
    await resourceRolesLists(resourceRoleLists);
    await resourceEducationsLists(resourceEducationLists);

    let requirementData = await getRequirement_data(resourceData);
    let Requirement_idLists = await prepareRequirement_ids(requirementData);


    let mypromise = async function functionOne(Requirement_ids) {

        return await new Promise(async(resolve, reject) => {

            resolve(Requirement_ids);
        });
    };

    let matchingProcessingData = [];
    let filteredIds = [];
    global.finalMatchDatas = [];



    mypromise(Requirement_idLists).then((res) => {
            var array = JSON.parse("[" + res + "]");
            return array;
        })
        .then(async ids => {
            filteredIds = Object.keys(ids);
            console.log(`Fetching Technologies`);
            return await SelectedTech.findAll({
                where: {
                    Requirement_id: {
                        [Op.in]: filteredIds
                    }
                }
            }).then(async data => {
                return data;
            });
        })
        .then(data => {
            var rTech = JSON.stringify(resourceTechnologyLists);
            console.log(rTech);

            let tmp = [];
            let per = 1;
            data.forEach(val => {
                if (rTech.includes(val.Technology)) {

                    const i = tmp.findIndex(_element => _element.Requirement_id === val.Requirement_id);
                    if (i > -1) {
                        var v = tmp[i].Matching;
                        tmp[i] = {
                            'Requirement_id': val.Requirement_id,
                            'Technology': v + 1
                        };
                    } else {
                        tmp.push({
                            'Requirement_id': val.Requirement_id,
                            'Technology': per
                        });
                    }
                }
            });
            matchingProcessingData.push(tmp);
        })
        .then(async ids => {
            console.log(`Fetching Domains`);
            return await SelectedDomains.findAll({
                where: {
                    Requirement_id: {
                        [Op.in]: filteredIds
                    }
                }
            }).then(async data => {
                return data;
            });
        })
        .then(data => {
            console.log(`Domain Matching Function`);
            var rDomain = JSON.stringify(resourceDomainLists);
            let tmp = [];
            let per = 1;

            data.forEach(val => {

                if (rDomain.includes(val.Domains)) {

                    const i = tmp.findIndex(_element => _element.Requirement_id === val.Requirement_id);
                    if (i > -1) {
                        var v = tmp[i].Matching;
                        tmp[i] = {
                            'Requirement_id': val.Requirement_id,
                            'Domain': v + 1
                        };
                    } else {
                        tmp.push({
                            'Requirement_id': val.Requirement_id,
                            'Domain': per
                        });
                    }
                }
            });
            matchingProcessingData.push(tmp);

        })
        .then(async ids => {
            console.log(`Fetching Roles`);
            return await SelectedRoles.findAll({
                where: {
                    Requirement_id: {
                        [Op.in]: filteredIds
                    }
                }
            }).then(async data => {
                return data;
            });
        })
        .then(data => {
            console.log(`Role Matching Function`);
            var rRole = JSON.stringify(resourceRoleLists);
            let tmp = [];
            let per = 1;
            data.forEach(val => {
                if (rRole.includes(val.Roles)) {
                    const i = tmp.findIndex(_element => _element.Requirement_id === val.Requirement_id);
                    if (i > -1) {
                        var v = tmp[i].Matching;
                        tmp[i] = {
                            'Requirement_id': val.Requirement_id,
                            'Roles': v + 1
                        };
                    } else {
                        tmp.push({
                            'Requirement_id': val.Requirement_id,
                            'Roles': per
                        });
                    }
                }
            });
            matchingProcessingData.push(tmp);

        })
        .then(async ids => {
            console.log(`Fetching Qualifications`);
            return await SelectedQualifications.findAll({
                where: {
                    Requirement_id: {
                        [Op.in]: filteredIds
                    }
                }
            }).then(async data => {
                return data;
            });
        })
        .then(data => {
            console.log(`Qualifications Matching Function`);
            var rEducation = JSON.stringify(resourceEducationLists);

            let tmp = [];
            let per = 1;

            data.forEach(val => {
                if (rEducation.includes(val.Qualifications)) {
                    const i = tmp.findIndex(_element => _element.Requirement_id === val.Requirement_id);
                    if (i > -1) {
                        var v = tmp[i].Matching;
                        tmp[i] = {
                            'Requirement_id': val.Requirement_id,
                            'Qualifications': v + 1
                        };
                    } else {
                        tmp.push({
                            'Requirement_id': val.Requirement_id,
                            'Qualifications': per
                        });
                    }
                }
            });
            matchingProcessingData.push(tmp);

        })
        .then(data => {
            console.log('MATCHING COMPLETED!');

            return matchingProcessingData;
        }).then(async data => {
            console.log('Process end.....');
            return await orderDataBy(matchingProcessingData);


        }).then(async data => {
            let Rids = [];
            data.forEach(element => {
                Rids.push(element.Requirement_id);

            });
            return await requirementTb.findAll({
                where: {
                    Requirement_id: {
                        [Op.in]: Rids
                    },
                },
                include: [{
                    model: usersTb,
                    required: true
                }, {
                    model: companyTb,
                    required: true
                }]
            }).then(val => {
                let finalData = [];
                let tmp = [];
                console.log(data);

                data.forEach(element => {
                    for (let i = 0; i < Object.keys(val).length; i++) {
                        if (element.Requirement_id == val[i].Requirement_id) {
                            tmp = val[i].dataValues;

                            let tech = element.Technology >= 0 ? element.Technology : 0;
                            let domain = element.Domain >= 0 ? element.Domain : 0;
                            let role = element.Roles >= 0 ? element.Roles : 0;
                            let qualification = element.Qualifications >= 0 ? element.Qualifications : 0;
                            let matching = 0;

                            if (tmp.Technology) {
                                tech += tmp.Technology;
                            }
                            if (tmp.Domain) {
                                domain += tmp.Domain;
                            }
                            if (tmp.Roles) {
                                role += tmp.Roles;
                            }
                            if (tmp.Qualifications) {
                                qualification += tmp.Qualifications;
                            }

                            matching = tech + domain + role + qualification;



                            let matchingData = {
                                'Technology': tech,
                                'Domain': domain,
                                'Roles': role,
                                'Qualifications': qualification,
                                'Matching': matching

                            }

                            tmp = Object.assign(tmp, matchingData);

                            finalData.push(tmp);

                        }

                    }
                });

                return finalData;
            });

        }).then(async data => {
            let result = data.sort((a, b) => parseFloat(b.Matching) - parseFloat(a.Matching));

            res.send(result);

        })
        .catch((error) => {
            console.log(`Handling error as we received ${error}`);
        });
};
exports.getRequirementById = async(req, res) => {
    console.log(req.body);
    await requirementTb.findAll({
            where: { Requirement_id: req.body.Requirement_id },
            include: [{
                model: SelectedTech,
                required: false
            }, {
                model: SelectedDomains,
                required: false
            }, {
                model: SelectedRoles,
                required: false
            }, {
                model: SelectedQualifications,
                required: false
            }, {
                model: usersTb,
                required: false
            }]
        }, )
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

exports.resourcePersonalDataCreation = async(req, res) => {
    console.log(req.body);
    let fields = req.body;

    const newPassword = "Eq@" + Math.floor(Math.random() * (1000000 - 10000 + 10)) + 1000;
    const passwordHash = bcrypt.hashSync(newPassword, 10);
    const resourceName = fields.Resource_name;
    const resourceMail = fields.Resource_email;
    const Resource_id = fields.Resource_id | 0;


    const resourceData = {
        Resource_salutation: fields.Resource_salutation,
        Resource_name: fields.Resource_name,
        Resource_secondname: fields.Resource_secondname,
        Resource_Experience: fields.Resource_experience,
        Resource_Email: fields.Resource_email,
        Resource_phone: fields.Resource_phone,
        Resource_Designation: fields.Resource_designation,
        Resource_stack: fields.Resource_stack,
        Resource_status: fields.Resource_status,
        Is_remote: fields.isRemote,
        Resource_Password: passwordHash,
        Resource_rate: fields.Resource_rate,
        Availability_status: fields.Availability_status,
        Available_from: fields.Available_from,
        Resource_currency: fields.Resource_currency,
        Available_to: fields.Available_to,
        Company_id: fields.Company_id,
        Created_by: fields.User_id,
        Resource_location: fields.Resource_location,
        Min_available_from: fields.Min_available_from,
        Min_available_to: fields.Min_available_to,
        Manager_id: (fields.Manager_id == 0 ? fields.User_id : fields.Manager_id),
    };
    const resourceUpdateData = {
        Resource_salutation: fields.Resource_salutation,
        Resource_name: fields.Resource_name,
        Resource_secondname: fields.Resource_secondname,
        Resource_Experience: fields.Resource_experience,
        Resource_Email: fields.Resource_email,
        Resource_phone: fields.Resource_phone,
        Resource_Designation: fields.Resource_designation,
        Resource_stack: fields.Resource_stack,
        Resource_status: fields.Resource_status,
        Is_remote: fields.isRemote,
        Resource_rate: fields.Resource_rate,
        Availability_status: fields.Availability_status,
        Available_from: fields.Available_from,
        Resource_currency: fields.Resource_currency,
        Available_to: fields.Available_to,
        Company_id: fields.Company_id,
        Created_by: fields.User_id,
        Resource_location: fields.Resource_location,
        Min_available_from: fields.Min_available_from,
        Min_available_to: fields.Min_available_to,
        Manager_id: (fields.Manager_id == 0 ? fields.User_id : fields.Manager_id),
    };
    await resourceTb.findOne({ where: { Resource_id: Resource_id } }).then(async function(obj) {
        // update
        if (obj) {
            console.log('Resource update');
            await obj.update(resourceUpdateData);
            res.send({ status: true });

        } else {
            console.log('Resource create');
            let val = await resourceTb.create(resourceData);
            var subject = "Expertsq | Resource Account Creation";
            var mailBody = "Dear " + resourceName + ",<br> Greetings from Expertq.<br>Please login to the Application and complete your profile.";
            mailBody += "<br> Username :" + resourceMail + "<br>Password :" + newPassword;
            var templatePath = "../../uploads/templates/resourceCreationMail.handlebars";
            await sendMails(resourceMail, subject, mailBody, templatePath);
            res.send(val);

        }
    }).catch(err => {
        console.log(err);
    })


};
exports.resourceSkillDataCreation = async(req, res) => {
    async function prepareTechnology() {
        await resourceTechnologyTbs.destroy({
            where: {
                Resource_id: req.body.Resource_id
            }
        });

        var techologyArr = req.body.technology_list;
        var Resource_Techs = [];
        for (var tech of techologyArr) {
            var techData = {
                RTechnology_name: tech.Technology,
                RTechnology_version: tech.Technology_version,
                RTechnology_level: tech.Technology_level,
                RTechnology_duration: tech.Technology_experience,
                Resource_id: req.body.Resource_id,
                Technology_id: tech.Technology_id
            }
            Resource_Techs.push(techData);
        }
        return Resource_Techs;
    }
    async function addTechlists(data) {
        await resourceTechnologyTbs.bulkCreate(data).then(data => {
            res.send({ status: true });
        });

    }
    let techLists = await prepareTechnology();
    await addTechlists(techLists)

};
exports.resourcedomainDataCreation = async(req, res) => {
    console.log(req.body);
    async function prepareDomain() {
        await resourceDomainTbs.destroy({
            where: {
                Resource_id: req.body.Resource_id
            }
        });

        var dataArr = req.body.lists;
        var Resource_datas = [];
        for (var val of dataArr) {
            var tData = {
                RDomain: val.Domain,
                RDomain_duration: val.Domain_duration,
                Resource_id: req.body.Resource_id,
                Domain_id: val.Domain_id,
            }
            Resource_datas.push(tData);
        }
        return Resource_datas;
    }
    async function addDomainlists(data) {
        await resourceDomainTbs.bulkCreate(data).then(data => {
            res.send({ status: true });
        });

    }
    let techLists = await prepareDomain();
    await addDomainlists(techLists)

};
exports.resourceRoleDataCreation = async(req, res) => {
    console.log('resourceRoleDataCreation');
    console.log(req.body);
    async function preparedata() {

        var dataArr = req.body.lists;
        var Resource_datas = [];
        for (var val of dataArr) {
            var tData = {
                RRole_name: val.Job_title,
                RRole_duration: val.Job_duration,
                Resource_id: req.body.Resource_id,
                Roles_id: val.Role_id,
            }
            Resource_datas.push(tData);
        }
        return Resource_datas;
    }
    async function addLists(data) {
        await resourceRoleTbs.bulkCreate(data).then(data => {
            res.send({ status: true });
        });

    }
    let techLists = await preparedata();
    await addLists(techLists)

};
exports.resourceQualificationDataCreation = async(req, res) => {
    console.log('resourceQualificationDataCreation');
    console.log(req.body);
    async function preparedata() {
        await resourceEducationTbs.destroy({
            where: {
                Resource_id: req.body.Resource_id
            }
        });

        var dataArr = req.body.lists;
        var Resource_datas = [];
        for (var val of dataArr) {
            var tData = {
                REducation: val.Education,
                REducation_passyear: val.Pass_year | '',
                Resource_id: req.body.Resource_id,
                Education_id: val.Education_id | '',
            }
            Resource_datas.push(tData);
        }
        return Resource_datas;
    }
    async function addLists(data) {
        await resourceEducationTbs.bulkCreate(data).then(data => {
            res.send({ status: true });
        });

    }
    let techLists = await preparedata();
    await addLists(techLists)

};




exports.resourceProfilePhotoUpdate = async(req, res) => {

    var form = new IncomingForm();
    form.multiples = true;

    var profilePhoto = "";
    let Resource_id = "";


    form.parse(req, async(err, fields, files) => {
        Resource_id = fields.Resource_id;
        const uploadFile = (fileName) => {
            console.log('--------------------------------');
            console.log('--------------------------------');
            const base64Data = new Buffer.from(fileName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
            const type = fileName.split(';')[0].split('/')[1];
            var newname = 'profile' + Math.floor(Math.random() * 100000) + '.' + type;

            const params = {
                Bucket: BUCKET_NAME + '/Profile_Photos',
                Key: newname,
                ContentEncoding: 'base64', // required
                ContentType: `image/${type}`,
                Body: base64Data
            };

            s3.upload(params, async function(err, data) {
                if (err) {
                    throw err;
                }
                console.log(`Profile Photos successfully. ${data.Location}`);
                console.log(data);

                let fdata = {
                    'Resource_photo': data.Location,
                }
                await updateUser(fdata);

            });
        };
        uploadFile(fields.file);
    });

    async function updateUser(data) {
        await resourceTb.update(data, { where: { Resource_id: Resource_id } }).then(data => {

            res.send({ status: profilePhoto });
        });
    }
};




exports.resourceCVUpdate = async(req, res) => {

    var form = new IncomingForm();
    form.multiples = true;

    var newpath = "";
    let Resource_id = "";


    form.parse(req, (err, fields, files) => {

        console.log('--------------');
        console.log('--------------');
        Resource_id = fields.Resource_id;
    });


    form.on('file', async(field, files) => {

        const uploadFile = async(fileName) => {
            console.log('Uploading Resume ................');
            const fileContent = fs.createReadStream(fileName.path);
            newpath = Math.floor(Math.random() * 100000) + fileName.name;

            const params = {
                Bucket: BUCKET_NAME + '/Resume',
                Key: newpath,
                Body: fileContent
            };

            s3.upload(params, function(err, data) {
                if (err) {
                    throw err;
                }
                console.log(`Resume uploaded. ${data.Location}`);
                newpath = data.Location;

            });

            newpath = "https://photoseq.s3.amazonaws.com/" + BUCKET_NAME + '/Resume/' + newpath;

        };


        if (files.type == 'application/pdf') {
            resumeName = await uploadFile(files);
        }

        let fdata = {
            'Resource_resume': newpath,
        }
        console.log(fdata);
        console.log(Resource_id);
        await updateUser(fdata, Resource_id);


    });
    async function updateUser(data, Resource_id) {
        resourceTb.update(data, { where: { Resource_id: Resource_id } }).then(data => {

            res.send({ status: true });
        });
    }
};


exports.resourceOtherDataCreation = (req, res) => {
    console.log(req.body);

    let data = {
        'Resource_summery': req.body.Resource_summery
    }
    resourceTb.update(data, {
            where: { Resource_id: req.body.Resource_id }
        })
        .then(num => {
            if (num == 1) {
                res.send({
                    status: true
                });
            } else {
                res.send({
                    status: false
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};

exports.requirementSkillDataCreation = async(req, res) => {

    console.log(req.body);

    async function addTechnologies(r_id, p_id) {
        let t_list = req.body.technology_list;
        let listOfTechnologies = [];
        t_list.forEach(element => {
            var cc = {
                "Technology": element['Technology'],
                "Technology_experience": element['Technology_experience'],
                "Technology_level": element['Technology_level'],
                "Technology_version": element['Technology_version'],
                "User_id": req.body.User_id,
                "Requirement_id": r_id,
                "Project_id": p_id,
                "Technology_id": element['Technology_id']
            }
            listOfTechnologies.push(cc);
        });

        return await SelectedTechTb.findOne({ where: { Requirement_id: req.body.Requirement_id } }).then(async function(obj) {
            if (obj) {
                obj.destroy({ where: { Requirement_id: req.body.Requirement_id } }).then(async data => {
                    return await SelectedTechTb.bulkCreate(listOfTechnologies)
                        .then(data => {
                            console.log("Technologies updated ");
                            return true;
                        })
                        .catch(err => {
                            console.log(err);
                        });
                });
            } else {
                return await SelectedTechTb.bulkCreate(listOfTechnologies)
                    .then(data => {
                        console.log("Technologies updated ");
                        return true;
                    })
                    .catch(err => {
                        console.log("Technologies entered ");
                        console.log(err);
                    });
            }
        });
    }

    let isdata = await addTechnologies(req.body.Requirement_id, req.body.Project_id);
    if (isdata) {
        res.send({ status: true });
    } else {
        console.log(isdata);
    }
};

exports.requirementdomainDataCreation = async(req, res) => {


    async function addDomains(r_id, p_id) {

        let d_list = req.body.lists;
        let listOfDomains = [];
        d_list.forEach(element => {
            var cc = {
                "Domains": element['Domain'],
                "Domain_duration": element['Domain_duration'],
                "User_id": User_id,
                "Requirement_id": r_id,
                "Project_id": p_id,
                "Domain_id": element['Domain_id']
            }
            listOfDomains.push(cc);
        });
        return await SelectedDomainsTb.findOne({ where: { Requirement_id: req.body.Requirement_id } }).then(async function(obj) {
            if (obj) {
                obj.destroy({ where: { Requirement_id: req.body.Requirement_id } }).then(async data => {
                    return SelectedDomainsTb.bulkCreate(listOfDomains)
                        .then(data => {
                            console.log("Domains entered ");
                            return true;
                        })
                        .catch(err => {
                            console.log(err);
                        });
                });
            } else {
                return SelectedDomainsTb.bulkCreate(listOfDomains)
                    .then(data => {
                        console.log("Domains entered ");
                        return true;
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }
        });
    }
    let User_id = req.body.User_id;
    let isdata = await addDomains(req.body.Requirement_id, req.body.Project_id);
    if (isdata) {
        res.send({ status: true });
    } else {
        console.log(isdata);
    }
};

exports.requirementRoleDataCreation = async(req, res) => {
    console.log("requirementRoleDataCreation");
    console.log(req.body);

    async function addRoles(r_id, p_id) {
        await SelectedRoles.destroy({
            where: {
                Requirement_id: req.body.Requirement_id
            }
        });
        let r_list = req.body.lists;
        let listOfRoles = [];
        r_list.forEach(element => {
            var cc = {
                "Roles": element['Job_title'],
                "Job_duration": element['Job_duration'],
                "User_id": User_id,
                "Requirement_id": r_id,
                "Project_id": p_id,
                "Roles_id": element['Role_id']
            }
            listOfRoles.push(cc);
        });
        return await SelectedRoles.findOne({ where: { Requirement_id: req.body.Requirement_id } }).then(function(obj) {
            if (obj) {
                obj.destroy({ where: { Requirement_id: req.body.Requirement_id } }).then(() => {
                    return SelectedRoles.bulkCreate(listOfRoles)
                        .then(data => {
                            console.log("Roles entered ");
                            return true;
                        })
                        .catch(err => {
                            console.log(err);
                        });
                });
            } else {
                return SelectedRoles.bulkCreate(listOfRoles)
                    .then(data => {
                        console.log("Qualifications entered ");
                        return true;
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }
        });
    }

    let User_id = req.body.User_id;
    let isdata = await addRoles(req.body.Requirement_id, req.body.Project_id);
    if (isdata) {
        res.send({ status: true });
    } else {
        console.log(isdata);
    }
}
exports.requirementQualificationDataCreation = async(req, res) => {
    console.log(req.body);
    async function addEducations(r_id, p_id) {
        let e_list = req.body.lists;
        let listOfCertification = [];
        console.log(req.body.Certification);
        e_list.forEach(element => {
            var cc = {
                "Qualifications": element['Education'],
                "Pass_year": element['Pass_year'],
                "User_id": User_id,
                "Requirement_id": r_id,
                "Project_id": p_id,
                "Education_id": element['Education_id']
            }
            listOfCertification.push(cc);
        });

        return SelectedQualificationsTb.findOne({ where: { Requirement_id: req.body.Requirement_id } }).then(function(obj) {
            if (obj) {
                obj.destroy({ where: { Requirement_id: req.body.Requirement_id } }).then(() => {
                    return SelectedQualificationsTb.bulkCreate(listOfCertification)
                        .then(data => {
                            console.log("Qualifications entered ");
                            return true;
                        })
                        .catch(err => {
                            console.log(err);
                        });
                });
            } else {
                return SelectedQualificationsTb.bulkCreate(listOfCertification)
                    .then(data => {
                        console.log("Qualifications entered ");
                        return true;
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }
        });
    }
    let User_id = req.body.User_id;
    let isdata = await addEducations(req.body.Requirement_id, req.body.Project_id);
    if (isdata) {
        res.send({ status: true });
    } else {
        console.log(isdata);
    }

}

exports.applyRequirement = async(req, res) => {
    console.log(req.body);
    if (!req.body.Resource_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const reDatas = {
        User_id: req.body.User_id,
        Requirement_id: req.body.Requirement_id,
        Resource_id: req.body.Resource_id
    };
    async function candiadateHandler() {
        return CandidatesTbs.findOne({ where: { Requirement_id: req.body.Requirement_id, Resource_id: req.body.Resource_id } })
            .then(function(obj) {
                if (obj)
                    return false;

                return CandidatesTbs.create(reDatas);
            })
    }
    let result = await candiadateHandler();
    if (result) {
        res.send(result);
    } else {
        res.send({})
    }


};