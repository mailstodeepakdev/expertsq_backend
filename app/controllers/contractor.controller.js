const db = require("../models");
const companyTb = db.companyTb;
const usersTb = db.user;
const contractorTb = db.contractownerTb;
const spocTb = db.spocTb;
const delegateTb = db.delegateTb;
const GovermentTbs = db.GovermentTbs;
const LocationAllocationTbs = db.LocationAllocationTbs;

const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt');
var fs = require('fs');
const path = require('path');
var handlebars = require('handlebars');
var nodemailer = require('nodemailer');
const sequelize = db.sequelize;
const IncomingForm = require('formidable').IncomingForm;
const AWS = require('aws-sdk');
const awsConfig = require('../../config/AWS.config');
const BUCKET_NAME = awsConfig.Bucket_Name;

const s3 = new AWS.S3({
    accessKeyId: awsConfig.Access_Key_ID,
    secretAccessKey: awsConfig.Secret_Access_Key,
});


async function getNotificationMailLists(User_email) {
    return await usersTb.findOne({ where: { User_email: User_email } }).then(data => {
        let mails = data.Notification_mails;
        return JSON.parse(mails);
    })

}

async function sendMail(reciverMail, subject, mailBody, templatePath) {
    const myEmail = "admin@expertsq.com";
    const emailPass = "Moonlight@2021";
    let ccMails = await getNotificationMailLists(reciverMail);

    var readHTMLFile = function(path, callback) {
        fs.readFile(path, { encoding: "utf-8" }, function(err, html) {
            if (err) {
                throw err;
                callback(err);
            } else {
                callback(null, html);
            }
        });
    };
    readHTMLFile(path.resolve(__dirname, templatePath), function(err, html) {
        var template = handlebars.compile(html);
        var replacements = {
            mailBody: mailBody,
        };
        var htmlToSend = template(replacements);
        var transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
                user: myEmail,
                pass: emailPass,
            },
        });
        var mailOptions = {
            from: myEmail,
            to: reciverMail,
            cc: ccMails,
            subject: subject,
            html: htmlToSend,
        };
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log("Email sent: " + info.response + " || " + reciverMail);
            }
        });
    });
}


exports.updateCompany = async(req, res) => {
    console.log(req.body);
    var form = new IncomingForm();
    const transactions = await sequelize.transaction();
    var flag = false;

    form.parse(req, async(err, fields, files) => {

        var companyData = {
            C_short_name: fields.C_short_name,
            C_full_name: fields.C_full_name,
            Website: fields.Website,
            No_employees: fields.No_employees,
            About: fields.About,
            Company_email: fields.Company_email,
            Numberof_Locations: fields.Numberof_Locations,
            Enable_masking: fields.ResourceMasked,
            Freelancers: fields.EnalbleFreelancers,
            Tiers_maching: fields.Tiers_maching,
        };
        console.log('--------------');
        console.log(fields);
        console.log('--------------');
        companyTb.findOne({ where: { Company_id: fields.Company_id } }).then(async function(obj) {
            if (obj) {
                console.log('----------- Company found --------');
                var status = await companyUpdation();
                if (status) {
                    res.send({ status: true });
                }

            } else {
                console.log('Company not found');
                res.send({ status: false });
            }
        })

        async function companyUpdation() {
            return companyTb.update(companyData, {
                    where: { Company_id: fields.Company_id }
                }, { transaction: transactions }).then(async num => {
                    console.log('Comapny table updated successfully')
                    if (num == 1) {
                        await governmentIds(fields.Company_id, transactions);
                        await transactions.commit();
                        return true;
                    } else {
                        return false;
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        }

        async function governmentIds(Company_id, transactions) {
            var data = {
                'Company_id': Company_id,
                'User_id': fields.User_id,
                'Company_TAN': fields.Company_TAN,
                'Company_CIN': fields.Company_CIN,
                'Company_PAN': fields.Company_PAN,
            }
            console.log(data);
            return await GovermentTbs.update(data, { where: { Company_id: Company_id } }, { transaction: transactions })
                .then(data => {
                    console.log('GOVERNMENT IDS SUCCESSFUL....');
                    flag = true;
                    return true;
                })
                .catch(err => {
                    console.log(err);
                    flag = false;
                    return false;

                });
        }
    });
};





exports.CreateDelegate = async(req, res) => {

    console.log("-----------------------");
    console.log(req.body);
    console.log("-----------------------");

    async function passwordGenerator(newPassword) {
        return bcrypt.hashSync(newPassword, 10);
    }
    async function createDelegate(delegateData, passwordHash) {
        delegateName =
            delegateData.Delegate_salutation + delegateData.Delegate_firstName;
        delegateEmail = delegateData.User_email;
        var data = {
            User_roles_id: 3,
            User_salutation: delegateData.User_salutation,
            User_email: delegateData.User_email,
            User_firstname: delegateData.User_firstname,
            User_secondname: delegateData.User_secondname,
            User_phonenumber: delegateData.User_phonenumber,
            User_designation: delegateData.User_designation,
            User_location: delegateData.User_location,
            User_password: passwordHash,
            Company_id: delegateData.Company_id
        };
        return await usersTb
            .create(data)
            .then(async(data) => {
                console.log("DELEGATE CREATION SUCCESSFUL....");
                var subject = "Account Creation Completed | Expertsq";
                var dBody = "Dear " + delegateName + ",<br>";
                dBody += "Please use the below credentials to login.";
                dBody +=
                    "<br> Username : " + delegateEmail + "<br> Password : " + newPassword;
                dBody +=
                    "<br> <i>Please remember to change the password once you login</i>";
                var templatemail =
                    "../../uploads/templates/accountCreationMail.handlebars";
                await sendMail(delegateEmail, subject, dBody, templatemail);

                return true;
            })
            .catch((err) => {
                console.log(err);
                delegateErrors = "Invalid / Duplicate Delegate Details!";
            });
    }

    var newPassword = "Eq@" + Math.floor(Math.random() * (1000000 - 10000 + 10)) + 1000;
    let passwordTwo = await passwordGenerator(newPassword);
    await createDelegate(req.body, passwordTwo);

    res.send({ status: true });


};



exports.getDelegates = async(req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    usersTb.findAll({
            where: { Company_id: req.body.Company_id, User_status: 1, User_roles_id: 3 },
        })
        .then(data => {
            res.status(200).send(data);

        })
        .catch(err => {
            res.send(err);

        });

};






exports.CreateSpoc = async(req, res) => {
    console.log(req.body);
    var User_roles_id = 4;
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var Company_id = req.body.Company_id;
    var passwordGenerate = "Eq@" + Math.round(Math.random() * (100000 - 10000) + 10000);;
    var passwordHash = bcrypt.hashSync(passwordGenerate, 10);

    const loginData = {
        User_salutation: req.body.User_salutation,
        User_firstname: req.body.Spoc_name,
        User_secondname: req.body.Spoc_SecondName,
        User_designation: req.body.Spoc_designation,
        User_phonenumber: req.body.Spoc_phone,
        Company_id: req.body.Company_id,
        User_email: req.body.Spoc_email,
        User_password: passwordHash,
        User_location: req.body.User_location,
    };
    async function addLocationAllocation(User_id, Company_id, Locations_id) {
        let v = {
            User_id: User_id,
            Company_id: Company_id,
            Locations_id: Locations_id
        }
        return await LocationAllocationTbs.create(v);
    }

    async function insertLogin(Company_id, User_roles_id) {
        loginData.Company_id = Company_id;
        loginData.User_roles_id = User_roles_id;
        console.log(loginData);
        return await usersTb.create(loginData)
            .then(async data => {
                await addLocationAllocation(data.User_id, Company_id, req.body.Spoc_location);
                return data;
            })
            .catch(err => {
                return err.message;
            });
    }
    try {
        const login = await insertLogin(Company_id, User_roles_id);
        var respos = {
            "status": "Success"
        }
        var subject = "Account creation | ExpertsQ";
        var mailBody = "Dear " + req.body.User_salutation + req.body.Spoc_name + ", <br> Please login to the Application and complete your profile";
        mailBody += "<br>Username : " + req.body.Spoc_email + "<br>Password : " + passwordGenerate;

        var templatePath = "../../uploads/templates/spocCreationMail.handlebars";
        await sendMail(req.body.Spoc_email, subject, mailBody, templatePath);
        res.send(respos);
    } catch (error) {
        console.log(error);
        res.send(error);
    }
};



exports.getSpocs = async(req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    usersTb.findAll({
            where: { Company_id: req.body.Company_id, User_roles_id: 4, User_status: 1 },
        })
        .then(data => {
            res.status(200).send(data);

        })
        .catch(err => {
            res.send(err);

        });

};

exports.deleteSpoc = (req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var data = {
        "User_status": 0,
    }

    usersTb.update(data, {
            where: { User_id: req.body.User_id }
        }).then(num => {
            if (num == 1) {
                res.send({
                    status: true
                });
            } else {
                res.send({
                    message: 'Cannot Delete'
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};

exports.deleteDelegate = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var data = {
        "User_status": 0,
    }

    usersTb.update(data, {
            where: { User_id: req.body.User_id }
        }).then(num => {
            if (num == 1) {
                res.send({
                    status: true
                });
            } else {
                res.send({
                    message: 'Cannot Delete'
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};
exports.getDelegatesById = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    usersTb.findByPk(req.body.User_id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });
};

exports.getSpocById = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    usersTb.findByPk(req.body.User_id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });
};

exports.editDelegateData = async(req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var editDelegate = {
        "User_firstname": req.body.User_firstname,
        "User_secondname": req.body.User_secondname,
        "User_email": req.body.User_email,
        "User_designation": req.body.User_designation,
        "User_phonenumber": req.body.User_phonenumber,
        "User_location": req.body.User_location,
        "Company_id": req.body.Company_id,
    }

    await usersTb.update(editDelegate, {
            where: { User_id: req.body.User_id, Company_id: req.body.Company_id }
        }).then(num => {
            if (num == 1) {
                res.send({
                    Status: true
                });
            } else {
                res.send({
                    Status: false
                });
            }
        })
        .catch(err => {
            console.log(err);
        });
};
exports.editSpocData = async(req, res) => {
    console.log(req.body);
    console.log('-----');
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var editSpoc = {
        "User_salutation": req.body.User_salutation,
        "User_firstname": req.body.User_firstname,
        "User_secondname": req.body.User_secondName,
        "User_DINnumber": req.body.User_DINnumber,
        "User_email": req.body.User_email,
        "User_location": req.body.User_location,
        "User_designation": req.body.User_designation,
        "User_phonenumber": req.body.User_phonenumber,
        "User_phonenumber2": req.body.User_phone2,
    }
    async function updateLocation(User_id) {
        console.log('Location updated');
        let c = {
            "Locations_id": req.body.Spoc_location
        }
        return await LocationAllocationTbs.update(c, { where: { User_id: User_id } });

    }

    async function checkDuplicateEmailId(email, User_id) {
        return await usersTb.findOne({
            where: {
                User_email: email,
                User_id: {
                    [Op.neq]: User_id
                }
            }
        });
    }
    async function updateData() {
        await usersTb.update(editSpoc, {
                where: { User_id: req.body.User_id }
            }).then(num => {
                if (num == 1) {
                    res.send({
                        Status: true
                    });
                } else {
                    res.send({
                        Status: false
                    });
                }
            })
            .catch(err => {
                console.log(err);
                res.status(500).send({
                    message: "Error updating Tutorial with id=" + id
                });
            });
    }
    let user_email = req.body.User_email;
    let User_id = req.body.User_id;

    let counts = await checkDuplicateEmailId(user_email, User_id);
    if (counts) {
        res.send({ status: false });
    } else {
        await updateLocation(User_id);
        await updateData();
    }

};