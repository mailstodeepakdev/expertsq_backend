const db = require("../models");
const resourceTb = db.resourceTb;
const companyTb = db.companyTb;
const requirementTb = db.requirement;
const projectTb = db.project;
const usersTb = db.user;
const assignTb = db.assignTb;
const TimesheetTbs = db.TimesheetTbs;
var Sequelize = require("sequelize");
var nodemailer = require('nodemailer');
var moment = require('moment');
const Op = db.Sequelize.Op;


exports.getRequirementData = (req, res) => {
    console.log(req.body);
    assignTb.findAll({
            where: {
                Resource_id: req.body.Resource_id,
                Approved_status: 'Approved',
            },
            include: [{
                    model: requirementTb,
                    where: { Requirement_id: req.body.Requirement_id },
                    include: [{
                        model: projectTb,
                        required: true,
                    }, {
                        model: usersTb,
                        required: true,
                    }]
                },
                {
                    model: resourceTb,
                    required: true,
                    include: {
                        model: usersTb,
                        required: true,
                    }
                }
            ]
        }).then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err
            });
        });
};

exports.getRequirementIdByResourceId = (req, res) => {
    console.log(req.body);

    requirementTb.findAll({
            include: [{
                model: assignTb,
                required: true,
                where: {
                    Resource_id: req.body.Resource_id
                }
            }]
        }).then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err
            });
        });
};
exports.getRequirementsListsByResourceIdTimesheet = (req, res) => {
    requirementTb.findAll({
            include: [{
                model: assignTb,
                required: true,
                where: {
                    Resource_id: req.body.Resource_id,
                },
                include: {
                    model: resourceTb,
                    required: true,
                    where: {
                        Resource_id: req.body.Resource_id,
                    }
                }
            }]
        }).then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err
            });
        });
};

exports.updateTimesheet = async(req, res) => {

    console.log(req.body);

    var n = 1;
    req.body.daysLists.forEach(async element => {

        let d = new Date(element);
        let weekday = d.getDay();

        var day = 'day' + n;
        var timesheetdata = {
            "Working_date": element,
            "Working_hours": Number(req.body.result[day]),
            "Resource_id": req.body.Resource_id,
            "Project_id": req.body.Project_id,
            "Requirement_id": req.body.Requirement_id,
            "User_id": req.body.User_id,
            "Resource_approvel": 'Approved',
            "Status": 'Pending',
            "Working_day": weekday
        };
        console.log(timesheetdata);
        n++;

        await upsert(timesheetdata).then(async function(result) {

        });

        async function upsert(values) {
            return TimesheetTbs
                .findOne({ where: { "Resource_id": req.body.Resource_id, "Working_date": element } })
                .then(function(obj) {
                    // update
                    if (obj)
                        return obj.update(values);
                    // insert
                    return TimesheetTbs.create(values);
                })
        }
    });
};

exports.getTimesheetResource = (req, res) => {

    var startDate = moment(req.body.thisweek).subtract(6, 'd').format('YYYY-MM-DD');
    console.log(startDate);
    console.log(req.body.thisweek);

    TimesheetTbs.findAll({
            where: {
                Resource_id: req.body.Resource_id,
                Working_date: {
                    [Op.between]: [startDate, req.body.thisweek]
                }
            },
            order: [
                ['Working_date', 'ASC'],
            ],
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};
exports.timesheetOk = (req, res) => {
    var timeok = {
        'Resource_approvel': 'Approved',
    }
    TimesheetTbs.update(timeok, {
            where: { Resource_id: req.body.Resource_id }
        }).then(num => {
            if (num) {
                res.send({
                    Status: true
                });
            } else {
                res.send({
                    Status: false
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};

exports.getRequirementsListsByResourceId = (req, res) => {

    requirementTb.findAll({
            include: {
                model: assignTb,
                required: true,
                where: { Resource_id: req.body.Resource_id }
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);

        });
};
exports.getRequirementDataTimesheet = (req, res) => {
    assignTb.findAll({
            where: {
                Resource_id: req.body.Resource_id,
                Requirement_id: req.body.Requirement_id,
                Approved_status: 'Approved',
            },
            include: [{
                    model: requirementTb,
                    include: [{
                        model: projectTb,
                        required: true,
                    }, {
                        model: usersTb,
                        required: true,
                    }]
                },
                {
                    model: resourceTb,
                    required: true,
                    include: {
                        model: usersTb,
                        required: true,
                    }
                }
            ]
        }).then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err
            });
        });
};