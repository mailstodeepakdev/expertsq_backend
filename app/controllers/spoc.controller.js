const db = require("../models");
const spocTb = db.spocTb;
const delegateTb = db.delegateTb;
const usersTb = db.user;
const Op = db.Sequelize.Op;
const bcrypt = require('bcrypt');
const companyTb = db.companyTb;
const BranchesTb = db.BranchesTb;
const BranchAllocationTbs = db.BranchAllocationTbs;
const userrolesTb = db.userroles;

var nodemailer = require('nodemailer');
var handlebars = require('handlebars');
var fs = require('fs');
const path = require('path')


async function getNotificationMailLists(User_email) {
    return await usersTb.findOne({ where: { User_email: User_email } }).then(data => {
        if (data) {
            let mails = data.Notification_mails;
            return JSON.parse(mails);
        }
    })

}
async function sendMail(reciverMail, subject, mailBody, templatePath) {
    const myEmail = "admin@expertsq.com";
    const emailPass = "Moonlight@2021";
    let ccMails = await getNotificationMailLists(reciverMail);

    var readHTMLFile = function(path, callback) {
        fs.readFile(path, { encoding: 'utf-8' }, function(err, html) {
            if (err) {
                throw err;
                callback(err);
            } else {
                callback(null, html);
            }
        });
    };
    readHTMLFile(path.resolve(__dirname, templatePath), function(err, html) {
        var template = handlebars.compile(html);
        var replacements = {
            mailBody: mailBody,
        };
        var htmlToSend = template(replacements);
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: myEmail,
                pass: emailPass
            }
        });

        var mailOptions = {
            from: myEmail,
            to: reciverMail,
            cc: ccMails,
            subject: subject,
            html: htmlToSend
        };
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response + " || " + reciverMail);
            }
        });
    });

}

exports.checkManagerType = async(req, res) => {
    if (!req.body.email) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    async function isSpoc(email) {
        return await spocTb.findOne({
                where: { Spoc_email: email },
                include: {
                    model: companyTb,
                    required: true
                },
            }).then(data => {
                if (data.Spoc_email === email && bcrypt.compareSync(req.body.password, data.Spoc_password)) {
                    var id = data.Spoc_id;
                    id = id.toString();
                    var auth = {
                        "status": "SPOC",
                        "Spoc_id": id
                    }
                    var result = new Array();
                    result.push(auth);
                    result.push(data);
                    return result;
                } else {
                    return false;
                }
            })
            .catch(err => {
                return false;
            });
    }
    async function isDelegate(email) {
        return await delegateTb.findOne({
                where: { Delegate_email: email },
                include: {
                    model: companyTb,
                    required: true
                }
            }).then(data => {
                if (data.Delegate_email === email && bcrypt.compareSync(req.body.password, data.Delegate_password)) {
                    var id = data.Delegate_id;
                    id = id.toString();
                    var auth = {
                        "status": "DELEGATE",
                        "Delegate_id": id
                    }
                    var result = new Array();
                    result.push(auth);
                    result.push(data);
                    return result;
                } else {
                    return false;
                }
            })
            .catch(err => {
                return false;
            });
    }

    const spocFlag = await isSpoc(req.body.email);
    const delegateFlag = await isDelegate(req.body.email);

    if (!spocFlag && !delegateFlag) {
        var result = {
            status: 'FALSE'
        }
        res.status(401).send(result);
    } else if (spocFlag[0]) {
        res.status(200).send(spocFlag);
        return true;
    } else if (delegateFlag[0]) {
        res.status(200).send(delegateFlag);
        return true;
    } else {
        res.status(401).send("Contact Admin - Not Working\n");
        return false;

    }
};


exports.getMyCompany = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    async function getCompanyData() {
        return await usersTb.findAll({
            where: {
                User_id: req.body.User_id
            },
            include: {
                model: spocTb,
                include: {
                    model: companyTb,
                    required: true
                }
            },

        });
    }
    const myCompanyData = await getCompanyData();
    res.status(200).send(myCompanyData);

};



exports.updateProfile = async(req, res) => {
    if (!req.body.Spoc_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const profileData = {
        Spoc_name: req.body.Spoc_name,
        Spoc_email: req.body.User_email,
        Spoc_designation: req.body.Spoc_designation,
        Spoc_phone: req.body.Spoc_phone
    };
    const userData = {
        User_email: req.body.User_email,
    }

    async function updateSpoc() {
        spocTb.update(profileData, {
            where: { Spoc_id: req.body.Spoc_id }
        }).then(num => {
            if (num == 1) {
                res.send({
                    status: "true"
                });
            } else {
                res.send({
                    status: "false"
                });
            }
        }).catch(err => {
            res.status(500).send({
                message: err
            });
        });
    }

    async function userMailUpdate() {
        return await usersTb.update(userData, {
                where: { User_id: req.body.User_id }
            }).then(num => {
                if (num == 1) {
                    return true;
                } else {
                    return false;
                }
            })
            .catch(err => {
                var errorMsg = {
                        error: 0
                    }
                    //res.status(500).send(errorMsg);
                return false;
            });
    }
    const isUpdatedSpoc = await updateSpoc();
    const isSpocMailUpdated = await userMailUpdate();

    if (isUpdatedSpoc == true && isSpocMailUpdated == true) {
        return true;

    } else {
        return false;
    }

};

exports.createListingManager = async(req, res) => {
    console.log(req.body);
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    async function passwordGenerator(newPassword) {
        return bcrypt.hashSync(newPassword, 10);
    }

    var Company_id = req.body.Company_id;

    var newPassword = "Eq@" + Math.floor(Math.random() * (1000000 - 10000 + 10)) + 1000;
    let passwordTwo = await passwordGenerator(newPassword);
    var passwordHash = passwordTwo;
    var User_roles_id = 5;


    const loginData = {
        User_email: req.body.User_email,
        User_password: passwordHash,
        User_firstname: req.body.User_firstName,
        User_secondname: req.body.User_secondName,
        User_salutation: req.body.User_salutation,
        User_designation: req.body.User_designation,
        User_phonenumber: req.body.User_phone,
        Company_id: req.body.Company_id,
        Invoice_address: req.body.InvoiceAddress,
        Notification_mails: JSON.stringify(req.body.Notification_mails)
    };
    async function insertLogin(Company_id, User_roles_id) {
        loginData.Company_id = Company_id;
        loginData.User_roles_id = User_roles_id;

        return await usersTb.create(loginData)
            .then(async data => {
                var subject = "Account creation | Expertsq";
                var dBody = "Dear " + req.body.User_firstName + ",<br>";
                dBody += "Please use the below credentials to login.";
                dBody += "<br> Username : " + req.body.User_email + "<br> Password : " + newPassword;
                var templatemail = "../../uploads/templates/accountCreationMail.handlebars";

                await sendMail(req.body.User_email, subject, dBody, templatemail);
                console.log(data);
                return data;
            })
            .catch(err => {
                console.log(err);
                return err.message;
            });
    }
    async function insertBranch(User_id) {
        console.log('-----------------');
        console.log(req.body.Branches_id);
        req.body.Branches_id.forEach(async element => {
            console.log(element);
            var branchAllocationData = {
                Company_id: req.body.Company_id,
                User_id: User_id,
                Branches_id: element,
            };
            await BranchAllocationTbs.create(branchAllocationData);

        });
    }
    try {
        const login = await insertLogin(Company_id, User_roles_id);
        var respos = {
            "status": "Success"
        }
        await insertBranch(login.User_id);
        res.send(respos);

    } catch (error) {
        res.send(error);
    }
};


exports.getListingManagers = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    usersTb.findAll({
            where: {
                Company_id: req.body.Company_id,
                User_roles_id: 5,
                User_status: 1
            },
            include: {
                model: BranchAllocationTbs,
                required: true,
                include: {
                    model: BranchesTb,
                    required: true
                }

            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};


exports.LMDeletion = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var data = {
        "User_status": 0,
    }

    usersTb.update(data, {
            where: { User_id: req.body.User_id }
        }).then(num => {
            if (num == 1) {
                res.send({
                    status: true
                });
            } else {
                res.send({
                    message: 'Cannot Delete'
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating  with id"
            });
        });
};


exports.createHiringManager = async(req, res) => {
    console.log(req.body);
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    async function passwordGenerator(newPassword) {
        return bcrypt.hashSync(newPassword, 10);
    }
    var Company_id = req.body.Company_id;
    var newPassword = "Eq@" + Math.floor(Math.random() * (1000000 - 10000 + 10)) + 1000;
    let passwordTwo = await passwordGenerator(newPassword);
    var passwordHash = passwordTwo;
    var User_roles_id = 6;


    const loginData = {
        User_email: req.body.User_email,
        User_password: passwordHash,
        User_firstname: req.body.User_firstName,
        User_secondname: req.body.User_secondName,
        User_salutation: req.body.User_salutation,
        User_designation: req.body.User_designation,
        User_phonenumber: req.body.User_phone,
        Company_id: req.body.Company_id,
        Invoice_address: req.body.InvoiceAddress,
        Notification_mails: JSON.stringify(req.body.Notification_mails)

    };
    async function insertBranch(User_id) {
        console.log('Branch insert for' + User_id);
        req.body.BranchName.forEach(async element => {
            console.log('Branch element for' + element);
            var branchAllocationData = {
                Company_id: req.body.Company_id,
                User_id: User_id,
                Branches_id: element,
            };
            console.log('---');
            console.log(branchAllocationData);
            console.log('---');
            await BranchAllocationTbs.create(branchAllocationData);

        });
    }
    async function insertLogin(Company_id, User_roles_id) {
        loginData.Company_id = Company_id;
        loginData.User_roles_id = User_roles_id;

        return await usersTb.create(loginData)
            .then(async data => {

                var subject = "Account creation | Expertsq";
                var dBody = "Dear " + req.body.User_firstName + ",<br>";
                dBody += "Please use the below credentials to login.";
                dBody += "<br> Username : " + req.body.User_email + "<br> Password : " + newPassword;
                var templatemail = "../../uploads/templates/accountCreationMail.handlebars";
                await sendMail(req.body.User_email, subject, dBody, templatemail);
                return data;
            })
            .catch(err => {
                return err.message;
            });
    }


    try {
        const login = await insertLogin(Company_id, User_roles_id);
        await insertBranch(login.User_id);
        var respos = {
            "status": "Success"
        }
        res.send(respos);
    } catch (error) {
        res.send(error);
    }

};

exports.getHiringManagers = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    usersTb.findAll({
            where: {
                Company_id: req.body.Company_id,
                User_roles_id: 6,
                User_status: 1
            },
            include: {
                model: BranchAllocationTbs,
                required: false,
                include: {
                    model: BranchesTb,
                    required: false
                }
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};


exports.HMDeletion = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var data = {
        "User_status": 0,
    }

    usersTb.update(data, {
            where: { User_id: req.body.User_id }
        }).then(num => {
            if (num == 1) {
                res.send({
                    status: true
                });
            } else {
                res.send({
                    message: 'Cannot Delete'
                });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: "Error updating  with id"
            });
        });
};

exports.UserDeletion = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var data = {
        "User_status": 0,
    }

    usersTb.update(data, {
            where: { User_id: req.body.User_id }
        }).then(num => {
            if (num == 1) {
                res.send({
                    status: true
                });
            } else {
                res.send({
                    message: 'Cannot Delete'
                });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: "Error updating  with id"
            });
        });
};


exports.getAddressById = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    BranchesTb.findOne({ where: { Branches_id: req.body.Branches_id } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};

exports.getManagersList = (req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    usersTb.findAll({
            where: {
                Company_id: req.body.Company_id,
                User_status: 1,
                User_roles_id: {
                    [Op.or]: [5, 6, 8]
                }
            },
            include: [{
                model: BranchAllocationTbs,
                required: false,
                include: {
                    model: BranchesTb,
                    required: false
                }
            }, {
                model: userrolesTb,
                required: true,
            }]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};


exports.createManager = async(req, res) => {
    console.log(req.body);
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    async function passwordGenerator(newPassword) {
        return bcrypt.hashSync(newPassword, 10);
    }


    var newPassword = "Eq@" + Math.floor(Math.random() * (1000000 - 10000 + 10)) + 1000;
    let passwordTwo = await passwordGenerator(newPassword);
    var passwordHash = passwordTwo;


    const loginData = {
        User_email: req.body.User_email,
        User_password: passwordHash,
        User_firstname: req.body.User_firstName,
        User_secondname: req.body.User_secondName,
        User_salutation: req.body.User_salutation,
        User_designation: req.body.User_designation,
        User_phonenumber: req.body.User_phone,
        Company_id: req.body.Company_id,
        Invoice_address: req.body.InvoiceAddress,
        User_roles_id: req.body.User_roles_id,
        Notification_mails: JSON.stringify(req.body.Notification_mails)
    };
    async function insertLogin() {

        return await usersTb.create(loginData)
            .then(async data => {
                var subject = "Account creation | Expertsq";
                var dBody = "Dear " + req.body.User_firstName + ",<br>";
                dBody += "Please use the below credentials to login.";
                dBody += "<br> Username : " + req.body.User_email + "<br> Password : " + newPassword;
                var templatemail = "../../uploads/templates/accountCreationMail.handlebars";

                await sendMail(req.body.User_email, subject, dBody, templatemail);
                console.log(data);
                return data;
            })
            .catch(err => {
                console.log(err);
                return err.message;
            });
    }
    async function insertBranch(User_id) {
        console.log('-----------------');
        console.log(req.body.Branches_id);
        req.body.Branches_id.forEach(async element => {
            console.log(element);
            var branchAllocationData = {
                Company_id: req.body.Company_id,
                User_id: User_id,
                Branches_id: element,
            };
            await BranchAllocationTbs.create(branchAllocationData);

        });
    }
    try {
        const login = await insertLogin();
        await insertBranch(login.User_id);
        if (login.User_id > 0) {
            res.send({ status: true });
        } else {
            res.send({ status: false });

        }

    } catch (error) {
        res.send(error);
    }
};