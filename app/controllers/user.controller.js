const db = require("../models");
const companyTb = db.companyTb;
const usersTb = db.user;
const ChatTbs = db.ChatTbs;
const BranchesTb = db.BranchesTb;
const InvoiceTbs = db.InvoiceTbs;
var Sequelize = require("sequelize");
const requirementTb = db.requirement;
const GovermentTbs = db.GovermentTbs;
const locationsTb = db.locations;
const resourceTb = db.resourceTb;
const assignTb = db.assignTb;
const projectTb = db.project;
const userrolesTb = db.userroles;
const interviewTb = db.interviewTb;
const ComplaintsCategoryTbs = db.ComplaintsCategoryTbs;
const LocationAllocationTbs = db.LocationAllocationTbs;
const ComplaintsTb = db.ComplaintsTb;
const FeedbackTbs = db.FeedbackTbs;
const IndianStatesTbs = db.IndianStatesTbs;
const ReleaseResourcesTbs = db.ReleaseResourcesTbs;
const ExtendTbs = db.ExtendTbs;
const CandidatesTbs = db.CandidatesTbs;
var pdf = require("pdf-creator-node");
var pdfConvert = require("html-pdf");
var fs = require("fs");
const resourceRoleTbs = db.resourceRoleTbs;
const resourceDomainTbs = db.resourceDomainTbs;
const resourceTechnologyTbs = db.resourceTechnologyTbs;
const resourceEducationTbs = db.resourceEducationTbs;
const TimesheetTbs = db.TimesheetTbs;
var nodemailer = require('nodemailer');
var handlebars = require('handlebars');
const cronJobController = require('./cron.controller');
var moment = require('moment');
const OfferLetterTbs = db.OfferLetterTbs;
const Op = db.Sequelize.Op;
const BranchAllocationTbs = db.BranchAllocationTbs;

const bankdetailsTb = db.bankdetailsTb;
const bcrypt = require('bcrypt');
const IncomingForm = require('formidable').IncomingForm;
var fs = require('fs');
const path = require('path');
const AWS = require('aws-sdk');
const awsConfig = require('../../config/AWS.config');
const BUCKET_NAME = awsConfig.Bucket_Name;

const s3 = new AWS.S3({
    accessKeyId: awsConfig.Access_Key_ID,
    secretAccessKey: awsConfig.Secret_Access_Key,
});

var options = {
    format: "Letter",
    orientation: "portrait",
    border: "10mm",
    header: {
        height: "45mm",
        contents: '<div style="text-align: center;">INVOICE | EXPERTSQ</div>'
    },
    footer: {
        height: "28mm",
        contents: '<div style="text-align: center;">www.expertsq.com</div>'

    }
};
async function getNotificationMailLists(User_email) {
    return await usersTb.findOne({ where: { User_email: User_email } }).then(data => {
        let mails = data.Notification_mails;
        return JSON.parse(mails);
    })

}
async function sendMail(reciverMail, subject, mailBody, templatePath) {
    const myEmail = "admin@expertsq.com";
    const emailPass = "Moonlight@2021";
    let ccMails = await getNotificationMailLists(reciverMail);
    var readHTMLFile = function(path, callback) {
        fs.readFile(path, { encoding: 'utf-8' }, function(err, html) {
            if (err) {
                throw err;
                callback(err);
            } else {
                callback(null, html);
            }
        });
    };
    readHTMLFile(path.resolve(__dirname, templatePath), function(err, html) {
        var template = handlebars.compile(html);
        var replacements = {
            mailBody: mailBody,
        };
        var htmlToSend = template(replacements);
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: myEmail,
                pass: emailPass
            }
        });

        var mailOptions = {
            from: myEmail,
            to: reciverMail,
            cc: ccMails,
            subject: subject,
            html: htmlToSend
        };
        transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response + " || " + reciverMail);
            }
        });
    });

}


exports.updateUserProfile = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const userData = {
        "User_salutation": req.body.User_salutation,
        "User_firstname": req.body.User_firstName,
        "User_secondname": req.body.User_secondName,
        "User_phonenumber": req.body.User_phone,
        "User_phonenumber2": req.body.User_phone2,
        "User_email": req.body.User_email,
        "User_password": req.body.User_password,
        "User_password2": req.body.User_password2,
        "User_location": req.body.User_location,
        "User_designation": req.body.User_designation,
        "User_DINnumber": req.body.User_DINnumber,
    };
    console.log("Profile update function");
    console.log(req.body);
    await usersTb.update(userData, {
            where: { User_id: req.body.User_id }
        }).then(num => {
            if (num == 1) {
                console.log("Profile updated");
                var result = {
                    "status": "Success"
                }
                res.status(200).send(JSON.stringify(result));
            } else {
                var result = {
                    "status": "Failed"
                }
                res.status(200).send(JSON.stringify(result));
            }
        })
        .catch(err => {
            var errorMsg = {
                error: 0
            }
            return false;
        });


};

exports.getProfile = async(req, res) => {
    console.log(req.body);
    console.log('------');
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    await usersTb.findOne({
        where: {
            User_id: req.body.User_id
        },
        include: [{
            model: companyTb,
            required: true,
            include: {
                model: GovermentTbs,
                required: false
            }
        }, {
            model: BranchAllocationTbs,
            required: false,
            include: {
                model: BranchesTb,
                required: false
            },

        }, {
            model: LocationAllocationTbs,
            required: false
        }],
    }).then(data => {

        res.status(200).send(data);
    }).catch(err => {
        console.log(err);
        res.status(200).send(err);
    });
};

exports.profilePhotoUpdate = async(req, res) => {

    var form = new IncomingForm();
    await profilePicS3Upload();

    async function profilePicS3Upload() {

        form.parse(req, async(err, fields, files) => {
            const uploadFile = (fileName) => {
                console.log('--------------------------------');
                console.log('--------------------------------');
                const base64Data = new Buffer.from(fileName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
                const type = fileName.split(';')[0].split('/')[1];
                var newname = 'profile' + Math.floor(Math.random() * 100000);

                const params = {
                    Bucket: BUCKET_NAME + '/Profile_Photos',
                    Key: newname,
                    ContentEncoding: 'base64', // required
                    ContentType: `image/${type}`,
                    Body: base64Data
                };

                s3.upload(params, async function(err, data) {
                    if (err) {
                        throw err;
                    }
                    console.log(`Profile Photos successfully. ${data.Location}`);
                    console.log(data);
                    var profilePhoto = {
                        "Profile_photo": data.Location
                    };
                    await usersTb.update(profilePhoto, {
                            where: { User_id: fields.User_id }
                        }).then(num => {
                            if (num == 1) {
                                res.send({
                                    Status: true
                                });
                            } else {
                                res.send({
                                    Status: false
                                });
                            }
                        })
                        .catch(err => {
                            console.log(err);
                            res.status(500).send({
                                message: "Error updating Tutorial with id=" + id
                            });
                        });

                });
            };
            uploadFile(fields.file);
        });


    }

};


exports.sendMsg = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var msgs = {
        "Sender_id": req.body.User_id,
        "Reciver_id": req.body.LManager_id,
        "Message": req.body.Message,
        "Requirement_id": req.body.Requirement_id,
        "Resource_id": req.body.Resource_id,
        "Sender_seen": 1,
        "Reciver_seen": 0,
    }

    await ChatTbs.create(msgs)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Tutorial."
            });
        });
};

exports.getMsg = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    await ChatTbs.findAll({
            where: {
                [Op.or]: [{
                        Sender_id: req.body.User_id,
                        Reciver_id: req.body.LManager_id,
                        Requirement_id: req.body.Requirement_id,
                        Resource_id: req.body.Resource_id,
                    },
                    {
                        Sender_id: req.body.LManager_id,
                        Requirement_id: req.body.Requirement_id,
                        Resource_id: req.body.Resource_id,
                        Reciver_id: req.body.User_id,
                    },
                ]
            }
        }).then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Tutorial."
            });
        });
};

exports.getTotalMessages = async(req, res) => {

    await ChatTbs.count({
            where: {
                Reciver_id: req.body.User_id,
                Reciver_seen: 0
            }
        })
        .then(data => {
            console.log("COUNT" + data);
            var c = {
                'Count': data
            }
            res.send(c);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};
exports.getChatHistory = async(req, res) => {
    uids = [];
    await ChatTbs.findAll({
            where: {
                [Op.or]: [{
                        Sender_id: req.body.User_id,
                    },
                    {
                        Reciver_id: req.body.User_id,
                    },
                ]
            },
        })
        .then(data => {
            if (data.length > 0) {
                var datas = [];
                data.forEach(element => {
                    var c = {
                        'Sender_id': element.Sender_id,
                        'Reciver_id': element.Reciver_id,
                        'Requirement_id': element.Requirement_id,
                        'Resource_id': element.Resource_id,
                    }
                    datas.push(c);
                });
                usersData(datas);
            } else {
                var c = {
                    'Count': 0
                }
                res.send(c);
            }
        })
        .catch(err => {
            console.log(err);
        });

    async function usersData(val) {
        var uniqueRequirementIds = [];
        var uniqueUsers = [];
        var uniqueResources = [];

        val.forEach(el => {
            if (!uniqueRequirementIds.includes(el.Requirement_id)) {
                uniqueRequirementIds.push(el.Requirement_id);
            }
            console.log('uniqueRequirementIds' + uniqueRequirementIds)
            if (!uniqueUsers.includes(el.Sender_id)) {
                uniqueUsers.push(el.Sender_id);
            }
            console.log('uniqueUsers' + uniqueUsers);
            if (!uniqueResources.includes(el.Resource_id)) {
                uniqueResources.push(el.Resource_id);
            }
            console.log('uniqueResources' + uniqueResources);
        })

        //   uniqueUsers = uniqueUsers.filter(function(item) {
        //     return item !== Number(req.body.User_id)
        // }) 
        //   console.log('uniqueUsers After removing'+ req.body.User_id+ "is" + uniqueUsers);


        await usersTb.findAll({
                where: {
                    User_id: {
                        [Op.or]: uniqueUsers
                    }
                },
                include: [{
                        model: requirementTb,
                        required: true,
                        where: {
                            Requirement_id: {
                                [Op.or]: uniqueRequirementIds
                            }
                        }
                    },
                    {
                        model: companyTb,
                        required: false
                    }
                ],
            }).then(data => {
                res.send(data);
            })
            .catch(err => {
                console.log(err);
            });
        //res.send(val);  
    }
};

exports.getSingleChat = async(req, res) => {
    console.log(req.body);
    await ChatTbs.findAll({
            where: {
                [Op.or]: [{
                        Sender_id: req.body.Sender_id,
                        Reciver_id: req.body.User_id,
                        Requirement_id: req.body.Requirement_id
                    },
                    {
                        Sender_id: req.body.User_id,
                        Reciver_id: req.body.Sender_id,
                        Requirement_id: req.body.Requirement_id
                    },
                    {
                        Sender_id: req.body.User_id,
                        Requirement_id: req.body.Requirement_id
                    },
                    {
                        Reciver_id: req.body.Sender_id,
                        Requirement_id: req.body.Requirement_id
                    },
                ]
            },
            include: [{
                    model: usersTb,
                    required: false,
                },
                {
                    model: resourceTb,
                    required: true,
                }
            ]
        }).then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};

exports.msgSeen = async(req, res) => {
    var Seen = {
        "Reciver_seen": 1,
    }
    console.log(req.body)
    await ChatTbs.update(Seen, {
            where: {
                Requirement_id: req.body.Requirement_id,
                Resource_id: req.body.Resource_id,

            }
        }).then(num => {
            console.log(num);
            if (num == 1) {
                res.send({
                    Status: true
                });
            } else {
                res.send({
                    Status: false
                });
            }
        })
        .catch(err => {
            console.log(err);
        });
};

exports.getResourceProfileData = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    await resourceTb.findAll({
            where: { Resource_id: req.body.Resource_id },
            include: [{
                    model: companyTb,
                    required: true,
                },
                {
                    model: resourceRoleTbs,
                    required: false,
                },
                {
                    model: resourceDomainTbs,
                    required: false,
                },
                {
                    model: resourceTechnologyTbs,
                    required: false,
                },
                {
                    model: resourceEducationTbs,
                    required: false,
                }
            ]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};


exports.getTimesheetBy_ResourceId = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    await resourceTb.findAll({
            where: { Resource_id: req.body.Resource_id },
            include: [{
                model: TimesheetTbs,
                required: true,
                where: {
                    Requirement_id: req.body.Requirement_id
                }
            }, ]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};

exports.getInvoices = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    async function get_Requirements(monday) {
        return await assignTb.findAll({
            where: { User_id: req.body.User_id, Approved_status: 'Approved' },
            include: [{
                model: resourceTb,
                required: true,
                include: [{
                    model: TimesheetTbs,
                    required: true,
                    where: {
                        Working_date: {
                            [Op.gte]: req.body.mondayCheck
                        },
                        Status: 'Approved'
                    }
                }]
            }, {
                model: requirementTb,
                required: true,
                include: {
                    model: usersTb,
                    required: true,
                    include: [{
                            model: companyTb,
                        },
                        {
                            model: BranchesTb,
                            required: true,
                        }
                    ]
                }
            }]
        });
    }

    async function calculate_Amount(details) {
        var userswithPrice = [];

        details.forEach(element => {
            if (element.ResourceTb !== null) {
                var resourceID = element.ResourceTb.Resource_id;
                var Resource_name = element.ResourceTb.Resource_name;
                var resourceRate = element.ResourceTb.Resource_rate;
                var resourceCurrency = element.ResourceTb.Resource_currency;

                if (element.ResourceTb.TimesheetTbs !== null) {
                    var totalHours = 0;
                    let requirementName = element.RequirementsTb.Requirement_name;
                    let Requirement_id = element.RequirementsTb.Requirement_id;
                    let hiringManager = element.RequirementsTb.UsersTb.User_firstname;
                    let hiringManager_mail = element.RequirementsTb.UsersTb.User_email;
                    let hiringgManager_id = element.RequirementsTb.UsersTb.User_id;
                    let hiringManager_company = element.RequirementsTb.UsersTb.CompanyTb.C_full_name;
                    let hiringManager_company_address = element.RequirementsTb.UsersTb.BranchesTbs[0].Company_city_address;

                    element.ResourceTb.TimesheetTbs.forEach(async element => {
                        totalHours += Number(element.Working_hours);
                    });
                    var getRandomId = (min = 0, max = 500000) => {
                        min = Math.ceil(min);
                        max = Math.floor(max);
                        const num = Math.floor(Math.random() * (max - min + 1)) + min;
                        return num.toString().padStart(6, "0")
                    };
                    var c = {
                        'Resource_id': resourceID,
                        'Resource_name': Resource_name,
                        'TotalPrice': (resourceRate * totalHours),
                        'Requirement_name': requirementName,
                        'Requirement_id': Requirement_id,
                        'hiringManager': hiringManager,
                        'hiringManager_mail': hiringManager_mail,
                        'hiringgManager_id': hiringgManager_id,
                        'hiringManager_company': hiringManager_company,
                        'hiringManager_company_address': hiringManager_company_address,
                        'totalHours': totalHours,
                        'resourceRate': resourceRate,
                        'resourceCurrency': resourceCurrency,
                        'invoiceid': getRandomId()
                    }
                    userswithPrice.push(c);
                }


            }
        });
        return userswithPrice;

    }

    async function getLatestMonday() {
        let currentDateObj = new Date();
        currentDateObj.setDate(currentDateObj.getDate() - (currentDateObj.getDay() + 1) % 7);
        currentDateObj = currentDateObj.toLocaleDateString("zh-Hans-CN");
        return currentDateObj;
    }
    async function prepareInvoice(val) {
        val.forEach(async element => {

            //var filePaths = "/Users/mobiotics/EQ/backend/uploads/templates/invoiceData.handlebars";

            var filePaths = path.resolve(__dirname, "../../uploads/templates/invoiceData.handlebars");
            var utc = new Date();

            let comission = Number(element.TotalPrice) / 10;
            let grantTotal = element.TotalPrice - comission;
            var replacements = {
                Resource_name: element.Resource_name,
                TotalPrice: element.TotalPrice,
                Requirement_name: element.Requirement_name,
                hiringManager: element.hiringManager,
                hiringManager_mail: element.hiringManager_mail,
                hiringManager_company: element.hiringManager_company,
                hiringManager_company_address: element.hiringManager_company_address,
                totalHours: element.totalHours,
                resourceRate: element.resourceRate,
                comission: comission + element.resourceCurrency,
                grantTotal: grantTotal + element.resourceCurrency,
                date: utc,
                invoiceid: element.invoiceid
            }
            console.log(replacements);

            var filename = "Invoice_" + element.Resource_name + "_" + utc + ".pdf";
            var filepath = "./Invoice_" + element.Resource_name + "_" + utc + ".pdf";
            await htmlloading(filePaths, filepath, filename, element, replacements);


        });
    }
    async function htmlloading(htmlFile, filepath, filename, element, replacements) {
        var readHTMLFile = function(filePaths, callback) {
            fs.readFile(filePaths, { encoding: 'utf-8' }, function(err, html) {
                if (err) {
                    throw err;
                    callback(err);
                } else {
                    callback(null, html);
                }
            });
        };
        readHTMLFile(path.resolve(htmlFile), async function(err, html) {
            var template = handlebars.compile(html);
            var htmlToSend = template(replacements);
            var options = { format: 'A4' };
            pdfConvert.create(htmlToSend, options).toFile(filepath, async function(err, res) {
                if (err) return console.log(err);
                console.log(res);
                await saveInvoice(filepath, filename, element);
            });
        });
    }

    async function saveInvoice(filepath, filename, element) {
        const fileContent = fs.createReadStream(filepath);
        const params = {
            Bucket: BUCKET_NAME + '/Invoices',
            Key: filename,
            Body: fileContent
        };

        s3.upload(params, async function(err, data) {
            if (err) {
                throw err;
            }
            console.log(`Invoice uploaded successfully. ${data.Location}`);
            console.log(data);
            var invoiceBodyMail = '<div> Dear ' + element.hiringManager + ' , <br>   The invoice for your resource ' + element.Resource_name + ' for the assignment ' + element.Requirement_name + ' has been generated.<a href="' + data.Location + '">Click to download</a> </div>';
            var subject = "Invoice | ExpertsQ";
            var templatemail = "../../uploads/templates/invoiceMail.handlebars";
            await sendMail(element.hiringManager_mail, subject, invoiceBodyMail, templatemail);

            var invoiceDBData = {
                Requirement_id: element.Requirement_id,
                Resource_id: element.Resource_id,
                User_id: element.hiringManager_id,
                Invoice_file: data.Location,
                Invoice_code: element.invoiceid
            }
            await InvoiceTbs.create(invoiceDBData);
        });
    }
    let monday = await getLatestMonday();
    let requirements = await get_Requirements(monday);
    let amountData = await calculate_Amount(requirements);
    let invoiceData = await prepareInvoice(amountData);

    return amountData;
};




exports.cronUsersInvoice = async(req, res) => {
    let uniqUsers = [];
    return await assignTb.findAll({
            where: { Approved_status: 'Approved' },
            include: {
                model: usersTb,
                required: true,
            }
        }).then(async data => {
            data.forEach(async element => {
                uniqUsers.push(element.UsersTb.User_id);
            });
            let findDuplicates = arr => arr.filter((item, index) => arr.indexOf(item) != index);
            uniqUsers = findDuplicates(uniqUsers);
            console.log('Unique for invoice:' + uniqUsers)
            return uniqUsers;
        })
        .catch(err => {
            console.log(err);
        });

};

exports.getInvoicesDb = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    await InvoiceTbs.findAll({
            include: [{
                model: requirementTb,
                required: true,
                where: { User_id: req.body.User_id },
                include: [{
                    model: usersTb,
                    required: true,
                }, ]
            }, {
                model: resourceTb,
                required: true,
                include: {
                    model: companyTb,
                    required: true,
                }
            }]
        }).then(async data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });

};
exports.getBranches = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    await BranchesTb.findAll({
            where: { Company_id: req.body.Company_id, Status: 'ACTIVE' },
            include: {
                model: locationsTb,
                required: true,
                where: { Location_status: 'ACTIVE' }
            }
        }).then(async data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });

};

exports.getLocations = async(req, res) => {
    if (!req.body.Company_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    await locationsTb.findAll({ where: { Company_id: req.body.Company_id, Location_status: 'ACTIVE' }, }).then(async data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });

};




exports.updateUser = async(req, res) => {
    console.log('---------------------');
    console.log(req.body);
    console.log('---------------------');
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const userData = {
        'User_salutation': req.body.User_salutation,
        'User_firstname': req.body.User_firstName,
        'User_secondname': req.body.User_secondName,
        'User_designation': req.body.User_designation,
        'User_phonenumber': req.body.User_phone,
        'User_email': req.body.User_email,
        'Invoice_address': req.body.Invoice_address,
    };
    console.log("Profile update function");

    async function insertBranch() {
        await BranchAllocationTbs.destroy({
            where: { User_id: req.body.User_id }
        }).then(async done => {
            req.body.Branches_id.forEach(async element => {
                var branchAllocationData = {
                    Company_id: req.body.Company_id,
                    User_id: req.body.User_id,
                    Branches_id: element,
                };

                await BranchAllocationTbs.create(branchAllocationData);
            });
        });

    }

    await usersTb.update(userData, {
            where: { User_id: req.body.User_id }
        }).then(async num => {
            if (num == 1) {
                console.log("Profile updated");
                await insertBranch();
                var result = {
                    "status": "Success"
                }
                res.status(200).send(JSON.stringify(result));
            } else {
                var result = {
                    "status": "Failed"
                }
                res.status(200).send(JSON.stringify(result));
            }
        })
        .catch(err => {
            console.log(err);
            var errorMsg = {
                error: 0
            }
            return false;
        });
};

exports.getInvoiceCount = async(req, res) => {

    return await requirementTb.count({
            where: { User_id: req.body.User_id },
            include: {
                model: InvoiceTbs,
                required: true,
                where: {
                    Transaction_id: {
                        [Op.eq]: ''
                    }
                }
            }
        })
        .then(data => {
            console.log('Total invoice' + data);
            res.status(200).send(JSON.stringify(data));
        })
        .catch(err => {
            console.log(err);
        });
};


exports.invoiceGeneration = async(req, res) => {
    init();

    function init() {
        var thisMonth = moment().month();
        let mondaysList = getMondays(moment().month(thisMonth).toDate());
        var n = 0;
        mondaysList.forEach(async element => {
            n++;
            var check = moment(element, 'MM/DD/YYYY');
            var mondayCheck = check.format('YYYY-MM-DD');
            var todayDate = new Date().toJSON().slice(0, 10).replace(/-/g, '-');

            if (todayDate >= mondayCheck && (n % 2) == 0) {
                console.log('valida date' + mondayCheck);
                await invoiceGenerate(mondayCheck);
            }
        });
    }

    async function invoiceGenerate(mondayCheck) {

        cronJobController.cronUsersInvoice().then(async data => {
            return data;
        }).then(users => {

            users.forEach(element => {
                var c = {
                    body: {
                        User_id: element,
                        mondayCheck: mondayCheck
                    }
                }
                cronJobController.getInvoices(c).then(data => {
                    console.log('********************');
                });
            });
        })
    }

    function getMondays(date) {
        var d = date || new Date(),
            month = d.getMonth(),
            mondays = [];
        d.setDate(1);
        // Get the first Monday in the month
        while (d.getDay() !== 1) {
            d.setDate(d.getDate() + 1);
        }
        // Get all the other Mondays in the month
        while (d.getMonth() === month) {
            mondays.push(new Date(d.getTime()).toLocaleDateString("en-US"));
            d.setDate(d.getDate() + 7);
        }
        return mondays;
    }
};


exports.getTotalCountDashboard = async(req, res) => {

    async function companyCount() {
        return await companyTb.count()
            .then(data => {
                console.log('Total companyTb' + data);
                return data;
            })
            .catch(err => {
                console.log(err);
            });
    }
    async function resourceCount() {
        return await resourceTb.count()
            .then(data => {
                console.log('Total resourceTb' + data);
                return data;
            })
            .catch(err => {
                console.log(err);
            });
    }
    async function projectCount() {
        return await projectTb.count()
            .then(data => {
                console.log('Total projectTb' + data);
                return data;
            })
            .catch(err => {
                console.log(err);
            });
    }
    async function assignmentCount() {
        return await requirementTb.count()
            .then(data => {
                console.log('Total requirementTb' + data);
                return data;
            })
            .catch(err => {
                console.log(err);
            });
    }
    let c = await companyCount();
    let r = await resourceCount();
    let p = await projectCount();
    let a = await assignmentCount();

    let counts = {
        'companyCount': c,
        'resourceCount': r,
        'projectCount': p,
        'assignmentCount': a,
    }
    res.send(counts);
};
exports.dashboardGetAllResources = (req, res) => {

    resourceTb.findAll({
            where: {
                Company_id: req.body.Company_id
            },
            include: {
                model: usersTb,
                required: true,
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};
exports.dashboardGetAllUsers = (req, res) => {

    usersTb.findAll({
            where: {
                Company_id: req.body.Company_id
            },
            include: [{
                    model: userrolesTb,
                    required: true,
                },
                {
                    model: BranchesTb,
                    required: true,
                }
            ]
        }).then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};
exports.dashboardGetAllProjects = (req, res) => {

    projectTb.findAll({
            where: {
                Company_id: req.body.Company_id
            },
            include: [{
                model: requirementTb,
                required: true,
                include: [{
                        model: usersTb,
                        required: true,
                    },
                    {
                        model: assignTb,
                        required: false,
                        where: {
                            Approved_status: 'Approved'
                        }
                    }
                ]
            }, ]
        }).then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

exports.getUsersForallocation = (req, res) => {

    usersTb.findAll({
            where: {
                Company_id: req.body.Company_id,
            },
        }).then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};
exports.getDataWorksheetData = (req, res) => {
    TimesheetTbs.findAll({
            where: {
                Resource_id: req.body.Resource_id,
                Requirement_id: req.body.Requirement_id,
            },
        }).then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};
exports.updateTimesheets = (req, res) => {

    var worksheetData = {
        "Working_date": req.body.worksheetData.Working_date,
        "Working_hours": req.body.worksheetData.Working_hours,
        "Comments": req.body.worksheetData.Comments
    }
    TimesheetTbs.update(worksheetData, {
            where: { Resource_id: req.body.Resource_id, Requirement_id: req.body.Requirement_id }
        })
        .then(num => {
            if (num == 1) {
                res.send({
                    Status: true
                });
            } else {
                res.send({
                    Status: false
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};

exports.addTransactionId = (req, res) => {

    console.log(req.body);
    var invoiceData = {
        "Transaction_id": req.body.Transaction_id,
    }
    InvoiceTbs.update(invoiceData, {
            where: { Invoice_id: req.body.Invoice_id }
        }).then(num => {
            console.log(num + ' Transaction ID Success');
            if (num == 1) {
                res.send({
                    Status: true
                });
            } else {
                res.send({
                    Status: false
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};
exports.getOfferLetter = (req, res) => {

    OfferLetterTbs.findAll({
            where: {
                Resource_id: req.body.Resource_id,
                Requirement_id: req.body.Requirement_id
            },
            include: [{
                model: resourceTb,
                required: true,
                where: { Resource_id: req.body.Resource_id },
                include: {
                    model: usersTb,
                    required: true
                }
            }, {
                model: requirementTb,
                required: true,
                where: { Requirement_id: req.body.Requirement_id },
                include: [{
                        model: usersTb,
                        required: true,
                        include: {
                            model: companyTb,
                            required: true,
                        }
                    },
                    {
                        model: projectTb,
                        required: true,
                    }
                ]
            }]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });

};
exports.getUsersExepctLManagers = (req, res) => {
    console.log(req.body);

    usersTb.findAll({
            where: {
                Company_id: req.body.Company_id,
                User_roles_id: {
                    [Op.in]: [6, 8, 2, 3, 4]
                },
                User_id: {
                    [Op.ne]: req.body.User_id
                }
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

exports.getUsersBranchById = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    await BranchesTb.findOne({
            where: { Branches_id: req.body.Branch_id },
            include: {
                model: locationsTb,
                required: true,
                where: { Location_status: 'ACTIVE' }
            }
        }).then(async data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });

};

exports.updateBranch = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var branchData = {

        "Company_city": req.body.Company_city,
        "Company_city_address": req.body.Company_city_address,
        "Company_gmap": req.body.Company_gmap,
        "Company_state": req.body.Company_state,
        "Company_GSTIN": req.body.Company_GSTIN,
        "Location_id": req.body.Location_id
    }
    await BranchesTb.update(branchData, {
            where: { Branches_id: req.body.Branch_id },
            include: {
                model: locationsTb,
                required: true,
            }
        }).then(async data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });

};


exports.companyProfileCompletion = async(req, res) => {
    console.log('Company completion check');
    console.log(req.body)
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    await usersTb.findOne({
            where: { User_id: req.body.User_id },
            include: [{
                model: companyTb,
                required: true,
                where: { AgreeTerms: 1 },
                include: [{
                    model: GovermentTbs,
                    required: true,
                }, {
                    model: bankdetailsTb,
                    required: true,
                }]
            }, ]
        }, ).then(data => {
            console.log(data)
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};
exports.checkcompnayRegistrationCompleted = async(req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    await usersTb.count({
            where: { User_id: req.body.User_id },
            include: [{
                model: companyTb,
                required: true,
                include: [{
                        model: GovermentTbs,
                        required: true,
                    },
                    {
                        model: bankdetailsTb,
                        required: true,
                    }
                ]
            }, ]
        }, ).then(data => {
            res.send({ count: data });
        })
        .catch(err => {
            console.log(err);
        });
};


exports.passwordReset = async(req, res) => {

    console.log(req.body);
    async function passwordHashing(newPassword) {
        return bcrypt.hashSync(newPassword, 10);
    }
    var pass = await passwordHashing(req.body.User_password);
    var currentassword = req.body.Current_password;


    async function userPasswordChange() {

        var passwords = {
            'User_password': pass
        }

        await usersTb.findOne({
                where: { User_id: req.body.User_id },
            }, ).then(async data => {
                if (bcrypt.compareSync(currentassword, data.User_password)) {
                    await usersTb.update(passwords, {
                        where: {
                            User_id: req.body.User_id
                        }
                    }).then(num => {
                        res.send({ status: true });
                    })
                } else {
                    res.send({ status: false });
                }
            })
            .catch(err => {
                console.log(err);
                res.send({ status: false });
            });

    }

    async function resourcePasswordChange() {

        var resourcePasswords = {
            'Resource_Password': pass
        }

        await resourceTb.findOne({
                where: { Resource_id: req.body.Resource_id },
            }, ).then(async data => {
                if (bcrypt.compareSync(currentassword, data.Resource_Password)) {
                    console.log(resourcePasswords)
                    await resourceTb.update(resourcePasswords, {
                        where: {
                            Resource_id: req.body.Resource_id
                        }
                    }).then(num => {
                        console.log(num);
                        if (num == 1) {
                            res.send({ status: true });
                        } else {
                            res.send({ status: false });
                        }
                    })
                } else {
                    res.send({ status: false });
                }
            })
            .catch(err => {
                console.log(err);
                res.send({ status: false });
            });

    }
    if (req.body.User_id !== null) {
        console.log('User password reset');
        await userPasswordChange();
    } else if (req.body.Resource_id !== null) {
        console.log('Resource password reset');
        resourcePasswordChange();
    } else {
        res.send({ status: false });
    }
};


exports.getComplaintsParentCategory = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    ComplaintsCategoryTbs.findAll({ where: { Category: 'Parent' } }).then(data => {
        res.send(data);
    });

};

exports.getComplaintsResourceCategory = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    ComplaintsCategoryTbs.findAll({ where: { Category: 'Resource' } }).then(data => {
        res.send(data);
    });

};

exports.getComplaintsProjectCategory = async(req, res) => {
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    ComplaintsCategoryTbs.findAll({ where: { Category: 'Project' } }).then(data => {
        res.send(data);
    });

};

exports.addComplaints = async(req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var c = {
        User_id: req.body.User_id,
        Company_id: req.body.Company_id,
        Complaints: req.body.description,
        Screenshot: req.body.Screenshot,
        Url: req.body.Url,
        Complain_Date: req.body.Complain_Date,
        Complain_status: req.body.Complain_status,
        ComplaintsCategory_id: req.body.parentCat,
        Sub_Category: req.body.Sub_Category,
        Title: req.body.title,
        Project_id: req.body.project_id > 0 ? req.body.project_id : null,
        Requirement_id: req.body.requirement_id > 0 ? req.body.requirement_id : null,
        Resource_id: req.body.resource_id > 0 ? req.body.resource_id : null
    }
    ComplaintsTb.create(c).then(data => {
        res.send(data);
    });

};

exports.getProjects = async(req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    projectTb.findAll({ where: { User_id: req.body.User_id } }).then(data => {
        res.send(data);
    });

};

exports.getAssignmentsByProjectId = async(req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    requirementTb.findAll({ where: { Project_id: req.body.Project_id } }).then(data => {
        res.send(data);
    });

};

exports.assignmentSelection = async(req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    assignTb.findAll({ where: { Requirement_id: req.body.Requirement_id } }).then(data => {
        res.send(data);
    });

};

exports.getCurrentComplaints = async(req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    ComplaintsTb.findAll({
        where: { User_id: req.body.User_id },
        include: [{
            model: ComplaintsCategoryTbs,
            required: false,
        }, {
            model: projectTb,
            required: false,
        }]
    }).then(data => {
        res.send(data);
    });

};

exports.getLocationById = async(req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    locationsTb.findOne({
        where: { Locations_id: req.body.Locations_id, Location_status: 'ACTIVE' },
    }).then(data => {
        res.send(data);
    });

};

exports.updateLocationById = async(req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var c = {
        "Locations_name": req.body.Location_Form.Locations_name,
        "Location_country": req.body.Location_Form.Locations_country,
        "Location_pincode": req.body.Location_Form.Locations_pincode,
        "Location_state": req.body.Location_Form.Locations_state,
    }
    locationsTb.update(c, { where: { Locations_id: req.body.Locations_id } }).then(data => {
        res.send(data);
    });
};

exports.submitFeedback = async(req, res) => {
    console.log(req.body);
    if (!req.body.User_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    var feedbackdata = {
        "Feedback": req.body.Feedback_Form.feedbackDescription,
        "User_id": req.body.User_id,
    }
    async function getUserMailID() {
        return await usersTb.findOne({ where: { User_id: req.body.User_id } });
    }
    FeedbackTbs.create(feedbackdata).then(async data => {
        var userData = await getUserMailID();
        var userFirstname = userData.User_firstname;
        var subject = "Feedback sent successfully";
        var mailBody = "Dear" + userFirstname + ",<br>";
        mailBody += "Thank you for your feedback.";
        var templatePath = "../../uploads/templates/feedbackReplay.handlebars";
        var reciverMail = userData.User_email;
        await sendMail(reciverMail, subject, mailBody, templatePath);
        res.send(data);
    });

};


exports.saveLiveVideo = async(req, res) => {


    var form = new IncomingForm();

    form.parse(req, async(err, fields, files) => {
        const uploadFile = (fileName) => {
            const fileContent = fs.createReadStream(fileName.file.path);
            var newname = Math.floor(Math.random() * 100000) + fileName.file.name;

            const params = {
                Bucket: BUCKET_NAME + '/Videos/live',
                Key: newname,
                Body: fileContent
            };

            s3.upload(params, async function(err, data) {
                if (err) {
                    throw err;
                }
                console.log(`live successfully. ${data.Location}`);
                console.log(data);
                const liveVideoData = {
                    Resource_id: fields.Resource_id,
                    Intro_video: data.Location,
                };
                await resourceTb.update(liveVideoData, {
                    where: { Resource_id: fields.Resource_id }
                }).then(async num => {
                    if (num == 1) {
                        console.log('Video DB updated' + num);
                        res.send({
                            status: true
                        });
                    } else {
                        res.send({
                            status: false
                        });
                    }
                }).catch(err => {
                    console.log(err);
                    res.status(500).send({
                        message: "Error updating Tutorial with id=" + id
                    });
                });

            });
        };
        uploadFile(files);
    });




    // form.parse(req, async(err, fields, files) => {
    //     var base64Data = fields.file

    //     console.log(fields);




    //     // var newname = Math.floor(Math.random() * 100000) + 'blobFile';
    //     // const params = {
    //     //     Bucket: BUCKET_NAME + '/Videos/live',
    //     //     Key: newname,
    //     //     Body: filess
    //     // };
    //     // s3.upload(params, async function(err, data) {
    //     //     if (err) {
    //     //         throw err;
    //     //     }
    //     //     console.log(`Video uploaded successfully. ${data.Location}`);
    //     //     const liveVideoData = {
    //     //         Resource_id: fields.Resource_id,
    //     //         Intro_video: data.Location,
    //     //     };
    //     //     return await resourceTb.update(liveVideoData, {
    //     //             where: { Resource_id: fields.Resource_id }
    //     //         }).then(async num => {
    //     //             if (num == 1) {
    //     //                 console.log('Video DB updated' + num);
    //     //                 res.send({
    //     //                     status: true
    //     //                 });
    //     //             } else {
    //     //                 res.send({
    //     //                     status: false
    //     //                 });
    //     //             }
    //     //         })
    //     //         .catch(async err => {
    //     //             console.log(err);
    //     //             res.send({
    //     //                 status: false
    //     //             });
    //     //         });
    //     // });

    // });

};


exports.getResourceIntroVideo = (req, res) => {

    resourceTb.findOne({ where: { Resource_id: req.body.Resource_id } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });
};

exports.getIndianStates = (req, res) => {

    IndianStatesTbs.findAll()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);

        });
};

exports.getResourceReleaseData = (req, res) => {

    resourceTb.findAll({
            where: { Resource_id: req.body.Resource_id },
            include: [{
                model: assignTb,
                required: true,
                where: {
                    Requirement_id: req.body.Requirement_id,
                    Resource_id: req.body.Resource_id,
                    Approved_status: {
                        [Op.ne]: 'Released'
                    }
                },
                include: {
                    model: requirementTb,
                    required: true,
                    where: { Requirement_id: req.body.Requirement_id }
                }
            }, ]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);

        });
};

exports.resourceRelease = async(req, res) => {
    console.log(req.body);
    let c = {
        'Requirement_id': req.body.Requirement_id,
        'Resource_id': req.body.Resource_id,
        'Release_type': req.body.Release_data.Release_type,
        'Released_by': req.body.Release_data.Released_by,
        'Release_date': req.body.Release_data.Release_date,
        'Release_reason': req.body.Release_data.Release_reason,
        'Rating': req.body.Release_data.Rating,
        'Release_explanation': req.body.Release_data.Release_explanation,
    }
    ReleaseResourcesTbs.findOne({ where: { Resource_id: req.body.Resource_id, Requirement_id: req.body.Requirement_id } }).then(async function(obj) {
        if (!obj) {
            console.log('Resource Releasing.....');
            ReleaseResourcesTbs.create(c).then(async(data) => {
                await updateResourceTb();
                await updateAssignTb();
                await mailNotification();
                res.send(data);
            })
        }
    });

    async function updateResourceTb() {
        let status = {
            'Resource_status': 'AVAILABLE'
        }
        resourceTb.update(status, { where: { Resource_id: req.body.Resource_id } });
    }
    async function updateAssignTb() {
        let status = {
            'Approved_status': 'Released'
        }
        assignTb.update(status, { where: { Resource_id: req.body.Resource_id, Requirement_id: req.body.Requirement_id } });
    }
    async function requirementData() {
        return await requirementTb.findOne({
            where: { Requirement_id: req.body.Requirement_id },
            include: {
                model: usersTb,
                required: true,
                include: {
                    model: companyTb,
                    required: true
                }
            }
        });

    }
    async function mailNotification() {

        resourceTb.findOne({
            where: { Resource_id: req.body.Resource_id },
            include: [{
                    model: usersTb,
                    required: true
                },
                {
                    model: assignTb,
                    required: true,
                    where: { Requirement_id: req.body.Requirement_id },
                }
            ]
        }).then(async data => {
            let RequirementsTb = await requirementData();
            let resourceName = data.Resource_name;
            let litingManagerEmail = data.UsersTb.User_email;
            let litingManagerName = data.UsersTb.User_firstname;
            let hiringManager_mail = RequirementsTb.UsersTb.User_email;
            let hiringManagerName = RequirementsTb.UsersTb.User_firstname;
            let requirementName = RequirementsTb.Requirement_name;

            var invoiceBodyMail = 'Dear ' + litingManagerName + ',<br>';
            invoiceBodyMail += 'Your closure summary is given below.<br>';
            invoiceBodyMail += 'Resource Name :' + resourceName + '<br>';
            invoiceBodyMail += 'Requirement Name :' + requirementName + '<br>';
            invoiceBodyMail += 'Hiring Manager :' + hiringManagerName + '<br>';
            invoiceBodyMail += 'Closure Date :' + req.body.Release_data.Release_date + '<br>';
            invoiceBodyMail += 'Release Reason :' + req.body.Release_data.Release_reason + '<br>';
            invoiceBodyMail += 'Release Explanation :' + req.body.Release_data.Release_reason_details + '<br>';
            invoiceBodyMail += 'Rating :' + req.body.Release_data.Rating + '/5<br>';
            let subject = "Resource Closure | " + requirementName;
            let templatemail = "../../uploads/templates/resourceClosure.handlebars";
            await sendMail(litingManagerEmail, subject, invoiceBodyMail, templatemail);

        })

    }

};


exports.updateResourceCount = (req, res) => {
    console.log(req.body);
    let c = {
        'No_of_resources': req.body.No_of_resources
    }
    requirementTb.update(c, { where: { Requirement_id: req.body.Requirement_id } }).then((data) => {
        if (data == 1) {
            res.send({ status: true });

        } else {
            res.send({ status: false });

        }

    })
};


exports.getProjectDataByUserid = (req, res) => {

    projectTb.findAll({ where: { User_id: req.body.User_id } })
        .then(data => {
            res.send(data);
        }).catch(err => {
            console.log(err);
        });
};
exports.getRequirementDataByProjectid = (req, res) => {

    requirementTb.findAll({
            where: { Project_id: req.body.Project_id },
            include: [{
                model: assignTb,
                required: false,
                include: {
                    model: resourceTb,
                    required: false,
                }
            }, {
                model: projectTb,
                required: true,
                where: { Project_id: req.body.Project_id }
            }]
        })
        .then(data => {
            res.send(data);
        }).catch(err => {
            console.log(err);
        });
};

exports.requiremetDateChange = async(req, res) => {

    console.log(req.body);
    let incomingdate = req.body.Date;
    incomingdate = moment(incomingdate, 'YYYY-MM-DD');
    incomingdate = incomingdate.format('YYYY-MM-DD');

    let projectDate = moment(req.body.projectDate, 'YYYY-MM-DD[T]HH:mm:ss');
    projectDate = projectDate.format('YYYY-MM-DD');

    async function extendProject() {
        let c = {
            'End_date': incomingdate
        }
        console.log(c);
        projectTb.update(c, { where: { Project_id: req.body.Project_id } });
    }

    if (req.body.type === 'End') {

        let c = {
            'Requirement_end': incomingdate
        }
        requirementTb.update(c, { where: { Requirement_id: req.body.Requirement_id } }).then(async data => {
            if (data == 1) {
                if (moment(incomingdate).isAfter(projectDate, 'days')) {
                    console.log('Project extended');
                    await extendProject();
                } else {
                    console.log('Project not extended');
                    console.log(incomingdate);
                    console.log(projectDate);

                }
                res.send({ status: true });
            } else {
                res.send({ status: false });
            }
        });

    } else if (req.body.type === 'Start') {
        let c = {
            'Requirement_start': incomingdate
        }
        requirementTb.update(c, { where: { Requirement_id: req.body.Requirement_id } }).then(async data => {
            if (data == 1) {
                if (moment(incomingdate).isAfter(projectDate, 'days')) {
                    await extendProject();
                }
                res.send({ status: true });
            } else {
                res.send({ status: false });
            }
        });
    }
};

exports.extendResource = async(req, res) => {
    console.log(req.body);
    async function addtoExtendTb(User_id) {

        let c = {
            'Requirement_id': req.body.Requirement_id,
            'Resource_id': req.body.Resource_id,
            'Extend_by': User_id,
            'Extend_date': req.body.Date,
            'Status': 'PENDING'

        }

        ExtendTbs.findOne({ where: { Resource_id: req.body.Resource_id, Requirement_id: req.body.Requirement_id } }).then(async function(data) {
            if (data) {
                return await ExtendTbs.update(c, { where: { Resource_id: req.body.Resource_id, Requirement_id: req.body.Requirement_id } });
            } else {
                return await ExtendTbs.create(c);

            }
        });

    }

    resourceTb.findOne({
        where: { Resource_id: req.body.Resource_id },
        include: {
            model: usersTb,
            required: true,
        }
    }).then(async data => {
        let resourceName = data.Resource_name;
        let listingManagerName = data.UsersTb.User_firstname;
        let listingManagerEmail = data.UsersTb.User_email;
        let User_id = data.UsersTb.User_id;

        var invoiceBodyMail = '<div> Dear ' + listingManagerName + ' , <br>';
        invoiceBodyMail += 'Your resource ' + resourceName + ' avaialble date has to be updated in the Application if its possible.';
        invoiceBodyMail += 'Expected date : ' + req.body.Date;
        var subject = "Resource Extension | Action Required";
        var templatemail = "../../uploads/templates/resourceExtension.handlebars";
        await addtoExtendTb(User_id);
        await sendMail(listingManagerEmail, subject, invoiceBodyMail, templatemail);
        res.send({ status: true });
    });
};

exports.resourceExtendRequests = (req, res) => {

    console.log(req.body);
    ExtendTbs.findAll({
            include: [{
                    model: resourceTb,
                    required: true,
                    where: {
                        [Op.or]: [{
                                Manager_id: req.body.User_id
                            },
                            {

                                Created_by: req.body.User_id,
                            }
                        ]
                    }
                },
                {
                    model: requirementTb,
                    required: true
                }
            ]
        }).then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

exports.extensionRequestHandler = async(req, res) => {

    async function respourceDateExtend() {
        let r = {
            'Min_available_to': req.body.result.new_minDate,
            'Available_to': req.body.result.newDate
        }
        await resourceTb.update(r, { where: { Resource_id: req.body.Resource_id } })

    }
    async function sendMailtoHmanager() {
        ExtendTbs.findOne({
            where: { Extend_id: req.body.Extend_id },
            include: {
                model: requirementTb,
                required: true,
                include: {
                    model: usersTb,
                    required: true
                }
            }
        }).then(async data => {
            let hmanager_email = data.RequirementsTb.UsersTb.User_email;
            let hmanager_name = data.RequirementsTb.UsersTb.User_firstname;

            let bodyMail = '<div> Dear ' + hmanager_name + ' , <br>Your extension request has been responded. Please check it now.    </div>';
            let subject = "Extension Request Resposnded | ExpertsQ";
            let templatemail = "../../uploads/templates/acceptExtension.handlebars";
            await sendMail(hmanager_email, subject, bodyMail, templatemail);


        });
    }

    let c = {
        'Status': req.body.Status
    }
    ExtendTbs.update(c, {
            where: { Extend_id: req.body.Extend_id }
        })
        .then(async num => {
            if (num == 1) {
                if (req.body.Status !== 'REJECTED') {
                    console.log('ACCEPTED...');
                    await respourceDateExtend();
                    await sendMailtoHmanager();

                } else {

                    await sendMailtoHmanager();
                }

                res.send({
                    status: true
                });
            } else {
                res.send({
                    status: false
                });
            }
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: "Error updating Tutorial with id=" + id
            });
        });
};

exports.getInterviewDetialsById = (req, res) => {

    interviewTb.findOne({ where: { Interview_id: req.body.Interview_id } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });
};
exports.getBracnhDataById = (req, res) => {

    bankdetailsTb.findOne({ where: { Bank_id: req.body.Bank_id } })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });
};
exports.updateBankAccount = (req, res) => {
    console.log(req.body);
    let cBank = {
        'Bank_name': req.body.Bank_name,
        'Bank_branch': req.body.Bank_Branch,
        'Bank_address': req.body.Bank_address,
        'Bank_accountNumber': req.body.Bank_accountNumber,
        'Bank_IFSC': req.body.Bank_IFSC,
        'Branches_id': req.body.Bank_branch_id,
    }
    bankdetailsTb.update(cBank, { where: { Bank_id: req.body.Bank_id } })
        .then(data => {
            console.log(data);
            res.send(data);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({
                message: "Error retrieving Tutorial with id=" + id
            });
        });
};
exports.getCandidates = async(req, res) => {

    console.log(req.body);
    await CandidatesTbs.findAll({
            where: { User_id: req.body.User_id },
            include: [{
                    model: resourceTb,
                    required: true
                },
                {
                    model: requirementTb,
                    required: true
                }
            ]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err);
        });
};


exports.companyLogoUpdate = async(req, res) => {

    var form = new IncomingForm();
    form.multiples = true;

    let User_id = "";


    form.parse(req, async(err, fields, files) => {
        User_id = fields.User_id;
        const uploadFile = (fileName) => {
            console.log('--------------------------------');
            console.log('--------------------------------');
            const base64Data = new Buffer.from(fileName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
            const type = fileName.split(';')[0].split('/')[1];
            var newname = 'profile' + Math.floor(Math.random() * 100000) + '.' + type;

            const params = {
                Bucket: BUCKET_NAME + '/Logos',
                Key: newname,
                ContentEncoding: 'base64', // required
                ContentType: `image/${type}`,
                Body: base64Data
            };

            s3.upload(params, async function(err, data) {
                if (err) {
                    throw err;
                }
                console.log(`logo uploaded successfully. ${data.Location}`);
                console.log(data);

                let fdata = {
                    'Company_logo': data.Location,
                }
                await updateCompany(fdata);

            });
        };
        uploadFile(fields.file);
    });

    async function updateCompany(fdata) {
        await usersTb.findOne({ where: { User_id: User_id } }).then(async data => {
            console.log('Company ID :' + data.Company_id);
            await companyTb.update(fdata, { where: { Company_id: data.Company_id } }).then(data => {
                console.log('data.Company_logo' + fdata.Company_logo);
                res.send({ Status: fdata.Company_logo });
            })

        });
    }
};