const db = require("../models");
const companyTb = db.companyTb;
const domainTb = db.domainTb;
const educationTb = db.educationTb;
const resourceTb = db.resourceTb;
const requirementTb = db.requirement;
const projectTb = db.project;
const technologyTb = db.technology;
const ReleaseResourcesTbs = db.ReleaseResourcesTbs;
const rolesTb = db.roles;
const usersTb = db.user;
const Op = db.Sequelize.Op;
const SelectedDomainsTb = db.SelectedDomains;
const SelectedQualificationsTb = db.SelectedQualifications;
const CandidatesTbs = db.CandidatesTbs
const SelectedTechTb = db.SelectedTech;
const selectedRolesTb = db.SelectedRoles;
const techcategoryTb = db.techcategory;
const resourceRoleTbs = db.resourceRoleTbs;
const resourceDomainTbs = db.resourceDomainTbs;
const resourceTechnologyTbs = db.resourceTechnologyTbs;
const resourceEducationTbs = db.resourceEducationTbs;
var Sequelize = require("sequelize");
var nodemailer = require('nodemailer');
const jobs = require('../controllers/cron.controller');
const moment = require('moment');
moment.suppressDeprecationWarnings = true;
const path = require('path');
var fs = require('fs');
var handlebars = require('handlebars');
const MasterTbs = db.MasterTbs;


async function getMasterTableData(category) {
    return await MasterTbs.findOne({ where: { Category: category } });
}


exports.projectMatching = async(req, res) => {
    if (!req.body.Requirement_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    const requirement_id = req.body.Requirement_id;
    var resource_List = [];
    let requirement_data = "";

    async function getRequirement_data() {
        let data = await requirementTb.findOne({
            where: {
                Requirement_id: requirement_id
            },
            include: [{
                model: projectTb,
                include: [{
                    model: selectedRolesTb,
                    required: false,
                    where: { Requirement_id: requirement_id }
                }, {
                    model: SelectedDomainsTb,
                    required: false,
                    where: { Requirement_id: requirement_id }
                }, {
                    model: SelectedTechTb,
                    required: false,
                    where: { Requirement_id: requirement_id }
                }, {
                    model: SelectedQualificationsTb,
                    required: false,
                    where: { Requirement_id: requirement_id }
                }],
                required: false,

            }]
        }).then(data => {
            return data
        }).catch(err => {
            return err
        });
        return data
    }
    requirement_data = await getRequirement_data();
    //res.send(requirement_data);
    let requirement_start = requirement_data.Requirement_start;
    let requirement_end = requirement_data.Requirement_end;


    let maching_role_users = [];
    let R_maching = [];
    let number_of_requirements = 0;
    let matching = 0;
    const roleMatching = 10;
    const educationMatching = 10;
    const technologyMatching = 50;
    const domainMatching = 10;
    const availabilityMatching = 15;
    const isVideoMatching = 5;

    async function maching_Role() {
        if (Object.keys(requirement_data.ProjectsTb['SelectedRolesTbs']).length > 0) {
            let totalRoles = 0;
            let R_role_list = requirement_data.ProjectsTb['SelectedRolesTbs'];
            for (const val of R_role_list) {

                var rol = await resourceRoleTbs.findAll({
                    where: {
                        RRole_name: val['Roles'],
                    }
                })
                totalRoles = Object.keys(requirement_data.ProjectsTb['SelectedRolesTbs']).length;
                let matchingPercentage = roleMatching / totalRoles;
                matchingPercentage = matchingPercentage / 2; //role,expt
                console.log("Role divion by " + matchingPercentage);
                rol.forEach(el => {
                    var p = 0;
                    if (el.Resource_id != null) {
                        if (el.RRole_duration >= val['Job_duration']) {
                            p++;
                            console.log("Role duration matched " + p);
                        }
                        if (el.RRole_name == val['Roles']) {
                            p++;
                            console.log("Role name matched " + p);
                        }
                        maching_role_users.push(el.Resource_id);
                        var c = {
                            "Resource_id": el.Resource_id,
                            "Rolemaching": (matchingPercentage * p)
                        }
                        R_maching.push(c);
                    }
                });

            }
            console.log("Role machers : " + maching_role_users);
        }
    }
    async function maching_Technology() {
        if (Object.keys(requirement_data.ProjectsTb['SelectedTechnologiesTbs']).length > 0) {

            totalTechs = Object.keys(requirement_data.ProjectsTb['SelectedTechnologiesTbs']).length;
            let matchingPercentage = technologyMatching / totalTechs;
            matchingPercentage = matchingPercentage / 2; //expt,level,version
            console.log("Tech division by:" + matchingPercentage);

            let R_technology_list = requirement_data.ProjectsTb['SelectedTechnologiesTbs'];
            for (const val of R_technology_list) {
                number_of_requirements++;
                var rol = await resourceTechnologyTbs.findAll({
                    where: {
                        RTechnology_name: val['Technology']
                    }
                });
                rol.forEach(el => {
                    var p = 0;
                    if (el.Resource_id != null) {
                        if (el.RTechnology_duration == val['Technology_experience']) {
                            p++;
                            console.log("Technology experience matched." + p);
                        }
                        if (el.RTechnology_level == val['Technology_level']) {
                            p++;
                            console.log("Technology level matched." + p);
                        }
                        // if(el.RTechnology_version == val['Technology_version'])
                        // {
                        //   p++;
                        //   console.log("Technology version matched."+p);
                        // }
                        var c = {
                            "Resource_id": el.Resource_id,
                            "Technologymaching": (matchingPercentage * p)
                        }
                        R_maching.push(c);
                        maching_role_users.push(el.Resource_id);
                    }
                });
            }
            console.log("Tech machers : " + maching_role_users);
        }
    }
    async function maching_Education() {
        if (Object.keys(requirement_data.ProjectsTb['SelectedQualificationsTbs']).length > 0) {
            let R_technology_list = requirement_data.ProjectsTb['SelectedQualificationsTbs'];
            let totalEdu = Object.keys(requirement_data.ProjectsTb['SelectedQualificationsTbs']).length;
            for (const val of R_technology_list) {
                number_of_requirements++;
                var rol = await resourceEducationTbs.findAll({
                    where: {
                        REducation: val['Qualifications']
                    }
                });

                let matchingPercentage = educationMatching / totalEdu;
                matchingPercentage = matchingPercentage; //Education
                console.log("Education divion by " + matchingPercentage);

                rol.forEach(el => {
                    var p = 0;
                    if (el.Resource_id != null) {
                        if (el.REducation == val['Qualifications']) {
                            p++;
                            console.log("Education " + p);
                        }
                        var c = {
                            "Resource_id": el.Resource_id,
                            "Educationmaching": (matchingPercentage * p)
                        }
                        R_maching.push(c);
                        maching_role_users.push(el.Resource_id);
                    }
                });
            }
            console.log("Education machers : " + maching_role_users);
        }
    }
    async function maching_Domain() {
        if (Object.keys(requirement_data.ProjectsTb['SelectedDomainsTbs']).length > 0) {
            let R_technology_list = requirement_data.ProjectsTb['SelectedDomainsTbs'];
            let totalDomain = Object.keys(requirement_data.ProjectsTb['SelectedDomainsTbs']).length;

            for (const val of R_technology_list) {
                console.log('Total Domains ' + val['Domains']);
                number_of_requirements++;
                var rol = await resourceDomainTbs.findAll({
                    where: {
                        RDomain: val['Domains']
                    }
                });

                let matchingPercentage = domainMatching / totalDomain;
                matchingPercentage = matchingPercentage / 2; //Domain,expt
                console.log("Domain divion by " + matchingPercentage);

                rol.forEach(el => {
                    var p = 0;
                    if (el.Resource_id != null) {
                        if (el.RDomain == val['Domains']) {
                            p++;
                            console.log("Domain mached" + p);
                        }
                        if (el.RDomain_duration >= val['Domain_duration']) {
                            p++;
                            console.log("Domain duration mached" + p);
                        }

                        var c = {
                            "Resource_id": el.Resource_id,
                            "Domainmaching": (matchingPercentage * p)
                        }
                        R_maching.push(c);
                        maching_role_users.push(el.Resource_id);
                    }
                });
            }
            console.log("Domains machers : " + maching_role_users);
        }
    }



    async function resourceAvailability() {
        console.log("Total machers! : " + maching_role_users);
        let finalResource = [];
        for (const val of maching_role_users) {
            var rol = await resourceTb.findAll({
                where: {
                    Resource_id: val,
                    Available_from: {
                        [Op.between]: [requirement_start, requirement_end],
                    },
                    Available_to: {
                        [Op.between]: [requirement_start, requirement_end],
                    },
                    Resource_status: {
                        [Op.ne]: 'SELECTED',
                    }
                }
            });
            console.log('requirement_start' + requirement_start);
            console.log(rol);
            console.log('requirement_end' + requirement_end);
            rol.forEach(el => {
                if (el.Resource_id != null && finalResource.includes(el.Resource_id) == false) {

                    var c = {
                        "Resource_id": el.Resource_id,
                        "Availabilitymaching": availabilityMatching
                    }
                    R_maching.push(c);
                    if (el.Intro_video != '') {
                        var c = {
                            "Resource_id": el.Resource_id,
                            "Videomaching": isVideoMatching
                        }
                        R_maching.push(c);

                    }
                    finalResource.push(el.Resource_id);
                }
            });
        }
        maching_role_users = [];
        maching_role_users = finalResource;
        console.log("Available machers : " + maching_role_users);
    }
    async function matchingResourceData() {
        let resourceLists = [];

        for (const element of resourceList) {
            var rol = await resourceTb.findAll({
                where: {
                    Resource_id: element.Resource_id,
                },
                include: {
                    model: companyTb,
                    required: true
                }
            });
            rol.forEach(el => {
                var c = {
                    "Resource_id": el.Resource_id,
                    "Resource_name": el.Resource_name,
                    "Resource_rate": el.Resource_rate,
                    "Resource_status": el.Resource_status,
                    "Available_from": el.Available_from,
                    "Resource_photo": el.Resource_photo,
                    "Available_to": el.Available_to,
                    "Created_by": el.Created_by,
                    "RoleMatching": element.RoleMatching,
                    "DomainMatching": element.DomainMatching,
                    "TechnologyMatching": element.TechnologyMatching,
                    "EducationMatching": element.EducationMatching,
                    "Availabilitymaching": element.Availabilitymaching,
                    "Videomaching": element.Videomaching,
                    "Matching": element.Matching,
                    "CompanyTb": el.CompanyTb,
                    "Requirement_id": requirement_id
                }
                resourceLists.push(c);
            });

        }
        resourceLists.sort(function(a, b) {
            return a.Matching + b.Matching;
        });
        return resourceLists;
    }

    async function arrangeResource() {
        let people = [];
        const groupBy = key => array =>
            array.reduce((objectsByKeyValue, obj) => {
                const value = obj[key];
                objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
                return objectsByKeyValue;
            }, {});
        var groupByBrand = groupBy('Resource_id');
        let datas = groupByBrand(R_maching);
        let meData = [];


        for (const [key, value] of Object.entries(datas)) {
            value.forEach(element => {
                people.push(element);

            });
        }
        var roleMach = 0;
        var domainMach = 0;
        var techMach = 0;
        var eduMach = 0;
        var availMach = 0;
        var videoMach = 0;

        for (var id of maching_role_users) {
            people.forEach(element => {
                if (id == element.Resource_id) {
                    if (element.Rolemaching >= 0) {
                        roleMach += element.Rolemaching;
                    }
                    if (element.Domainmaching >= 0) {
                        domainMach += element.Domainmaching;
                    }
                    if (element.Technologymaching >= 0) {
                        techMach += element.Technologymaching;
                    }
                    if (element.Educationmaching >= 0) {
                        eduMach += element.Educationmaching;
                    }
                    if (element.Availabilitymaching >= 0) {
                        availMach += element.Availabilitymaching;
                    }
                    if (element.Videomaching >= 0) {
                        videoMach += element.Videomaching;
                    }
                }

            });
            var c = {
                "Resource_id": id,
                "RoleMatching": roleMach,
                "DomainMatching": domainMach,
                "TechnologyMatching": techMach,
                "EducationMatching": eduMach,
                "Availabilitymaching": availMach,
                "Videomaching": videoMach,
                "Matching": Math.round((roleMach + domainMach + techMach + eduMach + availMach + videoMach))
            }
            meData.push(c);
            roleMach = 0;
            domainMach = 0;
            techMach = 0;
            eduMach = 0;
            availMach = 0;
            videoMach = 0;
        }

        return meData;
    }
    await maching_Role();
    await maching_Technology();
    await maching_Education();
    await maching_Domain();
    await resourceAvailability();
    let resourceList = await arrangeResource();
    let rData = await matchingResourceData();
    res.send(rData);
};



exports.resourceMatching = async(req, res) => {
    console.log(req.body);
    if (!req.body.Resource_id) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    const Resource_id = req.body.Resource_id;
    let RIDS = [];
    let uniqueRIDS = [];
    let difference;
    const ResourceData = await getResourceData();
    const resourceDomains = await getResourceDomainIds();
    const resourceRoles = await getResourceRoleIds();
    const resourceQualifications = await getResourceQualificationIds();
    const resourceTechnology = await getResourceTechIds();



    await requirementDomains(resourceDomains);
    await requirementRoles(resourceRoles);
    await requirementQualifications(resourceQualifications);
    await requirementTechnology(resourceTechnology);
    await countRequirementIds();
    await getRequirements().then(data => {
        let finalData = [];
        data.forEach(element => {
            let c = {
                Requirement_id: element.Requirement_id,
                Company_id: element.Company_id,
                Requirement_name: element.Requirement_name,
                Requirement_start: element.Requirement_start,
                Requirement_end: element.Requirement_end,
                Project_id: element.Project_id,
                CompanyTb: element.CompanyTb,
                CandidatesTbs: element.CandidatesTbs,
                difference: difference[element.Requirement_id],
            }
            finalData.push(c);

        });
        return finalData;
    }).then(data => {

        data.sort(GetSortOrder("difference"));
        res.send(data)
    })

    function GetSortOrder(prop) {
        return function(a, b) {
            if (a[prop] < b[prop]) {
                return 1;
            } else if (a[prop] > b[prop]) {
                return -1;
            }
            return 0;
        }
    }

    async function getResourceData() {
        return await resourceTb.findOne({ where: { Resource_id: Resource_id } });
    }
    async function getRequirements() {
        return await requirementTb.findAll({
            where: {
                Requirement_id: {
                    [Op.in]: uniqueRIDS
                },
            },
            include: [{
                    model: companyTb,
                    required: true
                },
                {
                    model: CandidatesTbs,
                    required: false
                }
            ]
        }).then(data => {
            return data;

        })
    }
    async function countRequirementIds() {
        const counts = {};
        const sampleArray = RIDS.sort((a, b) => (a > b ? -1 : 1))
        sampleArray.forEach(function(x) { counts[x] = (counts[x] || 0) + 1; });

        RIDS.forEach((c) => {
            if (!uniqueRIDS.includes(c)) {
                uniqueRIDS.push(c);
            }
        });
        difference = counts;
    }
    async function getResourceTechIds() {
        return await resourceTechnologyTbs.findAll({
            where: { Resource_id: Resource_id },
        }).then(data => {
            let ids = [];
            data.forEach(element => {
                if (!ids.includes(element.Technology_id)) {
                    ids.push(element.Technology_id);
                }

            });
            return ids;
        })
    }
    async function getResourceQualificationIds() {
        return await resourceEducationTbs.findAll({
            where: { Resource_id: Resource_id },
        }).then(data => {
            let ids = [];
            data.forEach(element => {
                if (!ids.includes(element.Education_id)) {
                    ids.push(element.Education_id);
                }

            });
            return ids;
        })
    }
    async function getResourceDomainIds() {
        return await resourceDomainTbs.findAll({
            where: { Resource_id: Resource_id },
        }).then(data => {
            let ids = [];
            data.forEach(element => {
                if (!ids.includes(element.Domain_id)) {
                    ids.push(element.Domain_id);
                }

            });
            return ids;
        })
    }
    async function getResourceRoleIds() {
        return await resourceRoleTbs.findAll({
            where: { Resource_id: Resource_id },
        }).then(data => {
            console.log('Roles count' + Object.keys(data).length);
            let ids = [];
            data.forEach(element => {
                if (!ids.includes(element.Roles_id)) {
                    ids.push(element.Roles_id);
                }
            });
            return ids;
        })
    }
    async function requirementDomains(resourceDomains) {
        await SelectedDomainsTb.findAll({
            where: {
                Domain_id: {
                    [Op.in]: resourceDomains
                }
            }
        }).then(data => {
            data.forEach(element => {
                RIDS.push(element.Requirement_id);
            });
        });
    }
    async function requirementRoles(resourceRoles) {
        await selectedRolesTb.findAll({
            where: {
                Roles_id: {
                    [Op.in]: resourceRoles
                }
            }
        }).then(data => {
            data.forEach(element => {
                RIDS.push(element.Requirement_id);
            });
        });
    }
    async function requirementQualifications(ids) {
        await SelectedQualificationsTb.findAll({
            where: {
                Education_id: {
                    [Op.in]: ids
                }
            }
        }).then(data => {
            data.forEach(element => {
                RIDS.push(element.Requirement_id);
            });
        });
    }
    async function requirementTechnology(ids) {
        await SelectedTechTb.findAll({
            where: {
                Technology_id: {
                    [Op.in]: ids
                }
            }
        }).then(data => {
            data.forEach(element => {
                RIDS.push(element.Requirement_id);
            });
        });
    }


};


exports.getMatchingValues = async(req, res) => {
    console.log(req.body);
    const Requirement_id = req.body.Requirement_id;
    const Resource_id = req.body.Resource_id;

    let ResourceDomains = await getResourceDomains();
    let RequirementDomains = await getRequirementDomains();
    let ResourceRoles = await getResourceRoles();
    let RequirementRoles = await getRequirementRoles();
    let ResourceQualifications = await getResourceQualifications();
    let RequirementQualifications = await getRequirementQualifications();
    let ResourceTechs = await getResourceTechs();
    let RequirementTechs = await getRequirementTechs();

    const domainPercentage = await getMasterTableData('MatchingDomain');
    const technologyPercentage = await getMasterTableData('MatchingTechnology');
    const rolePercentage = await getMasterTableData('MatchingRoles');
    const educationPercentage = await getMasterTableData('MatchingEducation');
    const availabilityPercentage = await getMasterTableData('MatchingAvailability');
    const introvideoPercentage = await getMasterTableData('IntroVideoAvailability');
    const resourceData = await getResourceData();


    let DomainMatchCount = await domainMatching(domainPercentage.Value) || 0;
    let RoleMatchCount = await roleMatching(rolePercentage.Value) || 0;
    let qualificationMatchCount = await qualificationMatching(educationPercentage.Value) || 0;
    let techMatchingCount = await technologyMatching(technologyPercentage.Value) || 0;
    let availabilityMatchingcount = await availabilitMatching(availabilityPercentage.Value) || 0;
    let introvideoMatchingcount = await introVideoMatching(introvideoPercentage.Value) || 0;
    await matchHandler();
    async function matchHandler() {

        console.log('DomainMatchCount =' + DomainMatchCount);
        console.log('RoleMatchCount =' + RoleMatchCount);
        console.log('qualificationMatchCount =' + qualificationMatchCount);
        console.log('techMatchingCount =' + techMatchingCount);
        console.log('availabilityMatchingcount =' + availabilityMatchingcount);
        console.log('introvideoMatchingcount =' + introvideoMatchingcount);
        let totla = DomainMatchCount + RoleMatchCount + qualificationMatchCount + techMatchingCount + Number(availabilityMatchingcount) + Number(introvideoMatchingcount);
        console.log('Total Match count =' + totla);
        let finalMatchingData = {
            DomainMatchCount: DomainMatchCount || 0,
            RoleMatchCount: RoleMatchCount || 0,
            qualificationMatchCount: qualificationMatchCount || 0,
            techMatchingCount: techMatchingCount || 0,
            availabilityMatchingcount: availabilityMatchingcount || 0,
            introvideoMatchingcount: introvideoMatchingcount || 0,
            total: totla || 0
        };
        res.send(finalMatchingData);
    }



    async function getResourceDomains() {
        return await resourceDomainTbs.findAll({ where: { Resource_id: Resource_id } }).then(data => {
            return data;
        })
    }
    async function getResourceRoles() {
        return await resourceRoleTbs.findAll({ where: { Resource_id: Resource_id } }).then(data => {
            return data;
        })
    }
    async function getResourceQualifications() {
        return await resourceEducationTbs.findAll({ where: { Resource_id: Resource_id } }).then(data => {
            return data;
        })
    }
    async function getResourceTechs() {
        return await resourceTechnologyTbs.findAll({ where: { Resource_id: Resource_id } }).then(data => {
            return data;
        })
    }
    async function getRequirementDomains() {
        return await SelectedDomainsTb.findAll({ where: { Requirement_id: Requirement_id } }).then(data => {
            return data;
        })
    }
    async function getRequirementRoles() {
        return await selectedRolesTb.findAll({ where: { Requirement_id: Requirement_id } }).then(data => {
            return data;
        })
    }
    async function getRequirementQualifications() {
        return await SelectedQualificationsTb.findAll({ where: { Requirement_id: Requirement_id } }).then(data => {
            return data;
        })
    }
    async function getRequirementTechs() {
        return await SelectedTechTb.findAll({ where: { Requirement_id: Requirement_id } }).then(data => {
            return data;
        })
    }
    async function technologyMatching(percentage) {
        let TotalCount = Object.keys(RequirementTechs).length;
        let matched = (TotalCount / percentage) * 100;
        let counter = 0;
        RequirementTechs.forEach(element => {
            ResourceTechs.forEach(val => {
                if (val.Technology_id == element.Technology_id && val.RTechnology_duration >= element.Technology_experience) {
                    console.log('Techs matched:' + val.Technology);
                    counter++;
                }
            });
        });
        let matching = (counter / matched) * 100;
        return Math.round(matching);
    }
    async function qualificationMatching(percentage) {
        let TotalCount = Object.keys(RequirementQualifications).length;
        let matched = (TotalCount / percentage) * 100;

        let counter = 0;
        RequirementQualifications.forEach(element => {
            ResourceQualifications.forEach(val => {
                if (val.Education_id == element.Education_id) {
                    console.log('Qualifications matched:' + val.REducation);
                    counter++;
                }
            });
        });
        let matching = (counter / matched) * 100;
        return Math.round(matching);
    }
    async function roleMatching(percentage) {
        let TotalCount = Object.keys(RequirementRoles).length;
        let matched = (TotalCount / percentage) * 100;
        let counter = 0;
        RequirementRoles.forEach(element => {
            ResourceRoles.forEach(val => {
                if (val.Roles_id == element.Roles_id && val.RRole_duration >= element.Job_duration) {
                    console.log('Roles matched:' + val.RRole_name);
                    counter++;
                }
            });
        });
        let matching = (counter / matched) * 100;
        return Math.round(matching);
    }
    async function domainMatching(percentage) {
        let domainTotalCount = Object.keys(RequirementDomains).length;
        let matched = (domainTotalCount / percentage) * 100;

        let counter = 0;
        RequirementDomains.forEach(element => {
            ResourceDomains.forEach(val => {
                if (val.Domain_id == element.Domain_id && val.RDomain_duration >= element.Domain_duration) {
                    console.log('DOMAIN matched:' + val.RDomain);
                    counter++;
                }
            });
        });
        let matching = (counter / matched) * 100;
        return Math.round(matching);
    }

    async function getResourceData() {
        return await resourceTb.findOne({ where: { Resource_id: Resource_id } })
    }
    async function availabilitMatching(percentage) {
        return await requirementTb.findOne({ where: { Requirement_id: Requirement_id } }).then(data => {
            console.log('------------------------------------------------');
            console.log('Available_from=' + resourceData.Available_from);
            console.log('Available_to=' + resourceData.Available_to);
            console.log('Requirement_start=' + data.Requirement_start);
            console.log('Requirement_end=' + data.Requirement_end);
            console.log('------------------------------------------------');
            if (data.Requirement_start <= resourceData.Available_from && data.Requirement_end >= resourceData.Available_to) {
                return Number(percentage);

            } else {
                return 0;
            }
        })
    }
    async function introVideoMatching(percentage) {
        return await resourceTb.findOne({ where: { Resource_id: Resource_id } }).then(data => {
            if (data.Intro_video !== '') {
                return Number(percentage);

            } else {
                return 0;
            }
        })

    }
};