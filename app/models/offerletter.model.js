module.exports = (sequelize, Sequelize) => {
    const AssignTb = sequelize.define("OfferLetterTbs", {
      OfferLetter_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      Requirement_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { 
          model: 'RequirementsTbs',
          key: 'Requirement_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      },
      Resource_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { 
          model: 'ResourceTbs',
          key: 'Resource_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      } ,
      Payment_status: {
        type: Sequelize.ENUM,
        values : ['Approved', 'Pending'],
        defaultValue: 'Pending'
      }, 
      Company_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { 
          model: 'CompanyTbs',
          key: 'Company_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      } ,  
      User_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { 
          model: 'UsersTbs',
          key: 'User_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      },
      ReportDate: {
        type: Sequelize.DATEONLY
      }, 
      ReportTime: {
        type: Sequelize.STRING
      },
      locationName: {
        type: Sequelize.STRING
      }, 
      address: {
        type: Sequelize.TEXT
      }, 
      reportingManager: {
        type: Sequelize.STRING
      }, 
      contactPerson: {
        type: Sequelize.STRING
      },
      contactPersonPhone: {
        type: Sequelize.STRING
      },
      instructions: {
        type: Sequelize.TEXT
      }, 
      documents: {
        type: Sequelize.TEXT
      }, 
      Invoice_address: {
        type: Sequelize.TEXT
      }, 
      Init_invoice_file: {
        type: Sequelize.TEXT
      }
    });
  
    return AssignTb;
  };
    
