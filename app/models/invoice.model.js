module.exports = (sequelize, Sequelize) => {
    const Invoice = sequelize.define("InvoiceTbs", {
        Invoice_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            primaryKey: true,

        },
        Invoice_code: {
            type: Sequelize.INTEGER,
            primaryKey: true,

        },
        Requirement_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            primaryKey: true,

            references: {
                model: 'RequirementsTbs',
                key: 'Requirement_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        },
        Resource_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            primaryKey: true,
            references: {
                model: 'ResourceTbs',
                key: 'Resource_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        },
        User_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            primaryKey: true,
            references: {
                model: 'UsersTbs',
                key: 'User_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        },
        Invoice_file: {
            type: Sequelize.STRING
        },
        From_date: {
            primaryKey: true,
            type: Sequelize.DATEONLY
        },
        To_date: {
            primaryKey: true,
            type: Sequelize.DATEONLY
        },
        Transaction_id: {
            type: Sequelize.STRING
        },
        Verification: {
            type: Sequelize.ENUM,
            values: ['VERIFIED', 'PENDING'],
            defaultValue: 'PENDING'
        },
        Invoice_type: {
            type: Sequelize.ENUM,
            values: ['WEEKLY', 'INITIAL', 'RELEASE', 'PENALTY'],
            defaultValue: 'WEEKLY'
        },
    });

    return Invoice;
};