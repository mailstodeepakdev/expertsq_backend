module.exports = (sequelize, Sequelize) => {
    const ComplaintsTb = sequelize.define("ComplaintsTb", {
    Complain_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },  
      User_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { 
          model: 'UsersTbs',
          key: 'User_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      }, 
      Title: {
        type: Sequelize.STRING
      },
      ComplaintsCategory_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { 
          model: 'ComplaintsCategoryTbs',
          key: 'ComplaintsCategory_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      },
      Company_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { 
          model: 'CompanyTbs',
          key: 'Company_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      } ,   
      Requirement_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { 
          model: 'RequirementsTbs',
          key: 'Requirement_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      },
      Project_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { 
          model: 'ProjectsTbs',
          key: 'Project_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      } ,
      Resource_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: { 
          model: 'ResourceTbs',
          key: 'Resource_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      } ,
      Complaints: {
        type: Sequelize.TEXT
      },
      Sub_Category: {
        type: Sequelize.TEXT
      },
      Screenshot: {
        type: Sequelize.TEXT
      },
      Url: {
        type: Sequelize.STRING
      },
      Complain_Date: {
        type: Sequelize.DATE
      },
      Complain_status: {
        type: Sequelize.ENUM,
        values : ['Reviewing', 'Pending','Solved'],
        defaultValue: 'Pending'
      },  
    });
  
    return ComplaintsTb;
  };
    
