module.exports = (sequelize, Sequelize) => {
    const ComplaintsCategoryTbs = sequelize.define("ComplaintsCategoryTbs", {
        ComplaintsCategory_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },  
      ComplaintsCategory: {
        type: Sequelize.STRING, 
      },
      Category: {
        type: Sequelize.ENUM,
        values : ['Parent', 'Resource', 'Project'],
        defaultValue: 'Parent'
      },  
    });
  
    return ComplaintsCategoryTbs;
  };
    
