module.exports = (sequelize, Sequelize) => {
    const AssignTb = sequelize.define("AssignTb", {
        Assign_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Requirement_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            references: {
                model: 'RequirementsTbs',
                key: 'Requirement_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        },
        Resource_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            references: {
                model: 'ResourceTbs',
                key: 'Resource_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        },
        Approved_status: {
            type: Sequelize.ENUM,
            values: ['Approved', 'Pending', 'Preapproved', 'Released'],
            defaultValue: 'Preapproved'
        },
        Approved_by: {
            type: Sequelize.INTEGER,
            allowNull: true,
            references: {
                model: 'UsersTbs',
                key: 'User_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        },
        User_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'UsersTbs',
                key: 'User_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        },
        Invoice_address: {
            type: Sequelize.STRING
        },
    });

    return AssignTb;
};