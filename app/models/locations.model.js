module.exports = (sequelize, Sequelize) => {
    const Locations = sequelize.define("LocationsTbs", {
        Locations_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Locations_name: {
            type: Sequelize.STRING
        },
        Location_country: {
            type: Sequelize.STRING
        },
        Location_pincode: {
            type: Sequelize.INTEGER
        },
        Location_state: {
            type: Sequelize.STRING
        },
        Location_category: {
            type: Sequelize.ENUM,
            values: ['PRIMARY', 'LOCAL'],
            defaultValue: 'LOCAL'
        },
        User_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'UsersTbs',
                key: 'User_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        },
        Company_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
                model: 'CompanyTbs',
                key: 'Company_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        },
        Location_status: {
            type: Sequelize.ENUM,
            values: ['ACTIVE', 'INACTIVE'],
            defaultValue: 'ACTIVE'
        },
    });

    return Locations;
};