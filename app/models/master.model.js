module.exports = (sequelize, Sequelize) => {
    const MasterTbs = sequelize.define("MasterTbs", {
        Master_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Category: {
            type: Sequelize.STRING,
            allowNull: false
        },
        Value: {
            type: Sequelize.STRING,
            allowNull: true
        },
        From: {
            type: Sequelize.STRING,
            allowNull: true
        },
        To: {
            type: Sequelize.STRING,
            allowNull: true
        },
    });

    return MasterTbs;
};