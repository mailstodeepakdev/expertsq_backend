module.exports = (sequelize, Sequelize) => {
    const LocationAllocationTbs = sequelize.define("LocationAllocationTbs", {
        LocationAllocation_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },  
      Company_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { 
          model: 'CompanyTbs',
          key: 'Company_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      } , 
      User_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { 
          model: 'UsersTbs',
          key: 'User_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      } , 
      Locations_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { 
          model: 'LocationsTbs',
          key: 'Locations_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      } ,   
    });
  
    return LocationAllocationTbs;
  };
    
