module.exports = (sequelize, Sequelize) => {
    const Education = sequelize.define("EducationTbs", {
        Education_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Qualification: {
            type: Sequelize.STRING
        },
        Status: {
            type: Sequelize.ENUM,
            values: ['PENDING', 'APPROVED', 'REJECTED'],
            defaultValue: 'APPROVED'
        },
    });
    return Education;
};