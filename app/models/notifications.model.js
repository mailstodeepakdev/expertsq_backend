module.exports = (sequelize, Sequelize) => {
    const NotificationsTbs = sequelize.define("NotificationsTbs", {
        Notifications_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },  
      User_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { 
          model: 'UsersTbs',
          key: 'User_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      }, 
      CC_mail_user_id: {
        type: Sequelize.TEXT
      },  
      Category: {
        type: Sequelize.ENUM,
        values : ['ProjectCreation', 'AssignmentCreation'],
        defaultValue: 'ProjectCreation'
      },
       Company_id: {
         type: Sequelize.INTEGER,
         allowNull: false,
         references: { 
           model: 'CompanyTbs',
           key: 'Company_id'
         },
         onUpdate: 'CASCADE',
         onDelete: 'CASCADE',
       },  
    });
  
    return NotificationsTbs;
  };    
   
    
   