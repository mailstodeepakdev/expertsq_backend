module.exports = (sequelize, Sequelize) => {
    const FeedbackTbs = sequelize.define("FeedbackTbs", {
    Feedback_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },  
      User_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { 
          model: 'UsersTbs',
          key: 'User_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      }, 
      Feedback: {
        type: Sequelize.STRING
      },  
    });
  
    return FeedbackTbs;
  };
    
