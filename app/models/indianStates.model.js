module.exports = (sequelize, Sequelize) => {
    const IndianStatesTbs = sequelize.define("IndianStatesTbs", {
        IndianStatesTbs_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        primaryKey: true,

      },   
      stateName: {
        type: Sequelize.STRING
      }, 
      stateCode: {
        type: Sequelize.STRING
      },   
    });
  
    return IndianStatesTbs;
  };
   
