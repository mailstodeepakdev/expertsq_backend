module.exports = (sequelize, Sequelize) => {
    const ReleaseResourcesTbs = sequelize.define("ReleaseResourcesTbs", {
        Release_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Requirement_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            references: {
                model: 'RequirementsTbs',
                key: 'Requirement_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        },
        Resource_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            references: {
                model: 'ResourceTbs',
                key: 'Resource_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        },
        Release_type: {
            type: Sequelize.ENUM,
            values: ['SHORT', 'FULL'],
            defaultValue: 'FULL'
        },
        Cron_status: {
            type: Sequelize.ENUM,
            values: ['PENDING', 'COMPLETED'],
            defaultValue: 'PENDING'
        },
        Released_by: {
            type: Sequelize.INTEGER,
            allowNull: true,
            references: {
                model: 'UsersTbs',
                key: 'User_id'
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
        },
        Release_date: {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        Release_reason: {
            type: Sequelize.STRING,
            allowNull: false
        },
        Rating: {
            type: Sequelize.STRING,
        },
        Release_explanation: {
            type: Sequelize.TEXT,
        }
    });

    return ReleaseResourcesTbs;
};