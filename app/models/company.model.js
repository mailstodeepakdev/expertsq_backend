module.exports = (sequelize, Sequelize) => {
    const CompanyTb = sequelize.define("CompanyTbs", {
        Company_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      C_short_name: {
        type: Sequelize.STRING,
        allowNull: true,
        unique: true
      },
      C_full_name: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      Company_email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: true,
      },
      Website: {
        type: Sequelize.STRING,
        allowNull: true,
        unique: true
      },
      No_employees: {
        type: Sequelize.INTEGER
      },
      Numberof_Locations: {
        type: Sequelize.INTEGER
      },
      Founded: {
        type: Sequelize.DATEONLY
      }, 
      About: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      Company_logo: {
        type: Sequelize.TEXT
      },
      Eq_rating: {
        type: Sequelize.INTEGER
      },
      Enable_masking: {
        type: Sequelize.INTEGER,
        defaultValue:0
      },
      Freelancers: {
        type: Sequelize.INTEGER
      },
      Tiers_maching: {
        type: Sequelize.STRING
      },
      Complete_registration: {
        type: Sequelize.INTEGER,
        defaultValue:0
      },
      Company_tier: {
        type: Sequelize.INTEGER,
        defaultValue:5
      },
      AgreeTerms: {
        type: Sequelize.INTEGER,
        defaultValue:0
      }
    });
  
    return CompanyTb;
  };
   
