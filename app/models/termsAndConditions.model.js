module.exports = (sequelize, Sequelize) => {
    const TermsAndConditionsTbs = sequelize.define("TermsAndConditionsTbs", {
        Terms_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },  
      Terms: {
        type: Sequelize.TEXT,
        },
    });
  
    return TermsAndConditionsTbs;
  };
    
