module.exports = (sequelize, Sequelize) => {
    const BranchAllocationTbs = sequelize.define("BranchAllocationTbs", {
        BrancheAllocation_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },  
      Company_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { 
          model: 'CompanyTbs',
          key: 'Company_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      } , 
      User_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { 
          model: 'UsersTbs',
          key: 'User_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      } , 
      Branches_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { 
          model: 'BranchesTbs',
          key: 'Branches_id'
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE', 
      } ,   
    });
  
    return BranchAllocationTbs;
  };
    
