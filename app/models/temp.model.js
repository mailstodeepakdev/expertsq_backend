module.exports = (sequelize, Sequelize) => {
    const TempTbs = sequelize.define("TempTbs", {
        TempTbs_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },    
      User_salutation: {
      type: Sequelize.TEXT,
      },
      User_firstName: {
      type: Sequelize.TEXT,
      },
      User_secondName: {
      type: Sequelize.TEXT,
      },
      User_phone: {
      type: Sequelize.TEXT,
      }, 
      User_email: {
      type: Sequelize.TEXT,
      }, 
      User_password: {
      type: Sequelize.TEXT,
      },  
      Delegate_salutation: {
      type: Sequelize.TEXT,
      },
      Delegate_firstName : {
      type: Sequelize.TEXT,
      } ,
      Delegate_secondName : {
      type: Sequelize.TEXT,
      }, 
      Delegate_email : {
      type: Sequelize.TEXT,
      },
      Delegate_designation : {
      type: Sequelize.TEXT,
      },
      Delegate_phone : {
      type: Sequelize.TEXT,
      },  
      Company_fullname : {
      type: Sequelize.TEXT,
      },
      Company_shortname : {
      type: Sequelize.TEXT,
      },
      Company_noemployees : {
      type: Sequelize.TEXT,
      },
      Company_email : {
      type: Sequelize.TEXT,
      },  
      Company_website : {
      type: Sequelize.TEXT,
      },  
      Company_about :{
      type: Sequelize.TEXT,
      },  
      Numberof_Locations : {
      type: Sequelize.TEXT,
      },  
      Company_TAN: {
      type: Sequelize.TEXT,
      },  
      Company_CIN: {
      type: Sequelize.TEXT,
      },  
      Company_PAN : {
      type: Sequelize.TEXT,
      }, 
      Locations_name : {
      type: Sequelize.TEXT,
      },
      Locations_country : {
      type: Sequelize.TEXT,
      },
      Locations_pincode : {
      type: Sequelize.TEXT,
      },
      Locations_state :{
      type: Sequelize.TEXT,
      } ,
      Bank_name : {
      type: Sequelize.TEXT,
      },
      Bank_Branch : {
      type: Sequelize.TEXT,
      },
      Bank_address : {
      type: Sequelize.TEXT,
      },  
      Bank_accountNumber :{
      type: Sequelize.TEXT,
      },    
      Bank_IFSC : {
      type: Sequelize.TEXT,
      }, 
    });
  
    return TempTbs;
  };
    
